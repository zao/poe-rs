#[macro_use]
extern crate failure;

mod types;
pub use types::*;

use byteorder::LE;

use byteorder::ReadBytesExt;

pub fn parse_dat<Spec>(data: &[u8]) -> failure::Fallible<Dat<Spec>>
where
    Spec: types::SpecInfo,
{
    let data_offset = twoway::find_bytes(data, b"\xBB\xbb\xBB\xbb\xBB\xbb\xBB\xbb")
        .ok_or_else(|| format_err!("Could not find magic number"))?;
    let table_len = data_offset - 4;

    let mut r = &data[..];
    let row_count = r.read_u32::<LE>()? as usize;
    if table_len != row_count * Spec::row_width() {
        let frac = table_len as f32 / row_count as f32;
        bail!(
            "Computed row size {} ({}/{}) does not match spec size {}",
            frac,
            table_len,
            row_count,
            Spec::row_width()
        );
    }
    let rows = {
        let v: failure::Fallible<Vec<Spec>> = (0..row_count)
            .map(|row_num| -> failure::Fallible<Spec> {
                let base = row_num * Spec::row_width();
                let base = &r[base..];
                Spec::populate(base, &data[data_offset..])
                    .ok_or_else(|| format_err!("Failed to read row ID {} (of {})", row_num, row_count))
            })
            .collect();
        v?
    };

    Ok(Dat { rows })
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_dat_files() -> failure::Fallible<()> {
        time_test!();
        use crate::formats::ggpk;
        let pack = ggpk::Ggpk::open(r#"Content.ggpk"#)?;

        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/AchievementSetRewards.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::AchievementSetRewards>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/FixedHideoutDoodads.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::FixedHideoutDoodadTypes>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/HideoutDoodads.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::HideoutDoodads>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/HideoutNPCs.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::HideoutNPCs>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/HideoutRarity.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::HideoutRarity>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/Hideouts.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::Hideouts>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
            let mut v = vec![];
            if let Ok(dat) = &res {
                for row in &dat.rows {
                    if row.IsEnabled != 0 {
                        v.push(&row.Name);
                    }
                }
            }
            v.sort();
            eprintln!("{:#?}", v);
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/ItemVisualIdentity.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::ItemVisualIdentity>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            } else if let Ok(dat) = res {
                eprintln!("{:?}", dat);
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/MasterHideoutLevels.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::MasterHideoutLevels>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        if let ggpk::ResolvedEntry::File(file) = pack.lookup("Data/WorldAreas.dat")? {
            let contents = file.contents();
            let res = super::parse_dat::<super::spec::WorldAreas>(&contents);
            let full_path = crate::util::find_full_path(&pack, file.offset);
            if res.is_err() {
                eprintln!("{}: {:?}", full_path, res);
                panic!();
            }
        }
        Ok(())
    }

    #[test]
    fn diff_hideout_decorations() -> Result<(), failure::Error> {
        use crate::formats::{dat, ggpk};
        use std::collections::{BTreeMap, BTreeSet};

        #[derive(Debug)]
        struct Info {
            name: String,
            variations: Vec<String>,
        }

        fn read_hideout_decorations(path: &str) -> failure::Fallible<BTreeMap<String, Info>> {
            let pack = ggpk::Ggpk::open(path)?;
            let hod_file = pack.lookup_file("Data/HideoutDoodads.dat")?;
            let bit_file = pack.lookup_file("Data/BaseItemTypes.dat")?;
            let hod_dat = dat::parse_dat::<super::spec::HideoutDoodads>(&hod_file.contents())?;
            let bit_dat = dat::parse_dat::<super::spec::BaseItemTypes>(&bit_file.contents())?;

            let mut ret = BTreeMap::new();
            for row in &hod_dat.rows {
                let key = row.BaseItemTypesKey;
                let bit_row = &bit_dat.rows[key as usize];
                let id = bit_row.Id.clone();
                if ret.contains_key(&id) {
                    panic!("duplicate base item type {}", key);
                }
                let name = bit_row.Name.clone();
                let variations = row.Variation_AOFiles.clone();
                ret.insert(id, Info { name, variations });
            }
            Ok(ret)
        }

        let old_decorations = read_hideout_decorations(r#"Y:\Games\GGPK\Content-3.7.5c.ggpk"#)?;
        let new_decorations = read_hideout_decorations(r#"Y:\Games\GGPK\Content-3.8.1e.ggpk"#)?;

        eprintln!("old: {}", old_decorations.len());
        eprintln!("new: {}", new_decorations.len());

        let variation_changes = new_decorations.iter().fold(Vec::new(), |mut v, (k, i)| {
            if let Some(old) = old_decorations.get(k) {
                let old: BTreeSet<&str> = old.variations.iter().map(|x| x.as_ref()).collect();
                let new: BTreeSet<&str> = i.variations.iter().map(|x| x.as_ref()).collect();
                let olds: Vec<&str> = old.difference(&new).copied().collect();
                let news: Vec<&str> = new.difference(&old).copied().collect();
                if !olds.is_empty() || !news.is_empty() {
                    v.push((olds, news));
                }
            }

            v
        });

        for e in variation_changes {
            eprintln!("old/new: {:?}", e);
        }

        let only_new = new_decorations.iter().fold(Vec::new(), |mut v, e| {
            if !old_decorations.contains_key(e.0) {
                v.push(e.1);
            }
            v
        });

        eprintln!("Only new: {:#?}", only_new);

        Ok(())
    }
}