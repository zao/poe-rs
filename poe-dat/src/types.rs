#[derive(Debug)]
pub struct Dat<Spec> {
    pub rows: Vec<Spec>,
}

pub trait SpecInfo {
    fn row_width() -> usize;
    fn populate(base: &[u8], data: &[u8]) -> Option<Self>
    where
        Self: Sized;
}

struct DatReader<'i> {
    row: &'i [u8],
    data: &'i [u8],
}

impl<'i> DatReader<'i> {
    fn new(row: &'i [u8], data: &'i [u8]) -> Self {
        Self { row, data }
    }
}

#[allow(non_snake_case)]
#[allow(clippy::cognitive_complexity)]
pub mod spec {
    use byteorder::{ReadBytesExt, LE};
    
    fn read_bool(r: &mut DatReader) -> Option<bool> {
        Some(r.row.read_u8().ok()? != 0)
    }

    fn read_byte(r: &mut DatReader) -> Option<u8> {
        r.row.read_u8().ok()
    }

    fn read_float(r: &mut DatReader) -> Option<f32> {
        r.row.read_f32::<LE>().ok()
    }

    fn read_ref_generic(r: &mut DatReader) -> Option<i32> {
        read_int(r)
    }

    fn read_int(r: &mut DatReader) -> Option<i32> {
        r.row.read_i32::<LE>().ok()
    }

    fn read_long(r: &mut DatReader) -> Option<i64> {
        r.row.read_i64::<LE>().ok()
    }

    fn read_ref_int(r: &mut DatReader) -> Option<i32> {
        let ptr = r.row.read_u32::<LE>().ok()? as usize;
        let mut r = DatReader::new(&r.data[ptr..], r.data);
        read_int(&mut r)
    }

    fn read_ref_list_float(r: &mut DatReader) -> Option<Vec<f32>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_float(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_list_ref_generic(r: &mut DatReader) -> Option<Vec<i32>> {
        read_ref_list_int(r)
    }

    fn read_ref_list_int(r: &mut DatReader) -> Option<Vec<i32>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_int(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_list_long(r: &mut DatReader) -> Option<Vec<i64>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_long(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_list_ref_string(r: &mut DatReader) -> Option<Vec<String>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_ref_string(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_list_uint(r: &mut DatReader) -> Option<Vec<u32>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_uint(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_list_ulong(r: &mut DatReader) -> Option<Vec<u64>> {
        let len = r.row.read_u32::<LE>().ok()? as usize;
        let ptr = r.row.read_u32::<LE>().ok()? as usize;

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut v = vec![];
        for _ in 0..len {
            v.push(read_ulong(&mut r)?);
        }
        Some(v)
    }

    fn read_ref_string(r: &mut DatReader) -> Option<String> {
        let ptr = r.row.read_u32::<LE>().ok()? as usize;
        if ptr == r.data.len() {
            return Some("".to_owned());
        }

        let mut r = DatReader::new(&r.data[ptr..], r.data);
        let mut code_units = vec![];
        loop {
            let cu = read_ushort(&mut r)?;
            if cu == 0 {
                break String::from_utf16(&code_units).ok();
            }
            code_units.push(cu);
        }
    }

    fn read_short(r: &mut DatReader) -> Option<i16> {
        r.row.read_i16::<LE>().ok()
    }

    fn read_ushort(r: &mut DatReader) -> Option<u16> {
        r.row.read_u16::<LE>().ok()
    }

    fn read_uint(r: &mut DatReader) -> Option<u32> {
        r.row.read_u32::<LE>().ok()
    }

    fn read_ulong(r: &mut DatReader) -> Option<u64> {
        r.row.read_u64::<LE>().ok()
    }

    use super::{DatReader, SpecInfo};

    include!(concat!(env!("OUT_DIR"), "/spec.rs"));
}
