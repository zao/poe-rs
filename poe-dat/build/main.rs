mod genspec;

use std::env;
use std::path::Path;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = env::var("OUT_DIR")?;
    let dest_path = Path::new(&out_dir).join("spec.rs");
    genspec::generate_spec("src/spec.json", dest_path)?;

    Ok(())
}
