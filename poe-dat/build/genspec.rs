use indexmap::IndexMap;

use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::path::Path;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Spec {
    #[serde(flatten)]
    files: BTreeMap<String, FileSpec>,
}

#[derive(Debug, Deserialize)]
struct FileSpec {
    fields: IndexMap<String, FieldSpec>,
}

#[derive(Debug, Deserialize)]
struct FieldSpec {
    #[serde(rename = "type")]
    ty: String,
}

fn transform_field_name(name: &str) -> String {
    if name == "2DArt" {
        return "Art2D".to_string();
    } else {
        return name.to_string();
    }
}

fn field_width(ty: &str) -> usize {
    let mut parts = ty.split('|');
    match parts.next().unwrap() {
        "ref" => match parts.next().unwrap() {
            "list" => 4 + 4,
            _ => 4,
        },
        "bool" => 1,
        "byte" => 1,
        "short" => 2,
        "ushort" => 2,
        "int" => 4,
        "uint" => 4,
        "long" => 8,
        "ulong" => 8,
        "float" => 4,
        "double" => 8,
        _ => {
            eprintln!("{}", ty);
            unimplemented!()
        }
    }
}

fn field_type(ty: &[&str]) -> String {
    match ty[0] {
        "ref" => match ty[1] {
            "list" => format!("Vec<{}>", field_type(&ty[2..])),
            "string" => "String".to_string(),
            "ref" => format!("{}", field_type(&ty[1..])),
            "generic" => "i32".to_string(),
            _ => field_type(&ty[1..]),
        },
        "bool" => "bool".to_string(),
        "byte" => "u8".to_string(),
        "short" => "i16".to_string(),
        "ushort" => "u16".to_string(),
        "int" => "i32".to_string(),
        "uint" => "u32".to_string(),
        "long" => "i64".to_string(),
        "ulong" => "u64".to_string(),
        "float" => "f32".to_string(),
        "double" => "f64".to_string(),
        _ => {
            eprintln!("{:?}", ty);
            unimplemented!()
        }
    }
}

fn underscore_field_type(ty: &[&str]) -> String {
    ty.join("_")
}

struct Field<'a> {
    ty: &'a str,
    rust_name: String,
    rust_ty: String,
    extraction_type: String,
}

pub fn generate_spec<P1, P2>(
    json_path: P1,
    target_path: P2,
) -> Result<(), Box<dyn std::error::Error>>
where
    P1: AsRef<Path>,
    P2: AsRef<Path>,
{
    let spec: Spec = serde_json::from_reader(File::open(json_path.as_ref())?)?;
    let mut test_names = Vec::new();

    let mut fh = File::create(target_path.as_ref())?;
    for (name, file) in &spec.files {
        let stem = Path::new(&name).file_stem().unwrap().to_str().unwrap();
        {
            let spec_name = stem.to_owned();
            let spec_file = format!("Data/{}", name);
            test_names.push((spec_name, spec_file));
        }
        let row_width: usize = file.fields.iter().map(|f| field_width(&f.1.ty)).sum();
        let fields = file
            .fields
            .iter()
            .map(|(name, field)| {
                let ty_parts: Vec<&str> = field.ty.split('|').collect();
                let rust_ty = field_type(&ty_parts);
                let rust_name = transform_field_name(name);
                let extraction_type = underscore_field_type(&ty_parts);
                Field {
                    ty: &field.ty,
                    rust_name,
                    rust_ty,
                    extraction_type,
                }
            })
            .collect::<Vec<_>>();
        writeln!(
            fh,
            "#[derive(Debug, Clone)]
pub struct {name:} {{",
            name = &stem
        )?;
        for field in &fields {
            writeln!(
                fh,
                "    pub {name}: {rust_ty}, // {ty:},",
                name = field.rust_name,
                ty = field.ty,
                rust_ty = field.rust_ty,
            )?;
        }
        writeln!(fh, "}}\n")?;

        writeln!(fh, "impl SpecInfo for {name} {{", name = &stem)?;
        writeln!(
            fh,
            r#"    fn row_width() -> usize {{ {row_width} }}"#,
            row_width = row_width,
        )?;

        if fields.is_empty() {
            writeln!(
                fh,
                "    fn populate(_base: &[u8], _data: &[u8]) -> Option<Self> {{ Some(Self {{}}) }}"
            )?;
        } else {
            writeln!(
                fh,
                r#"    fn populate(base: &[u8], data: &[u8]) -> Option<Self> {{
        let mut r = DatReader::new(base, data);
        Some(Self {{"#
            )?;

            for field in &fields {
                writeln!(
                    fh,
                    "            {rust_name}: read_{extraction_type}(&mut r)?,",
                    rust_name = field.rust_name,
                    extraction_type = field.extraction_type,
                )?;
            }
            writeln!(
                fh,
                r#"        }})
    }}"#
            )?;
        }
        writeln!(
            fh,
            r#"}}
"#,
        )?;
    }

    writeln!(
        fh,
        r#"
#[cfg(test)]
mod tests {{
    #[test]
    fn check_all_dat_specs() -> failure::Fallible<()> {{
        let pack = crate::formats::ggpk::Ggpk::open("C:/Temp/ggpk/Content-3.9.1.ggpk")?;"#
    )?;

    for t in test_names {
        writeln!(
            fh,
            r#"
        match pack.lookup_file("{test_file}") {{
            Ok(file) => if let Err(e) = crate::formats::dat::parse_dat::<super::{test_name}>(&file.contents()) {{
                eprintln!("{test_name}: {{:?}}", e);
            }}
            Err(_e) => eprintln!("{test_name}: DAT file missing"),
        }}
        
"#,
            test_name = t.0,
            test_file = t.1,
        )?;
    }
    writeln!(
        fh,
        r#"
        Ok(())
    }}
}}"#
    )?;

    Ok(())
}
