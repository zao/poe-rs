#!/usr/bin/env python

from PyPoE.poe.constants import VERSION
from PyPoE.poe.file import specification
import json

with open('poe-dat/src/spec.json', 'w') as f:
    json.dump(specification.load(version = VERSION.STABLE).as_dict(), f, indent = 4)
