use poe::formats::ggpk;
use rayon::prelude::*;
use std::borrow::Cow;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(short = "g", long = "ggpk", parse(from_os_str))]
    ggpk: PathBuf,

    #[structopt(short = "d", long = "dest", parse(from_os_str))]
    dest: Option<PathBuf>,

    #[structopt(long = "no-unpack")]
    no_unpack : bool,
}

fn main() -> failure::Fallible<()> {
    let opt = Options::from_args();
    let dest = opt.dest.unwrap_or(PathBuf::from("."));
    let pack = ggpk::Ggpk::open(&opt.ggpk)?;
    let unpack = !opt.no_unpack;
    crossbeam::thread::scope(|s| -> failure::Fallible<()> {
        let (file_tx, file_rx) = crossbeam::channel::unbounded();
        {
            let pack = pack.clone();
            s.spawn(move |_| {
                poe::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
            });
        }
        file_rx.iter().par_bridge().try_for_each(|(_dir, file)| -> failure::Fallible<()> {
            let inner_path = poe::util::find_full_path(&pack, file.offset);
            let full_path = dest.join(inner_path);
            let mut file = file;
            let data: Cow<[u8]> = match full_path.extension().map(|s| s.to_str().unwrap()) {
                Some("dds") if unpack => {
                    file = poe::util::resolve_symlinks(&pack, file);
                    poe::util::try_decompress_without_magic_word(file.contents())
                }
                Some("fmt") | Some("mat") if unpack => poe::util::try_decompress(file.contents()),
                Some("ast") | Some("smd") | Some("tgm") if unpack => Cow::from(poe::util::decompress(file.contents()).unwrap()),
                _ => Cow::from(file.contents()),
            };
            std::fs::create_dir_all(full_path.parent().unwrap())?;
            std::fs::write(full_path, data)?;
            Ok(())
        })
    }).unwrap()
}
