use failure::bail;
use poe::formats::ggpk;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(short = "g", long = "ggpk", parse(from_os_str))]
    ggpk: PathBuf,

    #[structopt(short = "t", long = "type")]
    typ: String,

    #[structopt(name = "FILE")]
    files: Vec<String>,
}

fn main() -> failure::Fallible<()> {
    use rayon::prelude::*;
    let opt = Options::from_args();
    let pack = ggpk::Ggpk::open(&opt.ggpk)?;
    let error_occured = opt.files.par_iter().map(|file| -> failure::Fallible<()> {
        let path = PathBuf::from(file.trim_start_matches('/'));
        match pack.lookup(&file) {
            Ok(ggpk::ResolvedEntry::File(file)) => {
                use ggpk::NodeInfo;
                let lowercase = file.name().to_lowercase();
                let ext = lowercase.rsplit('.').nth(0);
                match ext {
                    Some("fmt") => {
                        let data = poe::util::try_decompress(file.contents());
                        let fmt = poe::formats::fmt::parse_fmt(&data)?;
                        let export_path = path.with_extension(&opt.typ);
                        if &opt.typ == "obj" {
                            std::fs::create_dir_all(export_path.parent().unwrap())?;
                            let mut w = std::fs::File::create(&export_path)?;
                            fmt.write_obj(&mut w)?;
                        }
                    }
                    _ => {},
                }
            }
            Err(e) => bail!("{}", e),
            _ => bail!("Filename {} denotes directory.", file),
        }
        Ok(())
    })
    .fold(|| false, |e, res| e || res.is_err())
    .reduce(|| false, |a, b| a || b);

    if error_occured {
        bail!("Something went wrong");
    } else {
        Ok(())
    }
}
