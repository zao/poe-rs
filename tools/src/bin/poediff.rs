use poe::formats::ggpk;
use rayon::iter::ParallelBridge;
use rayon::prelude::ParallelIterator;
use std::sync::Arc;

fn slurp_pack(
    id: usize,
    pack: Arc<ggpk::Ggpk>,
    file_tx: crossbeam::channel::Sender<(usize, ggpk::File)>,
) -> std::thread::JoinHandle<()> {
    std::thread::spawn(move || {
        let (tx, rx) = crossbeam::channel::bounded(128);
        std::thread::spawn(move || {
            while let Ok((_, file)) = rx.recv() {
                file_tx.send((id, file)).unwrap();
            }
        });
        poe::util::visit_ggpk_files(&pack, tx, "/").unwrap();
    })
}

fn main() -> failure::Fallible<()> {
    let usage = "poediff first.ggpk second.ggpk";
    let mut args = std::env::args().skip(1);
    let f1 = args.next().expect(usage);
    let f2 = args.next().expect(usage);
    let p1 = ggpk::Ggpk::open(&f1)?;
    let p2 = ggpk::Ggpk::open(&f2)?;

    let (file_tx, file_rx) = crossbeam::channel::bounded(4096);

    let t1 = slurp_pack(1, p1.clone(), file_tx.clone());
    let t2 = slurp_pack(2, p2.clone(), file_tx.clone());
    drop(file_tx);

    let mux_t = std::thread::spawn(move || {
        use std::collections::{BTreeSet, HashMap};
        let mut v1: HashMap<String, ggpk::File> = HashMap::new();
        let mut v2: HashMap<String, ggpk::File> = HashMap::new();

        while let Ok((id, file)) = file_rx.recv() {
            if id == 1 {
                let full_path = poe::util::find_full_path(&p1, file.offset);
                v1.insert(full_path, file);
            } else if id == 2 {
                let full_path = poe::util::find_full_path(&p2, file.offset);
                v2.insert(full_path, file);
            }
        }

        let keys1: BTreeSet<String> = v1.keys().cloned().collect();
        let keys2: BTreeSet<String> = v2.keys().cloned().collect();

        let mut differences: Vec<(String, usize, usize)> = keys1
            .intersection(&keys2)
            .par_bridge()
            .filter_map(|k| {
                let c1 = v1[k].contents();
                let c2 = v2[k].contents();
                if c1.len() != c2.len() || c1 != c2 {
                    Some((k.clone(), c1.len(), c2.len()))
                } else {
                    None
                }
            })
            .collect();
        differences.sort();

        for k in differences {
            println!("Different: {}, {} bytes -> {} bytes", k.0, k.1, k.2,);
        }

        for k in keys1.difference(&keys2) {
            println!("Removed: {}, {} bytes", k, v1[k].contents().len());
        }
        for k in keys2.difference(&keys1) {
            println!("Added: {}, {} bytes", k, v2[k].contents().len());
        }
    });

    t1.join().unwrap();
    t2.join().unwrap();
    mux_t.join().unwrap();
    Ok(())
}
