use poe::formats::ggpk;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short, long, parse(from_os_str))]
    ggpk: Option<PathBuf>,

    #[structopt(name = "PATH")]
    path: String,
}

fn main() -> failure::Fallible<()> {
    let opt = Opt::from_args();
    let pack = ggpk::Ggpk::open(opt.ggpk.unwrap_or_else(|| "Content.ggpk".into()))?;
    let graph_file = pack.lookup_file(&opt.path)?;
    let contents = poe::util::try_decompress(graph_file.contents());
    let contents =
        poe::util::vec_to_utf16(&contents).expect(&format!("Graph file {} not UTF-16", &opt.path));
    let graph: fxgraph::Graph = serde_json::from_str(&contents)?;
    let mut dot_path = PathBuf::from(&opt.path);
    dot_path.set_extension("fxgraph.dot");
    // eprintln!("{:#?}", &graph);
    let dot_string = graph.render_dot()?;
    std::fs::create_dir_all(dot_path.parent().unwrap())?;
    std::fs::write(&dot_path, &dot_string[..])?;
    Ok(())
}

mod fxgraph {
    use serde::Deserialize;
    use std::collections::{BTreeMap, BTreeSet};
    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct FxGraph {
        pub version: usize,

        #[serde(default)]
        pub textures: Vec<Texture>,

        pub defaultgraph: Graph,

        #[serde(default)]
        pub graphinstances: Vec<GraphInstance>,

        pub minimapcolourmultiplier: Option<f32>,
        pub minimaponly: Option<bool>,
        pub shadertype: Option<String>,

        #[serde(default)]
        pub roofsection: bool,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct Texture {
        pub filename: String,
        pub format: String,
        pub sources: Vec<TextureSource>,
        pub count: usize,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct TextureSource {
        pub filename: String,
        pub src_mask: Option<String>,
        pub dest_mask: Option<String>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct Graph {
        pub version: Option<usize>,

        pub effect_order: Option<usize>,
        pub overriden_blend_mode: Option<String>,
        pub overriden_lighting_model: Option<String>,
        pub alphatest_threshold: Option<usize>,
        #[serde(default)]
        pub constant: bool,

        #[serde(default)]
        pub disable_terrain_effects: bool,
        #[serde(default)]
        pub enable_terrain_effects: bool,

        #[serde(default)]
        pub ignore_constant: bool,

        #[serde(default)]
        pub lighting_disabled: bool,

        #[serde(default)]
        pub no_normals: bool,

        #[serde(default)]
        pub no_playerlight_shadows: bool,

        #[serde(default)]
        pub use_start_time: bool,

        #[serde(default, rename = "OnEnterRenderStates")]
        pub on_enter_render_states: Vec<RenderState>,

        #[serde(default, rename = "OnExitRenderStates")]
        pub on_exit_render_states: Vec<RenderState>,

        #[serde(default, rename = "RestorableRenderStates")]
        pub restorable_render_states: Vec<RenderState>,

        #[serde(default)]
        pub textures: Vec<Texture>,

        #[serde(default)]
        pub nodes: Vec<GraphNode>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct RenderState {
        pub name: String,
        pub value: Option<String>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct GraphNode {
        pub name: String,
        pub ui_position: Vec<f32>,

        pub params: Option<String>,
        pub custom_parameter: Option<String>,
        pub stage: Option<String>,

        #[serde(default)]
        pub inputs: Vec<GraphNodeInput>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct GraphNodeInput {
        pub name: String,
        pub output_link: String,
        pub input_link: String,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct GraphInstance {
        pub parent: String,

        #[serde(default)]
        pub custom_parameters: Vec<GraphInstanceParameter>,

        pub alphatest_threshold: Option<usize>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(deny_unknown_fields)]
    pub struct GraphInstanceParameter {
        pub name: String,
        pub params: String,
    }

    enum NodeCategory {
        Source,
        Intermediate,
        Sink,
    }

    #[derive(Debug)]
    struct StructuredGraph<'g> {
        variables: Vec<(String, &'g GraphNode)>,
        objects: BTreeMap<String, Vec<(String, &'g GraphNode)>>,
    }

    #[derive(Debug)]
    enum QualifiedName {
        Free(String),
        Member { object: String, field: String },
    }

    impl Graph {
        fn format_attributes<I: Iterator<Item = (String, String)>>(attrs: I) -> String {
            let attrs = attrs
                .map(|(k, v)| format!(r#"{}="{}""#, k, v))
                .collect::<Vec<_>>();
            attrs.join(" ")
        }

        fn format_members(members: &[(String, &GraphNode)]) -> String {
            let labels = members
                .iter()
                .map(|m| format!("<{label}> {label}", label = m.0))
                .collect::<Vec<_>>();
            labels.join("|")
        }

        fn categorize_node(&self, node: &GraphNode) -> NodeCategory {
            if node.inputs.is_empty() {
                return NodeCategory::Source;
            }
            for other in &self.nodes {
                for input in &other.inputs {
                    if input.name == node.name {
                        return NodeCategory::Intermediate;
                    }
                }
            }
            NodeCategory::Sink
        }

        fn interpret_name(name: &str) -> QualifiedName {
            let mut parts = name.splitn(2, "__");
            let field = parts.next().unwrap().to_string();
            if let Some(object) = parts.next().map(ToString::to_string) {
                QualifiedName::Member { object, field }
            } else {
                QualifiedName::Free(field)
            }
        }

        fn determine_structure<'g>(&'g self) -> StructuredGraph<'g> {
            let qnodes = self
                .nodes
                .iter()
                .map(|n| (Self::interpret_name(&n.name), n))
                .collect::<Vec<_>>();

            let variables = qnodes
                .iter()
                .filter_map(|n| match &n.0 {
                    QualifiedName::Member { .. } => None,
                    QualifiedName::Free(name) => Some((name.clone(), n.1)),
                })
                .collect();
            let objects = qnodes.iter().fold(
                <BTreeMap<String, Vec<(String, &'g GraphNode)>>>::new(),
                |mut subs, qn| {
                    if let QualifiedName::Member { object, field } = &qn.0 {
                        subs.entry(object.clone())
                            .or_default()
                            .push((field.clone(), qn.1));
                    }
                    subs
                },
            );

            StructuredGraph { variables, objects }
        }

        fn format_ref(name: &str) -> String {
            match Self::interpret_name(&name) {
                QualifiedName::Free(name) => name,
                QualifiedName::Member { object, field } => format!("{}:{}", object, field),
            }
        }

        fn render_links<W: std::io::Write>(w: &mut W, node: &GraphNode) -> std::io::Result<()> {
            for input in &node.inputs {
                let source_ref = Self::format_ref(&input.name);
                let sink_ref = Self::format_ref(&node.name);
                let attrs = vec![
                    ("headlabel".to_string(), input.input_link.clone()),
                    ("taillabel".to_string(), input.output_link.clone()),
                ];
                writeln!(
                    w,
                    "{} -> {} [{}];",
                    &source_ref,
                    &sink_ref,
                    Self::format_attributes(attrs.into_iter())
                )?;
            }
            Ok(())
        }

        pub fn render_dot(&self) -> failure::Fallible<String> {
            use std::io::Write;
            let mut s = <Vec<u8>>::new();
            writeln!(s, r#"digraph G {{"#)?;
            // writeln!(s, r#"graph [fontsize=10 fontname="Verdana"];"#)?;
            // writeln!(s, r#"node [fontsize=10 fontname="Verdana"];"#)?;
            // writeln!(s, r#"edge [fontsize=10 fontname="Verdana"];"#)?;
            let structure = self.determine_structure();
            for (obj_name, fields) in &structure.objects {
                writeln!(
                    s,
                    r#"{name} [shape="record" label="{name}|{{{members}}}"];"#,
                    name = obj_name,
                    members = Self::format_members(fields)
                )?;
                for (_, field) in fields {
                    Self::render_links(&mut s, field)?;
                }
            }
            for (qname, node) in &structure.variables {
                let mut attrs = vec![];
                attrs.push((
                    "shape".to_string(),
                    match self.categorize_node(node) {
                        NodeCategory::Source => "box",
                        NodeCategory::Intermediate => "oval",
                        NodeCategory::Sink => "pentagon",
                    }
                    .to_string(),
                ));
                // attrs.push((
                //     "pos".to_string(),
                //     format!(
                //         "{},{}!",
                //         node.ui_position[0] / 50.0,
                //         node.ui_position[1] / 50.0
                //     ),
                // ));
                writeln!(
                    s,
                    "{} [{}];",
                    &node.name,
                    Self::format_attributes(attrs.into_iter())
                )?;
                Self::render_links(&mut s, node)?;
            }
            writeln!(s, "}}")?;
            Ok(String::from_utf8(s).unwrap())
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_all_mat_graphs() -> failure::Fallible<()> {
        use ggpk::NodeInfo;
        use poe::formats::{ggpk, mat};
        let pack = ggpk::Ggpk::open("../Content.ggpk")?;
        let mut problems = Vec::new();
        poe::util::visit_ggpk_files_sync(
            &pack,
            |_, file| {
                if file.name().to_lowercase().ends_with(".mat") {
                    let mat = mat::parse_mat(file.contents())?;
                    let res = match &mat {
                        mat::Mat::V3(ref mat) => {
                            if let Some(g) = &mat.graph {
                                let g: Result<super::fxgraph::GraphInstance, _> =
                                    serde_json::from_value(g.clone());
                                g.map(|_| ())
                            } else {
                                return Ok(());
                            }
                        }
                        mat::Mat::V4(ref mat) => {
                            let fxg: Result<super::fxgraph::FxGraph, _> =
                                serde_json::from_value(mat.payload.clone());
                            fxg.map(|_| ())
                        }
                    };
                    if let Err(e) = res {
                        problems.push((poe::util::find_full_path(&pack, file.offset), e));
                    }
                }
                Ok(())
            },
            "/",
        )?;
        for problem in &problems {
            eprintln!("{} - {}", problem.0, problem.1);
        }
        Ok(())
    }

    #[test]
    fn test_all_fxgraph_graphs() -> failure::Fallible<()> {
        use ggpk::NodeInfo;
        use poe::formats::ggpk;
        let pack = ggpk::Ggpk::open("../Content.ggpk")?;
        let mut problems = Vec::new();
        poe::util::visit_ggpk_files_sync(
            &pack,
            |_, file| {
                if file.name().to_lowercase().ends_with(".fxgraph") {
                    let contents = poe::util::vec_to_utf16(file.contents()).unwrap();
                    if !contents.trim().is_empty() {
                        let g: Result<super::fxgraph::Graph, _> = serde_json::from_str(&contents);
                        if let Err(e) = g {
                            problems.push((poe::util::find_full_path(&pack, file.offset), e));
                        }
                    }
                }
                Ok(())
            },
            "/",
        )?;
        for problem in &problems {
            eprintln!("{} - {}", problem.0, problem.1);
        }
        Ok(())
    }
}
