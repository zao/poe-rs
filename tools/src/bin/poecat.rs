#[macro_use]
extern crate failure;

use std::io::Write;

fn main() -> Result<(), failure::Error> {
    use poe::formats::ggpk;
    let args = std::env::args().collect::<Vec<_>>();
    let pack_filename = args
        .get(1)
        .ok_or_else(|| format_err!("GGPK filename not specified."))?;
    let filename = args
        .get(2)
        .ok_or_else(|| format_err!("Filename not specified."))?;
    let pack = ggpk::Ggpk::open(pack_filename)?;
    let file = pack.lookup(filename)?;
    if let ggpk::ResolvedEntry::File(file) = file {
        std::io::stdout().write_all(file.contents())?;
    }
    Ok(())
}
