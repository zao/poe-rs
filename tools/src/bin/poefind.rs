use poe::formats::ggpk;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(short = "g", long = "ggpk", parse(from_os_str))]
    ggpk: PathBuf,

    #[structopt(name = "GLOB", default_value = "*")]
    glob: String,
}

fn main() -> failure::Fallible<()> {
    let opt = Options::from_args();
    let pack = ggpk::Ggpk::open(&opt.ggpk)?;
    let glob = globset::Glob::new(&opt.glob)?.compile_matcher();
    poe::util::visit_ggpk_entries_sync(
        &pack,
        |_dir, entry| -> failure::Fallible<()> {
            use ggpk::NodeInfo;
            let path = poe::util::find_full_path(&pack, entry.offset());
            if glob.is_match(&path) {
                match entry {
                    ggpk::ResolvedEntry::File(f) => {
                        println!("{}|{}", path, f.size());
                    }
                    ggpk::ResolvedEntry::Directory(d) => {
                        println!("{}/", path);
                    }
                }
            }
            Ok(())
        },
        "/",
    )?;
    Ok(())
}
