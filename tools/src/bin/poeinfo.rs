fn extension<S: AsRef<str>>(path: S) -> String {
    let p = path.as_ref();
    let ext = p.rsplitn(2, ".").next().unwrap();
    ext.to_owned()
}

fn main() -> failure::Fallible<()> {
    use poe::formats::ggpk;
    let args = std::env::args().collect::<Vec<_>>();
    let pack = ggpk::Ggpk::open(&args[1])?;
    let filename = args[2].clone();
    let ext = extension(&filename);
    if let ggpk::ResolvedEntry::File(_file) = pack.lookup(&filename)? {
        match ext.as_ref() {
            "arm" => {}
            _ => {}
        }
    }
    Ok(())
}
