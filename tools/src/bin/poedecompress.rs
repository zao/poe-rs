use failure::bail;

fn main() -> failure::Fallible<()> {
    let file = std::env::args().nth(1).expect("No filename specified");
    let contents = std::fs::read(&file)?;
    if contents.len() < 4 {
        bail!("File too short");
    }
    let size_offset = if &contents[0..3] == b"CMP" { 3 } else { 0 };
    let uncompressed = poe::util::decompress_without_magic_word(&contents[size_offset..])?;
    std::fs::write(file, &uncompressed)?;
    Ok(())
}
