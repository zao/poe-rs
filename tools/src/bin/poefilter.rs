use failure::bail;
use globset::{Glob, GlobSetBuilder};
use poe::formats::ggpk;
use std::collections::{BTreeMap, BTreeSet};
use std::path::{Path, PathBuf};
use structopt::StructOpt;

fn from_glob(input: &str) -> Result<Glob, globset::Error> {
    Glob::new(input)
}

#[derive(Debug, StructOpt)]
struct Options {
    #[structopt(short = "g", long = "ggpk", parse(from_os_str))]
    ggpk: PathBuf,

    #[structopt(short = "t", long = "target", parse(from_os_str))]
    target: PathBuf,

    #[structopt(name = "PATTERN", parse(try_from_str = from_glob))]
    patterns: Vec<Glob>,
}

trait EntryInfo {
    fn entry_size(&self) -> u64;
}

struct TemplatePack {
    root: TemplateDirectory,
}

fn root_size() -> u64 {
    4 + // rec_len
    4 + // tag
    4 + // child count
    8 // child offset
}

struct TemplateDirectory {
    name: widestring::U16CString,
    digest: poe::formats::ggpk::Digest,
    entries: Vec<(TemplateInfo, TemplateEntry)>,
}

impl EntryInfo for TemplateDirectory {
    fn entry_size(&self) -> u64 {
        4 + // rec_len
        4 + // tag
        4 + // name_len
        4 + // child_count
        32 + // digest
        2 * (self.name.len() as u64 + 1) + // name
        (4 + 8) * self.entries.len() as u64 // child info
    }
}

struct TemplateInfo {
    hash: poe::formats::ggpk::NameHash,
    name: widestring::U16CString,
}

impl EntryInfo for poe::formats::ggpk::File {
    fn entry_size(&self) -> u64 {
        use poe::formats::ggpk::NodeInfo;
        let name = self.name16();
        (
            4 + // rec_len
            4 + // tag
            4 + // name_len
            32 + // digest
            2 * (name.len() as u64 + 1) + // name
            self.size() as u64
            // data
        )
    }
}

impl EntryInfo for TemplateEntry {
    fn entry_size(&self) -> u64 {
        match self {
            TemplateEntry::Directory(dir) => dir.entry_size(),
            TemplateEntry::File(file) => file.entry_size(),
        }
    }
}

enum TemplateEntry {
    Directory(TemplateDirectory),
    File(poe::formats::ggpk::File),
}

use byteorder::{LE, WriteBytesExt};
use std::io::{Seek, SeekFrom, Write};

fn write_entry<W: Write + Seek>(w: &mut W, entry: &TemplateEntry) -> std::io::Result<u64> {
    match entry {
        TemplateEntry::Directory(dir) => write_directory(w, dir),
        TemplateEntry::File(file) => write_file(w, file),
    }
}

fn write_file<W: Write + Seek>(w: &mut W, file: &poe::formats::ggpk::File) -> std::io::Result<u64> {
    let offset = w.seek(SeekFrom::Current(0))?;
    let rec_len = file.entry_size() as u32;
    w.write_u32::<LE>(rec_len)?;
    w.write_all(b"FILE")?;
    // w.write_u32::<LE>();
    unimplemented!()
}

fn write_directory<W: Write + Seek>(w: &mut W, dir: &TemplateDirectory) -> std::io::Result<u64> {
    unimplemented!()
}

fn write_ggpk_template<P: AsRef<Path>>(path: P, pack: &TemplatePack) -> std::io::Result<()> {
    let mut fh = std::fs::File::create(path)?;
    let root_size = root_size();
    fh.seek(SeekFrom::Current(root_size as i64))?;
    let root_offset = write_directory(&mut fh, &pack.root)?;
    fh.seek(SeekFrom::Start(0))?;
    fh.write_u32::<LE>(root_size as u32)?;
    fh.write_all(b"GGPK")?;
    fh.write_u32::<LE>(1)?;
    fh.write_u64::<LE>(root_offset)?;
    Ok(())
}

fn main() -> failure::Fallible<()> {
    let opts = Options::from_args();
    let set = {
        let mut builder = GlobSetBuilder::new();
        for pattern in opts.patterns {
            builder.add(pattern);
        }
        builder.build()?
    };
    let ggpk = ggpk::Ggpk::open(opts.ggpk)?;
    let tmpl = TemplatePack {
        root: TemplateDirectory {
            name: widestring::U16CString::new(vec![])?,
            entries: vec![],
            digest: Default::default(),
        },
    };
    write_ggpk_template(opts.target, &tmpl)?;

    let mut directory_contents = <BTreeMap<String, BTreeSet<String>>>::new();

    poe::util::visit_ggpk_entries_sync(
        &ggpk,
        |dir, e| {
            use ggpk::NodeInfo;
            let full_path = poe::util::find_full_path(&ggpk, e.offset());
            if set.is_match(&full_path) {
                eprintln!("{:?}", &full_path);
            }
            Ok(())
        },
        "/",
    )?;

    Ok(())
}
