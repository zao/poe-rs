#[macro_use]
extern crate log;

use byteorder::{WriteBytesExt, LE};
use indicatif::ProgressBar;

use poe::formats::ggpk::{NodeInfo, ResolvedEntry};

use std::collections::{BTreeMap, HashMap};
use std::path::PathBuf;

const USAGE: &str = "strip_ggpk_contents Content.ggpk Metadata.ggpk-lite";
const GGPK_ENTRY_SIZE: u64 = 4 + 4 + 4 + 2 * 8; // rec_len, tag, child_count, child_offsets
const FREE_ENTRY_SIZE: u64 = 4 + 4 + 8;

fn measure_entry_lite(entry: &ResolvedEntry) -> u64 {
    let name = entry.name16();
    let name_cb = name.len() as u64 * 2 + 2;
    match entry {
        ResolvedEntry::Directory(dir) => {
            let entry_count = dir.entries().count() as u64;
            4 + 4 + 4 + 4 + 32 + name_cb + entry_count * (4 + 8)
        }
        ResolvedEntry::File(file) => 4 + 4 + 4 + 32 + name_cb,
    }
}

fn measure_entry(entry: &ResolvedEntry) -> u64 {
    let name = entry.name16();
    let name_cb = name.len() as u64 * 2 + 2;
    match entry {
        ResolvedEntry::Directory(dir) => {
            let entry_count = dir.entries().count() as u64;
            4 + 4 + 4 + 4 + 32 + name_cb + entry_count * (4 + 8)
        }
        ResolvedEntry::File(file) => 4 + 4 + 4 + 32 + name_cb + file.contents().len() as u64,
    }
}

fn write_ggpk_chunk(fh: &mut std::fs::File) -> std::io::Result<()> {
    use std::io::Write;
    fh.write_u32::<LE>(GGPK_ENTRY_SIZE as u32)?;
    fh.write_all(b"GGPK")?;
    fh.write_u32::<LE>(2)?;
    fh.write_u64::<LE>(GGPK_ENTRY_SIZE as u64)?;
    fh.write_u64::<LE>((GGPK_ENTRY_SIZE + FREE_ENTRY_SIZE) as u64)?;
    Ok(())
}

fn write_free_chunk(fh: &mut std::fs::File) -> std::io::Result<()> {
    use std::io::Write;
    fh.write_u32::<LE>(FREE_ENTRY_SIZE as u32)?;
    fh.write_all(b"FREE")?;
    fh.write_u64::<LE>(0)?;
    Ok(())
}

fn write_file_chunk(
    fh: &mut std::fs::File,
    file: &poe::formats::ggpk::File,
) -> std::io::Result<()> {
    use std::io::Write;
    let name = file.name16();
    fh.write_u32::<LE>(measure_entry_lite(&ResolvedEntry::File(file.clone())) as u32)?;
    fh.write_all(b"FILE")?;
    fh.write_u32::<LE>(name.len() as u32 + 1)?;
    fh.write_all(file.digest().0.as_ref())?;
    for &cu in name.as_slice_with_nul() {
        fh.write_u16::<LE>(cu)?;
    }
    // fh.write_all(file.contents())?;
    Ok(())
}

fn write_pdir_chunk(
    fh: &mut std::fs::File,
    dir: &poe::formats::ggpk::Directory,
    dest_map: &HashMap<u64, u64>,
) -> std::io::Result<()> {
    use std::io::Write;
    let name = dir.name16();
    fh.write_u32::<LE>(measure_entry(&ResolvedEntry::Directory(dir.clone())) as u32)?;
    fh.write_all(b"PDIR")?;
    fh.write_u32::<LE>(name.len() as u32 + 1)?;
    fh.write_u32::<LE>(dir.entries().count() as u32)?;
    fh.write_all(dir.digest().0.as_ref())?;
    for &cu in name.as_slice_with_nul() {
        fh.write_u16::<LE>(cu)?;
    }
    for entry in dir.entries() {
        fh.write_u32::<LE>(entry.hash)?;
        fh.write_u64::<LE>(*dest_map.get(&entry.offset).unwrap())?;
    }

    Ok(())
}

fn main() -> anyhow::Result<()> {
    env_logger::init();
    let spinner = ProgressBar::new_spinner();
    let in_path: PathBuf = std::env::args().nth(1).expect(USAGE).into();
    let out_path: PathBuf = std::env::args().nth(2).expect(USAGE).into();

    let in_pack = poe::formats::ggpk::Ggpk::open(in_path).map_err(|e| e.compat())?;
    let mut relations = <HashMap<u64, u64>>::new();
    let mut entries = <HashMap<u64, poe::formats::ggpk::ResolvedEntry>>::new();

    let mut src_to_dest_offsets = <HashMap<u64, u64>>::new();
    let mut dest_to_src_offsets = <BTreeMap<u64, u64>>::new();
    let mut output_base: u64 = GGPK_ENTRY_SIZE + FREE_ENTRY_SIZE;
    {
        let root = in_pack.resolve(in_pack.root()).unwrap();
        src_to_dest_offsets.insert(root.offset(), output_base);
        dest_to_src_offsets.insert(output_base, root.offset());
        output_base += measure_entry(&root);
    }

    poe::util::visit_ggpk_entries_sync(
        &in_pack,
        |dir, entry| {
            relations.insert(dir.offset, entry.offset());
            entries.insert(
                dir.offset,
                poe::formats::ggpk::ResolvedEntry::Directory(dir),
            );
            src_to_dest_offsets.insert(entry.offset(), output_base);
            dest_to_src_offsets.insert(output_base, entry.offset());
            output_base += measure_entry_lite(&entry);
            entries.insert(entry.offset(), entry);

            Ok(())
        },
        "/",
    )
    .map_err(|e| e.compat())?;

    let mut out_fh = std::fs::File::create(out_path)?;
    write_ggpk_chunk(&mut out_fh)?;
    write_free_chunk(&mut out_fh)?;
    for (&dest_off, &src_off) in &dest_to_src_offsets {
        spinner.set_message(&format!("dst: {}, src: {}", dest_off, src_off));
        let entry = in_pack.resolve(src_off).unwrap();
        match entry {
            ResolvedEntry::Directory(dir) => {
                write_pdir_chunk(&mut out_fh, &dir, &src_to_dest_offsets)?
            }
            ResolvedEntry::File(file) => write_file_chunk(&mut out_fh, &file)?,
        }
    }

    info!("Relation count: {}", relations.len());
    info!("Entry count: {}", entries.len());
    info!("Output base: {}", output_base);
    // let first_few_outputs = dest_to_src_offsets.iter().take(5).collect::<Vec<_>>();
    // info!("First few outputs: {:#?}", first_few_outputs);

    Ok(())
}
