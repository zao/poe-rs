#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct Match {
    path: String,
    offsets: Vec<u64>,
}

fn main() -> Result<(), failure::Error> {
    use poe::formats::ggpk;
    use rayon::prelude::*;

    let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
    let pattern = {
        let text = "MetamorphosisFormedBoss";
        let mut v = vec![];
        text.encode_utf16().for_each(|cp| {
            v.push((cp & 0xFF) as u8);
            v.push((cp >> 8) as u8);
        });
        v
    };

    crossbeam::scope(|s| {
        let (match_tx, match_rx) = crossbeam::channel::unbounded::<Match>();
        let (tx, rx) = crossbeam::channel::unbounded::<(ggpk::Directory, ggpk::File)>();
        {
            // Search thread
            {
                let pack = pack.clone();
                s.spawn(move |_| {
                    rx.iter().par_bridge().for_each(|(_, file)| {
                        let full_path = poe::util::find_full_path(&pack, file.offset);
                        let data = file.contents();
                        let mut offsets = vec![];
                        let mut next_offset = 0;
                        loop {
                            if next_offset >= data.len() {
                                break;
                            }
                            match twoway::find_bytes(&data[next_offset..], &pattern) {
                                Some(pos) => {
                                    offsets.push(pos as u64);
                                    next_offset = pos + 1;
                                }
                                None => {
                                    break;
                                }
                            }
                        }
                        if !offsets.is_empty() {
                            match_tx
                                .send(Match {
                                    path: full_path,
                                    offsets,
                                })
                                .unwrap();
                        }
                    });
                });
            }

            // Print thread
            s.spawn(move |_| {
                for m in match_rx.iter() {
                    println!("{}: {:?}", m.path, m.offsets);
                }
            });
        }
        poe::util::visit_ggpk_files(&pack, tx, "/").unwrap();
    })
    .unwrap();

    Ok(())
}
