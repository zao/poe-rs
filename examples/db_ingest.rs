use poe::formats::ggpk::{Ggpk, NodeInfo};
use poe::util::visit_ggpk_files_sync;
// use sqlite3::{DatabaseConnection, ParamIx, PreparedStatement, QueryEach, StatementUpdate, ToSql};
use rusqlite::{params, Connection, NO_PARAMS};

struct InsertState {
    max_bytes: usize,
    num_bytes: usize,
    pile: Vec<poe::formats::ggpk::File>,
}

impl InsertState {
    fn new(max_bytes: usize) -> Self {
        Self {
            max_bytes,
            num_bytes: 0,
            pile: vec![],
        }
    }

    fn add(&mut self, file: poe::formats::ggpk::File) {
        self.num_bytes += file.contents().len();
        self.pile.push(file);
    }

    fn pump(&mut self, conn: &mut Connection) {
        if self.num_bytes >= self.max_bytes {
            let txn = conn.transaction().unwrap();
            {
                let mut stmt = txn.prepare_cached(
                    "INSERT OR IGNORE INTO rainbow_contents (signature, contents) VALUES ($1, $2)",
                ).unwrap();
                for file in &self.pile {
                    let digest = file.digest();
                    let contents = file.contents();
                    stmt.execute(params![&digest.0[..], &contents]).unwrap();
                }
            }
            txn.commit().unwrap();
            self.num_bytes = 0;
            self.pile.clear();
        }
    }
}

fn main() -> failure::Fallible<()> {
    let db_path = std::env::args()
        .nth(1)
        .expect("no database filename specified");
    let ggpk_path = std::env::args().nth(2).expect("no ggpk filename specified");
    let mut conn = Connection::open(db_path)?;
    conn.execute(
        r#"CREATE TABLE IF NOT EXISTS rainbow_contents (
        signature BLOB PRIMARY KEY NOT NULL,
        contents BLOB NOT NULL)"#,
        NO_PARAMS,
    )?;
    let pack = Ggpk::open(ggpk_path).unwrap();

    let (file_tx, file_rx) = crossbeam::channel::bounded::<poe::formats::ggpk::File>(10);
    let visit_t = std::thread::spawn(move || {
        visit_ggpk_files_sync(
            &pack,
            |_dir, file| {
                file_tx.send(file).unwrap();
                Ok(())
            },
            "/",
        )
        .unwrap();
    });

    let mut state = InsertState::new(50 * 1024 * 1024);
    while let Ok(file) = file_rx.recv() {
        state.add(file);
        state.pump(&mut conn);
    }
    state.pump(&mut conn);

    visit_t.join().unwrap();

    let mut stmt = conn.prepare("SELECT count(1) FROM rainbow_contents")?;
    let count: i64 = stmt.query_row(NO_PARAMS, |row| row.get(0))?;
    println!("Rainbow table count: {}", count);
    Ok(())
}
