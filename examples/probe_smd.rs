// #[macro_use]
// extern crate failure;

#[macro_use]
extern crate log;

use byteorder::{ReadBytesExt, LE};
use pretty_hex::*;
use std::io::{Read, Seek, SeekFrom};

use std::collections::{BTreeMap, BTreeSet};

fn read_vec<R: Read>(mut r: R, n: usize) -> Vec<u8> {
    let mut ret = vec![0u8; n];
    if r.read_exact(&mut ret).is_ok() {
        ret
    } else {
        vec![]
    }
}

fn main() -> failure::Fallible<()> {
    env_logger::init();
    let path = std::env::args()
        .nth(1)
        .unwrap_or_else(|| r#"\\10.0.1.20\depots\named\Content-2.2.0.ggpk"#.to_owned());
    eprintln!("Path: {}", path);
    let pack = poe::formats::ggpk::Ggpk::open(path)?;
    let mut trailer_versions = <BTreeMap<Option<u32>, usize>>::new();
    let mut trailer_sizes = <BTreeMap<usize, usize>>::new();
    let mut trailer_filenames = <BTreeMap<u32, BTreeSet<String>>>::new();
    poe::util::visit_ggpk_files_sync(
        &pack,
        |_dir, file| {
            use poe::formats::ggpk::NodeInfo;
            let lower_name = file.name().to_lowercase();
            if lower_name.ends_with(".smd") {
                let contents = poe::util::try_decompress(file.contents());
                let mut reader = std::io::Cursor::new(contents);
                let end_pos = reader.seek(std::io::SeekFrom::End(0))?;
                reader.seek(std::io::SeekFrom::Start(0))?;
                trace!("{}: {} bytes", file.name(), end_pos);
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                let version = reader.read_u8()?;
                trace!("version {}", version);

                if version != 1 {
                    // bail!(ParseError::UnknownVersion(version));
                }
                let triangle_count = reader.read_u32::<LE>()? as usize;
                let vertex_count = reader.read_u32::<LE>()? as usize;
                let _const_04 = reader.read_u8()?;
                let shape_count = reader.read_u16::<LE>()? as usize;
                let unk_count = reader.read_u32::<LE>()? as usize;
                trace!("triangle_count {}, vertex_count {}, _const_04 {}, shape_count {}, unk_count {}",
                    triangle_count, vertex_count, _const_04, shape_count, unk_count);

                // Bounding box
                trace!("reading bbox");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                reader.seek(SeekFrom::Current(6 * 4))?;

                // Shape list
                trace!("reading shape list");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                let shape_info_list = {
                    let mut v = Vec::with_capacity(shape_count);
                    for _ in 0..shape_count {
                        v.push((reader.read_u32::<LE>()?, reader.read_u32::<LE>()?));
                    }
                    v
                };

                // Shape name list
                let pre_shape_name_pos = reader.seek(SeekFrom::Current(0))?;
                trace!("reading shape name list");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                for shape_info in &shape_info_list {
                    let byte_count = shape_info.0 as usize;
                    reader.seek(SeekFrom::Current(byte_count as i64))?;
                }
                let post_shape_name_pos = reader.seek(SeekFrom::Current(0))?;
                trace!(
                    "shape name list length {}",
                    post_shape_name_pos - pre_shape_name_pos
                );
                assert_eq!(post_shape_name_pos - pre_shape_name_pos, unk_count as u64);

                // Indexed triangle list
                trace!("reading indexed triangle list");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                if vertex_count > 0xFFFF {
                    reader.seek(SeekFrom::Current(triangle_count as i64 * 3 * 4))?;
                } else {
                    reader.seek(SeekFrom::Current(triangle_count as i64 * 3 * 2))?;
                }

                // Vertex data list
                trace!("reading vertex data list");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                {
                    // XYZ 3f32, unk 8u8, UV 2f16, bone_idx 4u8, bone_weight 4u8
                    const VERTEX_SIZE: usize = 4 * 3 + 8 + 2 * 2 + 4 + 4;
                    reader.seek(SeekFrom::Current((vertex_count * VERTEX_SIZE) as i64))?;
                }

                // Extract trailer for troubleshooting
                let trailer_pos = reader.seek(std::io::SeekFrom::Current(0))?;
                trace!(
                    "trailer pos {}/{}, ({} bytes)",
                    trailer_pos,
                    end_pos,
                    (end_pos - trailer_pos)
                );
                trace!("reading trailer");
                trace!("{:?}", read_vec(reader.clone(), 32).hex_dump());
                let mut trailer = Vec::new();
                reader.read_to_end(&mut trailer)?;
                *trailer_sizes.entry(trailer.len()).or_default() += 1;

                if !trailer.is_empty() {
                    trace!("parsing trailer");
                    // Also parse the trailer
                    let mut r = &trailer[..];
                    let trailer_version = r.read_u32::<LE>()?;
                    trace!("trailer version {}", trailer_version);
                    *trailer_versions.entry(Some(trailer_version)).or_default() += 1;
                    let full_path = poe::util::find_full_path(&pack, file.offset());
                    trailer_filenames.entry(trailer_version).or_default().insert(full_path);
                } else {
                    *trailer_versions.entry(None).or_default() += 1;
                }
                // bail!("hi");
            }
            Ok(())
        },
        "/",
    )?;
    // eprintln!("trailers: {:#?}", trailer_filenames.get(&2));
    eprintln!("Trailer sizes: {:?}", trailer_sizes);
    eprintln!("Trailer versions: {:?}", trailer_versions);
    Ok(())
}
