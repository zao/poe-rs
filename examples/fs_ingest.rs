use poe::formats::ggpk::{Ggpk, NodeInfo};
use poe::util::visit_ggpk_files_sync;
use std::path::PathBuf;

fn hex_str<R: AsRef<[u8]>>(data: R) -> String {
    use std::fmt::Write;
    let mut s = String::new();
    for b in data.as_ref() {
        write!(&mut s, "{:02x}", b).unwrap();
    }
    s
}

const USAGE: &str = "usage: fs_ingest Content.ggpk target-dir/";

fn main() -> failure::Fallible<()> {
    let ggpk_path: PathBuf = std::env::args().nth(1).expect(USAGE).into();
    let target_dir: PathBuf = std::env::args().nth(2).expect(USAGE).into();
    let pack = Ggpk::open(ggpk_path).unwrap();
    std::fs::create_dir_all(&target_dir)?;
    let spinner = indicatif::ProgressBar::new_spinner();

    visit_ggpk_files_sync(
        &pack,
        |_dir, file| {
            let sig = file.digest().0;
            let dir_prefix = hex_str(&sig[..2]).chars().take(3).collect();
            let file_name = hex_str(&sig);
            let mut full_path = target_dir.clone();
            full_path.extend(&[dir_prefix, file_name]);
            spinner.set_message(&full_path.to_string_lossy());
            let _ = std::fs::create_dir_all(full_path.parent().unwrap());
            if std::fs::metadata(&full_path)
                .map(|meta| file.size() as u64 != meta.len())
                .unwrap_or(true)
            {
                if let Ok(mut fh) = std::fs::File::create(full_path) {
                    std::io::copy(&mut file.contents(), &mut fh)?;
                }
            }
            Ok(())
        },
        "/",
    )?;

    Ok(())
}
