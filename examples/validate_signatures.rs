fn main() -> Result<(), failure::Error> {
    use ggpk::NodeInfo;
    use poe::formats::ggpk;

    let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
    poe::util::visit_ggpk_entries_sync(
        &pack,
        |_parent, entry| {
            let computed = entry.compute_digest();
            let stored = entry.digest();
            if computed.0 != stored.0 {
                eprintln!(
                    "Signature mismatch for {}, computed {} != stored {}",
                    poe::util::find_full_path(&pack, entry.offset()),
                    computed,
                    stored
                );
            }
            Ok(())
        },
        "/",
    )?;

    {
        let root = pack.lookup("/")?;
        let computed = root.compute_digest();
        let stored = root.digest();
        if computed.0 != stored.0 {
            eprintln!(
                "Signature mismatch for /, computed {} != stored {}",
                computed, stored
            );
        }
    }

    Ok(())
}
