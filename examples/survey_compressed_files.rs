#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
enum FileClass {
    DecompressableWithHeader,
    DecompressableWithoutHeader,
    NotDecompressable,
}

fn main() -> Result<(), failure::Error> {
    use std::collections::BTreeMap;
    use std::sync::{Arc, Mutex};
    use poe::formats::ggpk;
    use rayon::prelude::*;

    let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
    let classes: Arc<Mutex<BTreeMap<(String, FileClass), usize>>> = Arc::new(Mutex::new(BTreeMap::new()));

    crossbeam::scope(|s| {
        let (tx, rx) = crossbeam::channel::unbounded::<(ggpk::Directory, ggpk::File)>();
        {
            let classes = classes.clone();
            
            s.spawn(move |_| {
                rx.iter().par_bridge().for_each(|(_, file)| {
                    use ggpk::NodeInfo;
                    let file_name = file.name();
                    let mut file_parts = file_name.rsplitn(2, '.');
                    let ext = file_parts.next().unwrap().to_owned().to_lowercase();
                    if file_parts.next().is_some() {
                        if poe::util::decompress(file.contents()).is_ok() {
                            *classes.lock().unwrap().entry((ext, FileClass::DecompressableWithHeader)).or_default() += 1;
                        }
                        else if poe::util::decompress_without_magic_word(file.contents()).is_ok() {
                            *classes.lock().unwrap().entry((ext, FileClass::DecompressableWithoutHeader)).or_default() += 1;
                        }
                        else {
                            *classes.lock().unwrap().entry((ext, FileClass::NotDecompressable)).or_default() += 1;
                        }
                    }
                });
            });
        }
        poe::util::visit_ggpk_files(&pack, tx, "/").unwrap();
    }).unwrap();

    for c in classes.lock().unwrap().iter() {
        println!("{:?}", c);
    }

    Ok(())
}