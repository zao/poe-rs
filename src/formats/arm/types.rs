#[derive(Debug, Clone)]
pub enum KEntry {
    F(usize),
    K {
        w: usize,
        h: usize,
        tail: Vec<isize>,
        last: Option<isize>,
    },
    N,
    S,
    O,
}

impl Default for KEntry {
    fn default() -> Self {
        KEntry::S
    }
}

#[derive(Debug)]
pub struct Quaternion {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32,
}

impl Quaternion {
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Self {
        Quaternion { x, y, z, w }
    }
}

impl Default for Quaternion {
    fn default() -> Self {
        Quaternion {
            x: 0.0,
            y: 0.0,
            z: 0.0,
            w: 1.0,
        }
    }
}

#[derive(Debug, Default)]
pub struct Arm {
    pub version: usize,
    pub gtets: Vec<String>,
    pub dims: (usize, usize, Option<usize>),
    pub a_upair: (usize, usize),
    pub name: String,
    pub b_upair: (usize, Option<usize>),
    pub k_1_by_1: KEntry,
    pub c_uious: Vec<(usize, isize, Option<usize>)>,
    pub dentries: Vec<Vec<DEntry>>,
    pub k_x_by_y: Vec<Vec<KEntry>>,
    pub whole_tail: Vec<String>,
    // pub doodads: Vec<Doodad>,
    // pub decals: Vec<Decal>,
    // pub more_decals: Vec<Decal>,
    // pub e2_usize: Option<usize>,
    // pub tail_line: Option<String>,
}

#[derive(Debug, Default)]
pub struct DEntry {
    pub x: usize,
    pub y: usize,
    pub h: f32,
    pub key: String,
}

#[derive(Debug)]
pub enum DKey {
    MobPack,
    Chest,
    Trigger,
}

#[derive(Debug, Default)]
pub struct Doodad {
    pub x: usize,
    pub y: usize,
    pub h: f32,
    pub r: Option<Quaternion>,
    pub q1: usize,
    pub q1_5: Option<usize>,
    pub q2s: Vec<f32>,
    pub q3: f32,
    pub ao: String,
    pub spec: String,
}

#[derive(Debug, Default)]
pub struct Decal {
    pub x: f32,
    pub y: f32,
    pub h: f32,
    pub q1: usize,
    pub s: f32,
    pub atlas: String,
    pub key: String,
}
