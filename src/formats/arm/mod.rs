mod types;
pub use types::*;

use std::path::PathBuf;

use failure::Fallible;
use regex::Regex;

fn parse_version<S: AsRef<str>>(s: S) -> Fallible<usize> {
    let version_str = s
        .as_ref()
        .splitn(2, "version ")
        .nth(1)
        .ok_or_else(|| format_err!("Failed to parse version {:?}", s.as_ref()))?;
    usize::from_str_radix(version_str, 10)
        .map_err(|e| format_err!("Failed to parse version number: {}", e))
}

fn parse_usize<S: AsRef<str>>(s: S) -> Fallible<usize> {
    usize::from_str_radix(s.as_ref(), 10)
        .map_err(|e| format_err!("Failed to parse usize number: {}, {}", e, s.as_ref()))
}

fn parse_upair<S: AsRef<str>>(s: S) -> Fallible<(usize, usize)> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^(\d+) (\d+)$"#).unwrap();
    }
    let caps = RE
        .captures(s.as_ref())
        .ok_or_else(|| format_err!("Failed to match an usize pair, {:?}", s.as_ref()))?;
    Ok((
        usize::from_str_radix(&caps[1], 10)
            .map_err(|e| format_err!("Failed to parse usize number: {}", e))?,
        usize::from_str_radix(&caps[2], 10)
            .map_err(|e| format_err!("Failed to parse usize number: {}", e))?,
    ))
}

fn parse_dims<S: AsRef<str>>(s: S) -> Fallible<(usize, usize, Option<usize>)> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^(\d+) (\d+)(?: (\d+))?$"#).unwrap();
    }
    let caps = RE
        .captures(s.as_ref())
        .ok_or_else(|| format_err!("Failed to match a dims string, {:?}", s.as_ref()))?;
    Ok((
        usize::from_str_radix(&caps[1], 10)
            .map_err(|e| format_err!("Failed to parse usize number: {}", e))?,
        usize::from_str_radix(&caps[2], 10)
            .map_err(|e| format_err!("Failed to parse usize number: {}", e))?,
        caps.get(3)
            .map(|v| usize::from_str_radix(v.as_str(), 10).map(Some))
            .unwrap_or(Ok(None))?,
    ))
}

fn parse_uiou<S: AsRef<str>>(s: S) -> Fallible<(usize, isize, Option<usize>)> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^(\d+) ([-0-9]+)(?: (\d+))?$"#).unwrap();
    }
    let caps = RE.captures(s.as_ref()).ok_or_else(|| {
        format_err!(
            "Failed to match an (usize, isize, Option<usize>) tuple, {:?}",
            s.as_ref()
        )
    })?;
    Ok((
        usize::from_str_radix(&caps[1], 10)
            .map_err(|e| format_err!("Failed to parse usize number: {}", e))?,
        isize::from_str_radix(&caps[2], 10)
            .map_err(|e| format_err!("Failed to parse isize number: {}", e))?,
        caps.get(3)
            .map(|v| usize::from_str_radix(v.as_str(), 10).map(Some))
            .unwrap_or(Ok(None))?,
    ))
}

fn parse_qstring<S: AsRef<str>>(s: S) -> Fallible<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^"([^"]*)"$"#).unwrap();
    }
    let caps = RE
        .captures(s.as_ref())
        .ok_or_else(|| format_err!("Failed to match a quoted string: {}", s.as_ref()))?;
    Ok(caps[1].to_owned())
}

fn parse_k_line_v14<S: AsRef<str>>(s: S) -> Fallible<Vec<KEntry>> {
    parse_k_line(s, 14)
}

fn parse_k_line_v19<S: AsRef<str>>(s: S) -> Fallible<Vec<KEntry>> {
    parse_k_line(s, 19)
}

fn parse_k_line<S: AsRef<str>>(s: S, version: usize) -> Fallible<Vec<KEntry>> {
    let mut parts = s.as_ref().split_ascii_whitespace();
    let mut ks = Vec::new();
    while let Some(x) = parts.next() {
        ks.push(match x {
            "f" => match parts.next() {
                Some(x) => KEntry::F(usize::from_str_radix(x, 10)?),
                _ => bail!("Failed to parse F-type K-entry"),
            },
            "o" => KEntry::O,
            "n" => KEntry::N,
            "s" => KEntry::S,
            "k" => {
                let to_take = if version >= 19 { 24 } else { 23 };
                let values = parts.by_ref().take(to_take).collect::<Vec<_>>();
                if values.len() != to_take {
                    bail!("Failed to parse K-type K-entry");
                }
                let w = usize::from_str_radix(values[0], 10)?;
                let h = usize::from_str_radix(values[1], 10)?;
                let tail: Result<Vec<isize>, _> = values[2..]
                    .iter()
                    .map(|s| isize::from_str_radix(s, 10))
                    .collect();
                let mut tail = tail?;
                let last = if version >= 19 { tail.pop() } else { None };
                KEntry::K { w, h, tail, last }
            }
            _ => bail!("Unexpected K-entry {} encountered in {}", x, s.as_ref()),
        });
    }

    Ok(ks)
}

fn parse_dentry<S: AsRef<str>>(s: S) -> Fallible<DEntry> {
    /*
    58 57 0 "
    36 41 2.26893 "mapboss"
    */
    
    use std::str::FromStr;
    lazy_static! {
        static ref RE: Regex = Regex::new(r#"^(\d+) (\d+) ([-0-9.e]+) "([^"]*)"$"#).unwrap();
    }

    let caps = RE
        .captures(s.as_ref())
        .ok_or_else(|| format_err!("Failed to match a D-entry line: {:?}", s.as_ref()))?;
    let x = usize::from_str_radix(&caps[1], 10)
        .map_err(|e| format_err!("Failed to parse x: {}: {:?}", e, s.as_ref()))?;
    let y = usize::from_str_radix(&caps[2], 10)
        .map_err(|e| format_err!("Failed to parse y: {}: {:?}", e, s.as_ref()))?;
    let h = f32::from_str(&caps[3])
        .map_err(|e| format_err!("Failed to parse h: {}: {:?}", e, s.as_ref()))?;
    let key = caps[4].to_owned();

    Ok(DEntry { x, y, h, key })
}

// fn parse_doodad_v14<S: AsRef<str>>(s: S) -> Fallible<Doodad> {
//     parse_doodad(s, 14)
// }

// fn parse_doodad_v18<S: AsRef<str>>(s: S) -> Fallible<Doodad> {
//     parse_doodad(s, 18)
// }

// fn parse_doodad_v25<S: AsRef<str>>(s: S) -> Fallible<Doodad> {
//     parse_doodad(s, 25)
// }

// fn parse_doodad<S: AsRef<str>>(s: S, version: usize) -> Fallible<Doodad> {
//     /*
//     v14-ish:
//     60 15 6.19592 0 1 -7.8125 1 "Metadata/Terrain/Doodads/Forest/Gravestones/graveyard_fence_door_v01_01.ao" "Metadata/MiscellaneousObjects/Doodad"
//     v18:
//     20 80 -0.291014 0 0 -0.144994 0.989433 0 1 0 1 "Metadata/Terrain/Doodads/Beach/kolik_damaged_v01_01.ao" "Metadata/MiscellaneousObjects/Doodad"
//     v23: (same layout as v18, just for reference)
//     93 77 -0 0 0 0 1 0 1 -179.348 1.05 "Metadata/Terrain/Doodads/RuinedCity/Trees/OldTree_v01_03.ao" "Metadata/MiscellaneousObjects/Doodad"
//     v25:
//     218 233 2.94506 0 0 0.995176 0.0981067 0 0 1 -21.7391 1 "Metadata/Terrain/Doodads/Cave/treasure_pile_19.ao" "Metadata/MiscellaneousObjects/TreasurePile"
//     */
//     use std::str::FromStr;
//     lazy_static! {
//         static ref RE_HEAD: Regex = Regex::new(r#"^(\d+) (\d+) ([-0-9.e]+) (.*)$"#).unwrap();
//         static ref RE_V14_MID: Regex = Regex::new(r#"^(\d+) (\d+) (.*)$"#).unwrap();
//         static ref RE_V18_MID: Regex =
//             Regex::new(r#"^([-0-9.e]+) ([-0-9.e]+) ([-0-9.e]+) ([-0-9.e]+) (\d+) (\d+) (.*)$"#)
//                 .unwrap();
//         static ref RE_V25_MID: Regex = Regex::new(
//             r#"^([-0-9.e]+) ([-0-9.e]+) ([-0-9.e]+) ([-0-9.e]+) (\d+) (\d+) (\d+) (.*)$"#
//         )
//         .unwrap();
//         static ref RE_TAIL: Regex = Regex::new(r#"^([-0-9.e]+) "([^"]*)"\s+"([^"]*)"$"#).unwrap();
//     }

//     let caps_head = RE_HEAD
//         .captures(s.as_ref())
//         .ok_or_else(|| format_err!("Failed to match a doodad line head: {:?}", s.as_ref()))?;

//     let x = usize::from_str_radix(&caps_head[1], 10)
//         .map_err(|e| format_err!("Failed to parse x: {}: {:?}", e, s.as_ref()))?;
//     let y = usize::from_str_radix(&caps_head[2], 10)
//         .map_err(|e| format_err!("Failed to parse y: {}: {:?}", e, s.as_ref()))?;
//     let h = f32::from_str(&caps_head[3])
//         .map_err(|e| format_err!("Failed to parse h: {}: {:?}", e, s.as_ref()))?;
//     let s = &caps_head[4];

//     // branch on whether we're v14+, v18+, or v25+ for the start of the thing
//     let (s, r, q1, q1_5, q2) = if version < 18 {
//         let caps_mid = RE_V14_MID
//             .captures(s.as_ref())
//             .ok_or_else(|| format_err!("Failed to match a doodad v14 mid: {:?}", s))?;
//         let q1 = usize::from_str_radix(&caps_mid[1], 10)
//             .map_err(|e| format_err!("Failed to parse q1: {}: {:?}", e, s))?;
//         let q2 = usize::from_str_radix(&caps_mid[2], 10)
//             .map_err(|e| format_err!("Failed to parse q2: {}: {:?}", e, s))?;
//         (caps_mid[3].to_owned(), None, q1, None, q2)
//     } else if version < 25 {
//         let caps_mid = RE_V18_MID
//             .captures(s.as_ref())
//             .ok_or_else(|| format_err!("Failed to match a doodad v18 mid: {:?}", s))?;
//         let r1 = f32::from_str(&caps_mid[1])
//             .map_err(|e| format_err!("Failed to parse r1: {}: {:?}", e, s))?;
//         let r2 = f32::from_str(&caps_mid[2])
//             .map_err(|e| format_err!("Failed to parse r2: {}: {:?}", e, s))?;
//         let r3 = f32::from_str(&caps_mid[3])
//             .map_err(|e| format_err!("Failed to parse r3: {}: {:?}", e, s))?;
//         let r4 = f32::from_str(&caps_mid[4])
//             .map_err(|e| format_err!("Failed to parse r4: {}: {:?}", e, s))?;
//         let r = Quaternion::new(r1, r2, r3, r4);
//         let q1 = usize::from_str_radix(&caps_mid[5], 10)
//             .map_err(|e| format_err!("Failed to parse q1: {}: {:?}", e, s))?;
//         let q2 = usize::from_str_radix(&caps_mid[6], 10)
//             .map_err(|e| format_err!("Failed to parse q2: {}: {:?}", e, s))?;
//         (caps_mid[7].to_owned(), Some(r), q1, None, q2)
//     } else {
//         let caps_mid = RE_V25_MID
//             .captures(s.as_ref())
//             .ok_or_else(|| format_err!("Failed to match a doodad v25 mid: {:?}", s))?;
//         let r1 = f32::from_str(&caps_mid[1])
//             .map_err(|e| format_err!("Failed to parse r1: {}: {:?}", e, s))?;
//         let r2 = f32::from_str(&caps_mid[2])
//             .map_err(|e| format_err!("Failed to parse r2: {}: {:?}", e, s))?;
//         let r3 = f32::from_str(&caps_mid[3])
//             .map_err(|e| format_err!("Failed to parse r3: {}: {:?}", e, s))?;
//         let r4 = f32::from_str(&caps_mid[4])
//             .map_err(|e| format_err!("Failed to parse r4: {}: {:?}", e, s))?;
//         let r = Quaternion::new(r1, r2, r3, r4);
//         let q1 = usize::from_str_radix(&caps_mid[5], 10)
//             .map_err(|e| format_err!("Failed to parse q1: {}: {:?}", e, s))?;
//         let q1_5 = usize::from_str_radix(&caps_mid[6], 10)
//             .map_err(|e| format_err!("Failed to parse q1: {}: {:?}", e, s))?;
//         let q2 = usize::from_str_radix(&caps_mid[7], 10)
//             .map_err(|e| format_err!("Failed to parse q2: {}: {:?}", e, s))?;
//         (caps_mid[8].to_owned(), Some(r), q1, Some(q1_5), q2)
//     };

//     let (s, q2s) = {
//         let mut strs: Vec<&str> = s.splitn(q2 + 1, " ").collect();
//         if strs.len() != q2 + 1 {
//             bail!("q2 count {} too large: {:?}", q2, s);
//         }
//         let s = strs.pop().unwrap();
//         let v: Fallible<Vec<f32>> = strs
//             .iter()
//             .enumerate()
//             .map(|(i, s)| {
//                 f32::from_str(s).map_err(|e| format_err!("Failed to parse q2s[{}]: {}", i, e))
//             })
//             .collect();
//         (s, v?)
//     };

//     let caps_tail = RE_TAIL
//         .captures(s)
//         .ok_or_else(|| format_err!("Failed to match a doodad line tail, {:?}", s))?;

//     let q3 = f32::from_str(&caps_tail[1])
//         .map_err(|e| format_err!("Failed to parse q3: {}: {:?}", e, s))?;

//     let ao = caps_tail[2].to_owned();
//     let spec = caps_tail[3].to_owned();

//     Ok(Doodad {
//         x,
//         y,
//         h,
//         r,
//         q1,
//         q1_5,
//         q2s,
//         q3,
//         ao,
//         spec,
//     })
// }

// fn parse_decal_v14<S: AsRef<str>>(s: S) -> Fallible<Decal> {
//     parse_decal(s, 14)
// }

// fn parse_decal_v18<S: AsRef<str>>(s: S) -> Fallible<Decal> {
//     parse_decal(s, 18)
// }

// fn parse_decal<S: AsRef<str>>(s: S, version: usize) -> Fallible<Decal> {
//     /*
//     183.781 616.065 1.5708 100  "Metadata/Decals/bloodtrail.atlas" "straight"
//     689.944 608.471 1.65806 100  "Metadata/Decals/bloodtrail.atlas" "straight"
//     1890.12 1886.69 2.35619 0 100  "Metadata/Decals/bloodtrailold.atlas" "curve"
//     */
//     use std::str::FromStr;
//     lazy_static! {
//         static ref RE_V14: Regex =
//             Regex::new(r#"^(?P<x>[-0-9.e]+) (?P<y>[-0-9.e]+) (?P<h>[-0-9.e]+) (?P<s>[-0-9.e]+)\s+"(?P<atlas>[^"]*)" "(?P<key>[^"]*)"$"#)
//                 .unwrap();
//         static ref RE_V18: Regex = Regex::new(
//             r#"^(?P<x>[-0-9.e]+) (?P<y>[-0-9.e]+) (?P<h>[-0-9.e]+) (?P<q1>\d+) (?P<s>[-0-9.e]+)\s+"(?P<atlas>[^"]*)" "(?P<key>[^"]*)"$"#
//         )
//         .unwrap();
//     }

//     let caps = if version >= 18 {
//         RE_V18.captures(s.as_ref())
//     } else {
//         RE_V14.captures(s.as_ref())
//     }
//     .ok_or_else(|| format_err!("Failed to match a decal line: {:?}", s.as_ref()))?;

//     let x = f32::from_str(&caps["x"])
//         .map_err(|e| format_err!("Failed to parse x: {}: {:?}", e, s.as_ref()))?;
//     let y = f32::from_str(&caps["y"])
//         .map_err(|e| format_err!("Failed to parse y: {}: {:?}", e, s.as_ref()))?;
//     let h = f32::from_str(&caps["h"])
//         .map_err(|e| format_err!("Failed to parse h: {}: {:?}", e, s.as_ref()))?;
//     let q1 = caps
//         .name("q1")
//         .map(|v| usize::from_str_radix(v.as_str(), 10))
//         .unwrap_or(Ok(0))?;
//     let s = f32::from_str(&caps["s"])
//         .map_err(|e| format_err!("Failed to parse s: {}: {:?}", e, s.as_ref()))?;
//     let atlas = caps["atlas"].to_owned();
//     let key = caps["key"].to_owned();

//     Ok(Decal {
//         x,
//         y,
//         h,
//         q1,
//         s,
//         atlas,
//         key,
//     })
// }

struct ArmParseHelper<'a, I: Iterator<Item = &'a str>> {
    lines: I,
}

impl<'a, I> ArmParseHelper<'a, I>
where
    I: Iterator<Item = &'a str>,
{
    fn try_parse<F, T>(&mut self, parser: F, label: &str) -> Fallible<T>
    where
        F: FnOnce(&'a str) -> Fallible<T>,
    {
        self.lines
            .next()
            .ok_or_else(|| format_err!("No {} string to parse", label))
            .and_then(parser)
    }

    fn version(&mut self) -> Fallible<usize> {
        self.try_parse(parse_version, "version")
    }

    fn gtets(&mut self) -> Fallible<Vec<String>> {
        let gtet_count = self.try_parse(parse_usize, "usize")?;
        (0..gtet_count)
            .map(|i| {
                self.try_parse(parse_qstring, "quoted string")
                    .map_err(|e| format_err!("gtets[{}]: {}", i, e))
            })
            .collect()
    }

    fn dims(&mut self) -> Fallible<(usize, usize, Option<usize>)> {
        self.try_parse(parse_dims, "dims")
            .map_err(|e| format_err!("dims: {}", e))
    }

    fn a_upair(&mut self) -> Fallible<(usize, usize)> {
        self.try_parse(parse_upair, "upair")
            .map_err(|e| format_err!("a_upair: {}", e))
    }

    fn name(&mut self) -> Fallible<String> {
        self.try_parse(parse_qstring, "quoted string")
            .map_err(|e| format_err!("name: {}", e))
    }

    fn b_upair(&mut self, version: usize) -> Fallible<(usize, Option<usize>)> {
        if version >= 16 {
            let (a, b) = self
                .try_parse(parse_upair, "upair")
                .map_err(|e| format_err!("b_upair: {}", e))?;
            Ok((a, Some(b)))
        } else {
            let a = self
                .try_parse(parse_usize, "usize")
                .map_err(|e| format_err!("b_usize: {}", e))?;
            Ok((a, None)) // TODO: Determine what these mean and if there should be a default value
        }
    }

    fn k_1_by_1(&mut self, version: usize) -> Fallible<KEntry> {
        Ok(self
            .kline(version)
            .map_err(|e| format_err!("k_1_by_1: {}", e))?[0]
            .clone())
    }

    fn k_x_by_y(&mut self, version: usize, _w: usize, h: usize) -> Fallible<Vec<Vec<KEntry>>> {
        (0..h)
            .map(|i| {
                self.kline(version)
                    .map_err(|e| format_err!("k_x_by_y[{}]: {}", i, e))
            })
            .collect()
    }

    fn kline(&mut self, version: usize) -> Fallible<Vec<KEntry>> {
        self.try_parse(
            if version >= 19 {
                parse_k_line_v19
            } else {
                parse_k_line_v14
            },
            "k-line",
        )
    }

    fn c_uious(&mut self, a: (usize, usize)) -> Fallible<Vec<(usize, isize, Option<usize>)>> {
        let c_count = 2 * (a.0 + a.1);

        (0..c_count)
            .map(|i| {
                self.try_parse(parse_uiou, "uiou")
                    .map_err(|e| format_err!("c_uious[{}]: {}", i, e))
            })
            .collect()
    }

    fn dentry(&mut self, label: &str) -> Fallible<Vec<DEntry>> {
        let count = self
            .try_parse(parse_usize, "usize")
            .map_err(|e| format_err!("{} d-line count: {}", label, e))?;

        (0..count)
            .map(|_| self.try_parse(parse_dentry, label))
            .collect()
    }

    fn dentries(&mut self, version: usize) -> Fallible<Vec<Vec<DEntry>>> {
        if version >= 29 {
            ["d1", "d2", "d3", "d4", "d5", "d6"]
                .iter()
                .map(|label| self.dentry(label))
                .collect()
        } else if version >= 26 {
            ["d1", "d2", "d3", "d4", "d5"]
                .iter()
                .map(|label| self.dentry(label))
                .collect()
        } else if version >= 20 {
            [
                "mob_pack", "chest", "d3", "trigger", "d5", "d6", "d7", "d8", "d9", "d10",
            ]
            .iter()
            .map(|label| self.dentry(label))
            .collect()
        } else {
            [
                "mob_pack", "chest", "d3", "trigger", "d5", "d6", "d7", "d8", "d9",
            ]
            .iter()
            .map(|label| self.dentry(label))
            .collect()
        }
    }

    // fn doodads(&mut self, version: usize) -> Fallible<Vec<Doodad>> {
    //     let count = self
    //         .try_parse(parse_usize, "usize")
    //         .map_err(|e| format_err!("doodad count: {}", e))?;

    //     (0..count)
    //         .map(|_| {
    //             self.try_parse(
    //                 if version >= 25 {
    //                     parse_doodad_v25
    //                 } else if version >= 18 {
    //                     parse_doodad_v18
    //                 } else {
    //                     parse_doodad_v14
    //                 },
    //                 "doodad",
    //             )
    //         })
    //         .collect()
    // }

    // fn e0_usize(&mut self, version: usize) -> Fallible<Option<usize>> {
    //     Ok(if version >= 26 {
    //         Some(
    //             self.try_parse(parse_usize, "usize")
    //                 .map_err(|e| format_err!("e0_usize: {}", e))?,
    //         )
    //     } else {
    //         None
    //     })
    // }

    // fn decals(&mut self, version: usize) -> Fallible<Vec<Decal>> {
    //     let count = self
    //         .try_parse(parse_usize, "usize")
    //         .map_err(|e| format_err!("decal_count: {}", e))?;

    //     (0..count)
    //         .map(|_| {
    //             self.try_parse(
    //                 if version >= 18 {
    //                     parse_decal_v18
    //                 } else {
    //                     parse_decal_v14
    //                 },
    //                 "decal",
    //             )
    //         })
    //         .collect()
    // }

    // fn e1_usize(&mut self, version: usize) -> Fallible<Option<usize>> {
    //     Ok(if version >= 15 {
    //         Some(
    //             self.try_parse(parse_usize, "usize")
    //                 .map_err(|e| format_err!("e1_usize: {}", e))?,
    //         )
    //     } else {
    //         None
    //     })
    // }

    // fn e2_usize(&mut self, version: usize) -> Fallible<Option<usize>> {
    //     Ok(if version >= 23 {
    //         Some(
    //             self.try_parse(parse_usize, "usize")
    //                 .map_err(|e| format_err!("e2_usize: {}", e))?,
    //         )
    //     } else {
    //         None
    //     })
    // }

    fn raw_line(&mut self) -> Option<String> {
        self.lines.next().map(ToOwned::to_owned)
    }
}

pub fn parse_arm(s: &str) -> Fallible<Arm> {
    let lines = s.lines().filter(|x| !x.is_empty());
    let mut helper = ArmParseHelper { lines };

    let version = helper.version()?;
    let gtets = helper.gtets()?;
    let dims = helper.dims()?;
    let a_upair = helper.a_upair()?;
    let name = helper.name()?;
    let b_upair = helper.b_upair(version)?;
    let k_1_by_1 = helper.k_1_by_1(version)?;
    let c_uious = helper.c_uious(a_upair)?;
    let dentries = helper.dentries(version)?;

    let (w, h) = match k_1_by_1 {
        KEntry::K { w, h, .. } => (w, h),
        KEntry::S => (1, 1),
        _ => bail!("k_1_by_1 is not of K-type or S-type"),
    };
    let dim_wh = (dims.0 * a_upair.0, dims.1 * a_upair.1);
    if w > dim_wh.0 || h > dim_wh.1 {
        bail!(
            "dims on some axis from k {:?} exceeds dims*scale {:?}",
            (w, h),
            dim_wh
        );
    }

    let k_x_by_y = helper.k_x_by_y(version, w, h)?;

    let mut whole_tail = Vec::new();
    while let Some(line) = helper.raw_line() {
        whole_tail.push(line);
    }

    // let doodads = helper.doodads(version)?;
    // let decals = helper.decals(version)?;
    // let more_decals = if version >= 15 {
    //     helper.decals(version)?
    // } else {
    //     vec![]
    // };
    // let e2_usize = helper.e2_usize(version)?;
    // let tail_line = {
    //     // number of elements are (w - 1)(h - 1)
    //     // b_upair.1 == 0 seemed like a good indication of presence
    //     // but did not hold universally.
    //     // see Metadata/Terrain/EndGame/MapBlackguard/Rooms/room_1.arm
    //     // for a huge example
    //     helper.raw_line()
    // };

    // // match &tail_line {
    // //     Some(line) => {
    // //         let n = line
    // //             .split_ascii_whitespace()
    // //             .filter(|s| !s.is_empty())
    // //             .count();
    // //         if n != (w - 1) * (h - 1) {
    // //             bail!(
    // //                 "tail line element count {} not equal to {} ({} - 1)({} - 1)",
    // //                 n,
    // //                 (w - 1) * (h - 1),
    // //                 w,
    // //                 h
    // //             );
    // //         }
    // //     }
    // //     _ => {}
    // // }

    // if let Some(line) = helper.raw_line() {
    //     bail!("Unexpected line at end of content: {:?}", line);
    // }

    Ok(Arm {
        version,
        gtets,
        dims,
        a_upair,
        name,
        b_upair,
        k_1_by_1,
        c_uious,
        dentries,
        k_x_by_y,
        whole_tail,
        // doodads,
        // decals,
        // more_decals,
        // e2_usize,
        // tail_line,
        // ..Default::default()
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(not(windows))]
    const REFERENCE_PATH: &str = r#"/mnt/c/Temp/GGPKs/Content-2019-03-31.ggpk"#;
    // #[cfg(not(windows))]
    // const DUMP_PATH: &str = r#"/mnt/c/Temp/"#;

    #[cfg(windows)]
    const REFERENCE_PATH: &str = r#"C:\Temp\GGPKs\Content-2019-03-31.ggpk"#;
    // #[cfg(windows)]
    // const DUMP_PATH: &str = r#"C:\Temp\"#;

    fn find_full_path(
        pack: &crate::formats::ggpk::Ggpk,
        relations: &std::collections::HashMap<u64, u64>,
        offset: u64,
    ) -> String {
        use crate::formats::ggpk::NodeInfo;
        let mut offset = offset;
        let mut parts = Vec::new();
        while let Ok(x) = pack.resolve(offset) {
            let name = x.name();
            if !name.is_empty() {
                parts.push(x.name());
            }
            if let Some(next) = relations.get(&offset) {
                offset = *next;
            } else {
                break;
            }
        }
        parts.reverse();
        parts.join("/")
    }

    #[test]
    fn test_arm_v() -> Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use std::collections::HashMap;
        use std::sync::Arc;
        let path: PathBuf = std::env::var("POE_REFERENCE_PATH").unwrap_or_else(|_| REFERENCE_PATH.to_owned()).into();
        let pack = ggpk::Ggpk::open(&path)?;

        let relations = {
            let mut rel = HashMap::new();
            let mut fh =
                std::io::BufReader::new(std::fs::File::open("all_u64_offset_u64_parent.bin")?);
            while let Ok(offset) = fh.read_u64::<LE>() {
                if let Ok(parent) = fh.read_u64::<LE>() {
                    rel.insert(offset, parent);
                }
            }
            Arc::new(rel)
        };

        let (results_tx, results_rx) =
            std::sync::mpsc::sync_channel::<(usize, usize, u64, Fallible<super::Arm>)>(128);
        let t_pack = pack.clone();
        let t_rel = relations.clone();
        let t = std::thread::spawn(move || {
            let pack = t_pack;
            let relations = t_rel;

            let mut failures = Vec::new();
            while let Ok((i, version, offset, res)) = results_rx.recv() {
                match res {
                    Err(e) => {
                        let full_path = find_full_path(&pack, &relations, offset);
                        failures.push((i, version, full_path, e));
                    }
                    Ok(_arm) => {}
                }
            }
            failures.sort_by(|a, b| a.1.cmp(&b.1));

            for (_i, version, full_path, e) in failures {
                eprintln!("Failed to parse v{} ARM {}\n{}", version, full_path, e);
            }
        });

        use byteorder::{ReadBytesExt, LE};
        use rayon::iter::ParallelBridge;
        use rayon::prelude::ParallelIterator;
        let list_fh = std::io::BufReader::new(std::fs::File::open("arm_u64_offset.bin")?);

        use itertools::unfold;
        unfold(list_fh, |fh| fh.read_u64::<LE>().ok())
            .enumerate()
            .par_bridge()
            .try_for_each(|(i, offset)| -> Fallible<()> {
                if let ggpk::ResolvedEntry::File(file) = &pack.resolve(offset)? {
                    let filename = file.name();
                    if filename.ends_with(".arm") {
                        let full_name = find_full_path(&pack, &relations, offset);
                        let contents = file.contents();
                        let is_arm = contents.starts_with(&[
                            0xFF, 0xFE, b'v', 0x00, b'e', 0x00, b'r', 0x00, b's', 0x00, b'i', 0x00,
                            b'o', 0x00, b'n', 0x00, b' ', 0x00,
                        ]);
                        if !is_arm {
                            return Ok(());
                        }
                        let version_part = &contents.get(18..22);
                        // match version_part {
                        //     _ => return Ok(()),
                        // }
                        let version_part = version_part.unwrap();
                        let version = (version_part[0] - b'0') * 10 + (version_part[2] - b'0');
                        let v = crate::util::vec_to_utf16(&contents)
                            .ok_or_else(|| format_err!("ARM file {} not UTF-16", &full_name))?;
                        let pa = super::parse_arm(&v);
                        results_tx.send((i, version as usize, offset, pa))?;
                    }
                }
                Ok(())
            })?;
        eprintln!("boop!");
        drop(results_tx);
        t.join().unwrap();
        eprintln!("beep!");
        Ok(())
    }
}
