mod types;
pub use types::*;

pub fn parse_mat(data: &[u8]) -> failure::Fallible<Mat> {
    let data = crate::util::try_decompress(&data);
    if let Some(text) = crate::util::vec_to_utf16(&data) {
        if text.starts_with('{') {
            Ok(Mat::V4(MatV4::parse(&text)?.into()))
        }
        else {
            Ok(Mat::V3(MatV3::parse(&text)?.into()))
        }
    }
    else {
        bail!("Material file not UTF-16");
    }
}

impl MatV3 {
    pub fn parse(contents: &str) -> failure::Fallible<MatV3> {
        use regex::Regex;
        lazy_static! {
            static ref SPLIT_RE: Regex = Regex::new(r#"(\S+)(?:\s+)?(.*)"#).unwrap();
            static ref SCALAR_RE: Regex = Regex::new(r#"([-+.0-9e]+)"#).unwrap();
            static ref QS_RE: Regex = Regex::new(r#""([^"]*)""#).unwrap();
            static ref QSPAIR_RE: Regex = Regex::new(r#""([^"]*)"(?:\s+)"([^"]*)""#).unwrap();
        }
        let mut lines = contents.lines().map(|s| s.trim()).filter(|x| !x.is_empty());
        let mut ret: MatV3 = Default::default();
        {
            let version_line = lines.next().ok_or_else(|| format_err!("Version line missing."))?;
            let caps = SPLIT_RE.captures(&version_line).ok_or_else(|| format_err!("Version line malformed."))?;
            if &caps[1] != "Version" {
                bail!("First line not a Version line.");
            }
            ret.version = caps[2].parse()?;
        };
        for line in lines {
            let caps = SPLIT_RE.captures(&line).ok_or_else(|| format_err!("Line malformed."))?;
            match &caps[1] {
                "AddressU" => ret.address_u = Some(caps[2].to_owned()),
                "AddressV" => ret.address_v = Some(caps[2].to_owned()),
                "BlendMode" => ret.blend_mode = caps[2].to_owned(),
                "ColourTexture" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Colour texture line malformed."))?;
                    ret.colour_texture = Some(caps[1].to_owned());
                }
                "Effect" => {
                    let caps = QSPAIR_RE.captures(&caps[2]).ok_or_else(|| format_err!("Effect line malformed."))?;
                    ret.effects.insert(caps[1].to_owned(), caps[2].to_owned());
                }
                "Graph" => {
                    let json: serde_json::Value = serde_json::from_str(&caps[2].to_owned())?;
                    ret.graph = Some(json);
                }
                "MinimapColour" => ret.minimap_colour = Some(caps[2].to_owned()),
                "MinimapOnly" => ret.minimap_only = Some(()),
                "NormalSpecularTexture" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Normal-specular texture line malformed."))?;
                    ret.normal_specular_texture = Some(caps[1].to_owned());
                }
                "NormalTexture" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Normal texture line malformed."))?;
                    ret.normal_texture = Some(caps[1].to_owned());
                }
                "RoofSection" => ret.roof_section = Some(()),
                "ShaderType" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Shader type line malformed."))?;
                    ret.shader_type = Some(caps[1].to_owned());
                }
                "SourceTextures" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Source textures line malformed."))?;
                    ret.source_textures = caps[1].to_owned();
                }
                "SpecularExponent" => ret.specular_exponent = Some(caps[2].parse()?),
                "SpecularTexture" => {
                    let caps = QS_RE.captures(&caps[2]).ok_or_else(|| format_err!("Specular texture line malformed."))?;
                    ret.specular_texture = Some(caps[1].to_owned());
                }
                "Tiled" => ret.tiled = Some(()),
                _ => {},
            }
        }
        Ok(ret)
    }
}

impl MatV4 {
    pub fn parse(contents: &str) -> failure::Fallible<MatV4> {
        Ok(MatV4 {
            payload: serde_json::from_str(contents)?,
        })
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_all_mat() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;

        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|scope| {
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                scope.spawn(move |_| {
                    file_rx.iter().par_bridge().for_each(|(_dir, file): (ggpk::Directory, ggpk::File)| {
                        if file.name().to_lowercase().ends_with(".mat") {
                            let res = super::parse_mat(file.contents());
                            match res {
                                Ok(mat) => {
                                    match mat {
                                        super::Mat::V3(_mat) => {
                                            // if let Some(graph) = &mat.graph {
                                            //     println!("{:?}", graph);
                                            // }
                                        }
                                        super::Mat::V4(_mat) => {
                                            // eprintln!("{}", serde_json::to_string_pretty(&mat.payload).unwrap());
                                        },
                                    }
                                }
                                // _ => {}
                                Err(e) => eprintln!("{}", e),
                            }
                        }
                    });
                });
            }
            crate::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
        }).unwrap();
        Ok(())
    }

    #[test]
    fn identify_mat_keys() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;
        use std::collections::{BTreeMap, BTreeSet};

        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|scope| {
            let (all_keys_tx, all_keys_rx) = crossbeam::channel::unbounded();
            let (key_tx, key_rx) = crossbeam::channel::unbounded();
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                scope.spawn(move |_| {
                    file_rx.iter().par_bridge().for_each(|(_dir, file): (ggpk::Directory, ggpk::File)| {
                        if file.name().to_lowercase().ends_with(".mat") {
                            if let Some(text) = crate::util::vec_to_utf16(file.contents()) {
                                let lines = text.lines();
                                let mut all_keys = BTreeSet::new();
                                lines.for_each(|line| {
                                    let mut parts = line.splitn(2, ' ').filter(|s| !s.is_empty());
                                    let key = parts.next().unwrap();
                                    let value = parts.next();
                                    all_keys.insert(key.to_owned());
                                    key_tx.send((key.to_owned(), value.map(|x| x.to_owned()))).unwrap();
                                });
                                all_keys_tx.send(all_keys).unwrap();
                            }
                        }
                    });
                });
            }
            crate::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
            let keys = key_rx.iter().fold(<BTreeMap<String, BTreeSet<Option<String>>>>::new(), |mut t, (key, value)| {
                t.entry(key).or_default().insert(value);
                t
            });
            for (k, v) in keys.iter() {
                eprintln!("{} : {:?}", k, v.iter().next());
            }
            let mut ak_iter = all_keys_rx.iter();
            let first_set = ak_iter.next().unwrap();
            let common = ak_iter.fold(first_set, |set, keys| {
                set.intersection(&keys).cloned().collect()
            });
            eprintln!("common: {:#?}", common);
        }).unwrap();
        Ok(())
    }
}