use std::collections::BTreeMap;

#[derive(Debug, Default)]
pub struct MatV3 {
    pub address_u: Option<String>,
    pub address_v: Option<String>,
    pub blend_mode: String,
    pub colour_texture: Option<String>,
    pub effects: BTreeMap<String, String>,
    pub graph: Option<serde_json::Value>,
    pub minimap_colour: Option<String>,
    pub minimap_only: Option<()>,
    pub normal_specular_texture: Option<String>,
    pub normal_texture: Option<String>,
    pub roof_section: Option<()>,
    pub shader_type: Option<String>,
    pub source_textures: String,
    pub specular_exponent: Option<f32>,
    pub specular_texture: Option<String>,
    pub tiled: Option<()>,
    pub version: usize,
}

#[derive(Debug, Default)]
pub struct MatV4 {
    pub payload: serde_json::Value,
}

#[derive(Debug)]
pub enum Mat {
    V3(Box<MatV3>),
    V4(Box<MatV4>),
}