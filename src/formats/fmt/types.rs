use half::f16;

#[derive(Debug)]
pub struct Fmt {
    pub version: u8,
    pub d: [u8; 4],
    pub bbox: [f32; 6],
    pub shapes: Vec<Shape>,
    pub d0s: Vec<D0>,
    pub indices: Indices,
    pub vertices: Vec<Vertex>,
    pub d1s: Vec<D1>,
    pub d3s: Vec<D3>,
    pub string_table: Vec<String>,
    pub string_data: Vec<u16>,
}

#[derive(Debug)]
pub struct Shape {
    pub name_offset: u32,
    pub material_offset: u32,
    pub triangle_offset: u32,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub unknown: [u8; 8],
    pub uv: [f16; 2],
}

#[derive(Debug)]
pub enum Indices {
    Short(Vec<u16>),
    Long(Vec<u32>),
}

impl Default for Indices {
    fn default() -> Self {
        Indices::Short(vec![])
    }
}

#[derive(Debug)]
pub struct D0 {
    pub unk0: [u8; 6],
}

#[derive(Debug)]
pub struct D1 {
    pub unk0: [u8; 8],
    pub f8: f32,
}

#[derive(Debug)]
pub struct D3 {
    pub x: [u8; 4],
    pub f: [f32; 12],
    pub unk: [u8; 26],
}
