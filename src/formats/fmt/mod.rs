mod types;
pub use types::*;

use crate::util::survey;

use byteorder::{ReadBytesExt, LE};
use std::io::Read;

pub fn parse_fmt(data: &[u8]) -> failure::Fallible<Fmt> {
    let mut reader = &data[..];
    let version = reader.read_u8()?;
    if version != 4 {
        bail!("FMT file not version 4");
    }

    let triangle_count = reader.read_u32::<LE>()? as usize;
    let vertex_count = reader.read_u32::<LE>()? as usize;
    let shape_count = reader.read_u16::<LE>()? as usize;
    let d = {
        let mut res = [0u8; 4];
        reader.read_exact(&mut res)?;
        res
    };
    let bbox = {
        let mut res = [0.0f32; 6];
        reader.read_f32_into::<LE>(&mut res)?;
        res
    };

    let shapes = {
        let mut res = Vec::with_capacity(shape_count);
        for _ in 0..shape_count {
            res.push(Shape {
                name_offset: reader.read_u32::<LE>()?,
                material_offset: reader.read_u32::<LE>()?,
                triangle_offset: reader.read_u32::<LE>()?,
            });
        }
        res
    };

    let d0s = {
        let mut v = Vec::with_capacity(d[0] as usize);
        for _ in 0..d[0] as usize {
            let mut unk0 = [0u8; 6];
            reader.read_exact(&mut unk0)?;
            v.push(D0 { unk0 });
        }
        v
    };

    let indices = if vertex_count > 0xFFFF {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        reader.read_u32_into::<LE>(&mut v)?;
        Indices::Long(v)
    } else {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        reader.read_u16_into::<LE>(&mut v)?;
        Indices::Short(v)
    };

    let vertices = {
        let mut v = Vec::with_capacity(vertex_count);
        for _ in 0..vertex_count {
            let mut position = [0f32; 3];
            reader.read_f32_into::<LE>(&mut position)?;
            let mut unknown = [0u8; 8];
            reader.read_exact(&mut unknown)?;
            let mut uv = [half::consts::ZERO; 2];
            crate::util::read_f16_into_le(&mut reader, &mut uv)?;
            v.push(Vertex {
                position,
                unknown,
                uv,
            });
        }
        v
    };

    let d1s = {
        let mut v = Vec::with_capacity(d[1] as usize);
        for _ in 0..d[1] as usize {
            let mut unk0 = [0u8; 8];
            reader.read_exact(&mut unk0)?;
            let f8 = reader.read_f32::<LE>()?;
            v.push(D1 { unk0, f8 });
        }
        v
    };

    let d3s = {
        let mut res = Vec::with_capacity(d[3] as usize);
        for _ in 0..d[3] as usize {
            let mut x = [0u8; 4];
            reader.read_exact(&mut x)?;
            let mut f = [0.0f32; 12];
            reader.read_f32_into::<LE>(&mut f)?;
            let mut unk = [0u8; 26];
            reader.read_exact(&mut unk)?;
            res.push(D3 { x, f, unk });
        }
        res
    };

    let string_table_code_unit_count = reader.read_u32::<LE>()? as usize;
    if string_table_code_unit_count > data.len() {
        bail!(
            "String table size {} larger than input size {}",
            string_table_code_unit_count,
            data.len()
        );
    }

    let string_data = {
        let mut v = vec![];
        v.resize(string_table_code_unit_count, 0);
        reader.read_u16_into::<LE>(&mut v)?;
        v
    };

    let string_table: Vec<String> = {
        string_data
            .split(|x| *x == 0)
            .map(|s| String::from_utf16_lossy(&s))
            .collect()
    };

    if reader.read_u8().is_ok() {
        bail!("Extra data after string table");
    }

    Ok(Fmt {
        version,
        d,
        bbox,
        shapes,
        d0s,
        indices,
        vertices,
        d1s,
        d3s,
        string_table,
        string_data,
    })
}

pub fn probe_fmt_survey(
    file_id: u64,
    data: &[u8],
    survey: &mut survey::Instance,
) -> failure::Fallible<()> {
    use std::io::Cursor;
    let mut reference = survey::Reference::default();
    let mut reader = Cursor::new(&data[..]);

    survey.open_region("header", &mut reader);

    survey.open_region("version", &mut reader);
    let _version = reader.read_u8()?;
    survey.close_region(&mut reader);

    survey.open_region("triangle_count", &mut reader);
    let triangle_count = reader.read_u32::<LE>()? as usize;
    survey.close_region(&mut reader);

    survey.open_region("vertex_count", &mut reader);
    let vertex_count = reader.read_u32::<LE>()? as usize;
    survey.close_region(&mut reader);

    survey.open_region("shape_count", &mut reader);
    let shape_count = reader.read_u16::<LE>()? as usize;
    survey.close_region(&mut reader);

    reference
        .scalars
        .insert("triangle_count".to_owned(), triangle_count as u64);
    reference
        .scalars
        .insert("vertex_count".to_owned(), vertex_count as u64);
    reference
        .scalars
        .insert("shape_count".to_owned(), shape_count as u64);

    survey.open_region("d", &mut reader);
    let d = {
        let mut v = [0u8; 4];
        reader.read_exact(&mut v)?;
        v
    };
    survey.close_region(&mut reader);

    survey.open_region("bbox", &mut reader);
    let _bbox = {
        let mut v = [0.0f32; 6];
        reader.read_f32_into::<LE>(&mut v)?;
        v
    };
    survey.close_region(&mut reader);

    survey.open_region("shapes", &mut reader);
    let shapes = {
        let mut v = Vec::with_capacity(shape_count);
        for _ in 0..shape_count {
            survey.open_region("shape", &mut reader);
            v.push(Shape {
                name_offset: reader.read_u32::<LE>()?,
                material_offset: reader.read_u32::<LE>()?,
                triangle_offset: reader.read_u32::<LE>()?,
            });
            survey.close_region(&mut reader);
        }
        v
    };
    survey.close_region(&mut reader);

    let d0s = {
        let mut v = Vec::with_capacity(d[0] as usize);
        for _ in 0..d[0] as usize {
            let mut unk0 = [0u8; 6];
            reader.read_exact(&mut unk0)?;
            v.push(D0 { unk0 });
        }
        v
    };

    let indices = if vertex_count > 0xFFFF {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        reader.read_u32_into::<LE>(&mut v)?;
        Indices::Long(v)
    } else {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        reader.read_u16_into::<LE>(&mut v)?;
        Indices::Short(v)
    };

    let vertices = {
        let mut v = Vec::with_capacity(vertex_count);
        for _ in 0..vertex_count {
            let mut position = [0f32; 3];
            reader.read_f32_into::<LE>(&mut position)?;
            let mut unknown = [0u8; 8];
            reader.read_exact(&mut unknown)?;
            let mut uv = [half::consts::ZERO; 2];
            crate::util::read_f16_into_le(&mut reader, &mut uv)?;
            v.push(Vertex {
                position,
                unknown,
                uv,
            });
        }
        v
    };

    let d1s = {
        let mut v = Vec::with_capacity(d[1] as usize);
        for _ in 0..d[1] as usize {
            let mut unk0 = [0u8; 8];
            reader.read_exact(&mut unk0)?;
            let f8 = reader.read_f32::<LE>()?;
            v.push(D1 { unk0, f8 });
        }
        v
    };

    let d3s = {
        let mut v = Vec::with_capacity(d[3] as usize);
        for _ in 0..d[3] as usize {
            let mut x = [0u8; 4];
            reader.read_exact(&mut x)?;
            let mut f = [0.0f32; 12];
            reader.read_f32_into::<LE>(&mut f)?;
            let mut unk = [0u8; 26];
            reader.read_exact(&mut unk)?;
            v.push(D3 { x, f, unk });
        }
        v
    };

    let string_table_code_unit_count = reader.read_u32::<LE>()? as usize;
    if string_table_code_unit_count * 2 > data.len() {
        bail!(
            "String table size {} larger than input size {}",
            string_table_code_unit_count,
            data.len()
        );
    }

    let string_table_data = {
        let mut v = vec![];
        v.resize(string_table_code_unit_count * 2, 0);
        reader.read_exact(&mut v)?;
        v
    };

    let string_table: Vec<String> = {
        let mut reader = &string_table_data[..];
        let mut v = vec![];
        v.resize(string_table_code_unit_count, 0);
        reader.read_u16_into::<LE>(&mut v)?;
        v.split(|x| *x == 0)
            .map(|s| String::from_utf16_lossy(&s))
            .collect()
    };

    if reader.read_u8().is_ok() {
        bail!("Extra data after string table");
    }

    drop(shapes);
    drop(indices);
    drop(d0s);
    drop(vertices);
    drop(d1s);
    drop(d3s);
    drop(string_table);

    // eprintln!("{:?}", reader.seek(SeekFrom::Current(0)));
    survey.sample(file_id, reader.clone(), &reference);
    Ok(())
}

impl Fmt {
    pub fn write_obj(&self, w: &mut dyn std::io::Write) -> failure::Fallible<()> {
        use std::borrow::Cow;
        use std::collections::VecDeque;
        use std::iter::FromIterator;
        for v in &self.vertices {
            writeln!(w, "v {} {} {}", v.position[0], v.position[1], v.position[2])?;
            writeln!(w, "vt {} {}", v.uv[0], v.uv[1])?;
        }
        let mut shapes = self.shapes.iter().collect::<VecDeque<_>>();
        let indices: Cow<[u32]> = match &self.indices {
            Indices::Long(indices) => Cow::from(indices),
            Indices::Short(indices) => Cow::from_iter(indices.iter().map(|i| u32::from(*i))),
        };
        for (i, idx) in indices.chunks(3).enumerate() {
            if !shapes.is_empty() && shapes.front().unwrap().triangle_offset == i as u32 {
                let _shape = shapes.pop_front().unwrap();
                writeln!(w, "g {}", format!("shape{}", i))?;
            }
            writeln!(
                w,
                "f {0}/{0} {1}/{1} {2}/{2}",
                idx[0] + 1,
                idx[1] + 1,
                idx[2] + 1
            )?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_fmt_files() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)
            .or_else(|_| ggpk::Ggpk::open(r#"Content.ggpk"#))?;
        crate::util::visit_ggpk_entries_sync(
            &pack,
            |_, entry| {
                if let ggpk::ResolvedEntry::File(file) = entry {
                    use ggpk::NodeInfo;
                    if file.name().to_lowercase().ends_with(".fmt") {
                        let contents = file.contents();
                        let data = crate::util::decompress(contents);
                        let data: &[u8] = if let Ok(data) = &data { data } else { contents };
                        let res = super::parse_fmt(&data);
                        match res {
                            Ok(_fmt) => {}
                            Err(e) => {
                                bail!("{}: {}", crate::util::find_full_path(&pack, file.offset), e)
                            }
                        }
                    }
                }
                Ok(())
            },
            "/",
        )?;
        Ok(())
    }

    #[test]
    fn fmt_survey() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use rayon::iter::ParallelBridge;
        use rayon::iter::ParallelIterator;
        use std::collections::{BTreeSet, HashMap};
        use std::sync::{Arc, Mutex};

        type Region = (Vec<u8>, super::survey::Instance);

        let pack = Arc::new(
            ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)
                .or_else(|_| ggpk::Ggpk::open(r#"Content.ggpk"#))?,
        );
        let survey = Arc::new(Mutex::new(super::survey::Survey::default()));
        let regions: Arc<Mutex<HashMap<u64, Region>>> =
            Arc::new(Mutex::new(HashMap::new()));
        let (ds_tx, ds_rx) = crossbeam::channel::unbounded::<(u8, u8, u8, u8)>();
        let (entry_tx, entry_rx) = crossbeam::channel::bounded(4096);
        crossbeam::thread::scope(|s| {
            {
                let pack = pack.clone();
                s.spawn(move |_| {
                    crate::util::visit_ggpk_files(&pack, entry_tx, "/").unwrap();
                });
            }
            entry_rx.iter().par_bridge().for_each(|(_dir, file)| {
                use ggpk::NodeInfo;
                if file.name().to_lowercase().ends_with(".fmt") {
                    let contents = file.contents();
                    let data = crate::util::decompress(contents);
                    let data: &[u8] = if let Ok(data) = &data { data } else { contents };
                    if let Some(&[4, _, _, _, _, _, _, _, _, _, _, x, y, z, w]) = data.get(0..15) {
                        let d = (x, y, z, w);
                        // ds_tx.send(d).unwrap();
                        if true {
                            // x > 1 && y > 1 && z == 0 && w == 0 {
                            let mut si = super::survey::Instance::new(survey.clone());
                            let res = super::probe_fmt_survey(file.offset, data, &mut si);
                            if let Err(e) = res {
                                eprintln!(
                                    "Error in {:?} {} - {}",
                                    d,
                                    crate::util::find_full_path(&pack, file.offset),
                                    e
                                );
                            }
                        }
                    }
                }
            });
            drop(ds_tx);
        })
        .unwrap();
        let ds = ds_rx.iter().collect::<BTreeSet<_>>();
        for ds in ds {
            eprintln!("{:?}", ds);
        }
        let survey = survey.lock().unwrap();
        let results = survey.results.lock().unwrap();
        // for (k, v) in results.iter() {
        //     use super::SurveyValue::*;
        //     eprintln!("{} - {:?}", crate::util::find_full_path(&pack, *k), v);
        // }
        use std::collections::BTreeMap;
        // for (k, v) in results.iter() {
        //     use super::SurveyValue::*;
        //     eprintln!("{} - {:?}", crate::util::find_full_path(&pack, *k), v);
        // }
        use std::rc::Rc;
        let mut stats = <BTreeMap<&str, usize>>::new();
        let lenses: Vec<Rc<dyn super::survey::Lens>> = vec![
            Rc::new(super::survey::PrimitiveCountLens::new()),
            Rc::new(super::survey::QuaternionLens::new()),
            Rc::new(super::survey::ReasonableFloatLens::new()),
        ];
        for (_id, (reference, v)) in results.iter() {
            for sample in v {
                for lens in &lenses {
                    for label in lens.evaluate(sample, &reference) {
                        *stats.entry(label).or_default() += 1;
                    }
                }
            }
        }
        // for (stat, amount) in &stats {
        //     eprintln!("{}: {} ({:0.3}%)", stat, amount, 100.0 * *amount as f32 / results.len() as f32);
        // }

        let regions = regions.lock().unwrap();
        let mut counts_per_byte: BTreeMap<u64, (u32, [u32; 256])> = BTreeMap::new();
        for (_id, val) in regions.iter() {
            let data = &val.0;
            let si = &val.1;
            let mystery = si.regions.iter().find(|x| x.1 == "mystery").unwrap();
            for (i, b) in data[(mystery.2 as usize)..(mystery.3 as usize)]
                .iter()
                .enumerate()
            {
                let e = counts_per_byte
                    .entry(i as u64)
                    .or_insert_with(|| (0, [0u32; 256]));
                e.0 += 1;
                e.1[*b as usize] += 1;
            }
        }
        for (off, counts) in counts_per_byte.iter().take(10) {
            eprintln!("0x{:08x} ({}x): {:?}", off, counts.0, &counts.1[..]);
        }
        // let mut counts = <BTreeMap<super::SurveyCategory, usize>>::new();
        // for (_, v) in results.iter() {
        //     for value in v {
        //         if let Some(x) = match value {
        //             super::SurveyValue::U8(_) => Some(super::SurveyCategory::U8),
        //             super::SurveyValue::U16(_) => Some(super::SurveyCategory::U16),
        //             super::SurveyValue::U32(x) if *x < 0x10_0000 => Some(super::SurveyCategory::U32),
        //             super::SurveyValue::U64(x) if *x < 0x1_0000_0000_0000 => Some(super::SurveyCategory::U64),
        //             super::SurveyValue::F16(_) => Some(super::SurveyCategory::F16),
        //             super::SurveyValue::F32(_) => Some(super::SurveyCategory::F32),
        //             super::SurveyValue::F64(_) => Some(super::SurveyCategory::F64),
        //             _ => None,
        //         } {
        //             *counts.entry(x).or_default() += 1;
        //         }
        //     }
        // }
        // for (k, v) in &counts {
        //     eprintln!("{:?}: {} ({:0.3}%)", k, v, 100.0 * *v as f32 / results.len() as f32);
        // }
        Ok(())
    }
}
