mod types;
pub use types::*;

use nom::branch::alt;
use nom::bytes::complete::{is_not, tag, take_till};
use nom::character::complete::{char, digit1, line_ending, not_line_ending, space1};
use nom::combinator::{map, map_res};
use nom::multi::count;
use nom::sequence::{delimited, preceded, separated_pair};
use nom::IResult;

#[allow(dead_code)]
fn parse_usize<'a>(input: &'a str) -> IResult<&'a str, usize> {
    map_res(digit1, |digit_str: &str| digit_str.parse::<usize>())(input)
}

#[allow(dead_code)]
fn parse_isize<'a>(input: &'a str) -> IResult<&'a str, isize> {
    alt((
        map_res(digit1, |digit_str: &str| digit_str.parse::<isize>()),
        map(preceded(tag("-"), digit1), |digit_str: &str| {
            -digit_str.parse::<isize>().unwrap()
        }),
    ))(input)
}

#[allow(dead_code)]
fn parse_qstring<'a>(input: &'a str) -> IResult<&'a str, &'a str> {
    delimited(char('"'), take_till(|x| x == '"'), char('\"'))(input)
}

#[allow(dead_code)]
fn parse_qstring1<'a>(input: &'a str) -> IResult<&'a str, &'a str> {
    delimited(char('"'), is_not("\""), char('"'))(input)
}

fn parser<'a>(input: &'a str) -> IResult<&'a str, Tgr> {
    let (input, version) = preceded(tag("version "), parse_usize)(input)?;
    let (input, _) = line_ending(input)?;

    let (input, size) = preceded(
        tag("Size: "),
        separated_pair(parse_usize, space1, parse_usize),
    )(input)?;
    let (input, _) = line_ending(input)?;

    let (input, resolution) = preceded(
        tag("Resolution: "),
        separated_pair(parse_usize, space1, parse_usize),
    )(input)?;
    let (input, _) = line_ending(input)?;

    let (input, master_file) = preceded(tag("MasterFile: "), parse_qstring1)(input)?;
    let (input, _) = line_ending(input)?;

    let (input, node_count) = preceded(tag("Nodes: "), parse_usize)(input)?;
    let (input, _) = line_ending(input)?;

    let (input, edge_count) = preceded(tag("Edges: "), parse_usize)(input)?;
    let (input, _) = line_ending(input)?;

    let (input, gts) = count(
        |input: &str| -> IResult<&str, String> {
            let (input, gt) = parse_qstring(input)?;
            let (input, _) = line_ending(input)?;
            Ok((input, gt.to_owned()))
        },
        3,
    )(input)?;
    let gts: &[String; 3] = gts.as_slice().try_into().unwrap();

    // Node list
    let (input, nodes) = count(
        |input: &str| -> IResult<&str, Node> {
            let (input, (x, y)) = separated_pair(parse_usize, space1, parse_usize)(input)?;
            let (input, num_count) = preceded(space1, parse_usize)(input)?;
            let (input, nums) = count(preceded(space1, parse_usize), num_count)(input)?;
            let (input, name_tag) = preceded(space1, parse_qstring)(input)?;
            let (input, orientation) = preceded(space1, is_not(" "))(input)?;
            let (input, note_count) = preceded(space1, parse_usize)(input)?;
            let (input, notes) = count(preceded(space1, parse_qstring), note_count)(input)?;
            let notes = notes.into_iter().map(|n| n.to_owned()).collect();

            let (input, tails) = count(preceded(space1, parse_usize), 4)(input)?;
            let tails: &[usize; 4] = tails.as_slice().try_into().unwrap();

            let (input, _) = line_ending(input)?;

            Ok((
                input,
                Node {
                    x,
                    y,
                    nums,
                    tag: name_tag.to_owned(),
                    orientation: orientation.to_owned(),
                    notes,
                    tails: *tails,
                },
            ))
        },
        node_count,
    )(input)?;

    // Edge list
    let (input, edges) = count(
        |input: &str| -> IResult<&str, Edge> {
            let (input, (from, to)) = separated_pair(parse_usize, space1, parse_usize)(input)?;
            let (input, coord_count) = preceded(space1, parse_usize)(input)?;
            let (input, coords) = count(
                preceded(space1, separated_pair(parse_usize, space1, parse_usize)),
                coord_count,
            )(input)?;
            let coords = coords.into_iter().map(|n| n.to_owned()).collect();

            let (input, big) = preceded(space1, parse_usize)(input)?;
            let (input, b) = if version > 14 {
                preceded(space1, parse_usize)(input).map(|(input, x)| (input, Some(x)))?
            } else {
                (input, None)
            };
            let (input, et) = preceded(space1, parse_qstring)(input)?;
            let (input, tails) = count(preceded(space1, parse_usize), 2)(input)?;
            let tails: &[usize; 2] = tails.as_slice().try_into().unwrap();
            let (input, tail_word) = if version <= 14 {
                preceded(space1, not_line_ending)(input).map(|(input, x)| (input, Some(x.to_owned())))?
            } else {
                (input, None)
            };

            let (input, _) = line_ending(input)?;

            Ok((
                input,
                Edge {
                    from,
                    to,
                    coords,
                    big,
                    b,
                    et: et.to_owned(),
                    tails: *tails,
                    tail_word,
                },
            ))
        },
        edge_count,
    )(input)?;

    use std::convert::TryInto;

    Ok((
        input,
        Tgr {
            version,
            size,
            resolution,
            master_file: master_file.to_owned(),
            gts: gts.clone(),
            nodes,
            edges,
        },
    ))
}

pub fn parse_tgr(contents: &str) -> failure::Fallible<Tgr> {
    match parser(contents) {
        Ok(("", tgr)) => Ok(tgr),
        Ok((input, _)) => {
            bail!(
                "Data remaining after parse: {:?}...",
                input.chars().take(100).collect::<String>()
            );
        }
        Err(e) => bail!("{:?}", e),
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_tgr_files() -> failure::Fallible<()> {
        time_test!();
        use crate::{formats::ggpk, util};
        let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
        let (tx, rx) = crossbeam::channel::bounded(128);
        let t = {
            let pack = pack.clone();
            std::thread::spawn(move || {
                util::visit_ggpk_files(&pack, tx, "/").unwrap();
            })
        };

        let mut success_count = 0;
        let mut failures = vec![];
        while let Ok((_, file)) = rx.recv() {
            use ggpk::NodeInfo;
            if file.name().to_lowercase().ends_with(".tgr") {
                let full_path = crate::util::find_full_path(&pack, file.offset());
                let text = crate::util::vec_to_utf16(file.contents())
                    .ok_or_else(|| format_err!("TGR file not UTF-16LE"));
                let res = super::parse_tgr(&text.unwrap());
                if res.is_ok() {
                    success_count += 1;
                }
                if res.is_err() {
                    eprintln!("{}: {:?}", full_path, res);
                    failures.push((full_path, res));
                }
            }
        }
        let total_count = success_count + failures.len();
        eprintln!(
            "{}/{} success, {}/{} fail",
            success_count,
            total_count,
            failures.len(),
            total_count
        );
        // for f in failures {
        //     eprintln!("{} - {:?}", f.0, f.1);
        // }

        t.join().unwrap();
        Ok(())
    }
}
