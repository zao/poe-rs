#[derive(Debug)]
pub struct Tgr {
    pub version: usize,
    pub size: (usize, usize),
    pub resolution: (usize, usize),
    pub master_file: String,
    pub gts: [String; 3],
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
}

#[derive(Debug, Clone)]
pub struct Node {
    pub x: usize,
    pub y: usize,
    pub nums: Vec<usize>,
    pub tag: String,
    pub orientation: String,
    pub notes: Vec<String>,
    pub tails: [usize; 4],
}

#[derive(Debug, Clone)]
pub struct Edge {
    pub from: usize,
    pub to: usize,
    pub coords: Vec<(usize, usize)>,
    pub big: usize,
    pub b: Option<usize>,
    pub et: String,
    pub tails: [usize; 2],
    pub tail_word: Option<String>,
}
