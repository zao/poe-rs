mod types;
pub use types::*;

use std::io::{BufRead, BufReader};

fn decode_contents(bytes: &[u8]) -> failure::Fallible<Vec<u8>> {
    use byteorder::{ByteOrder, LE};
    Ok(
        if bytes.len() >= 2 && bytes.len() % 2 == 0 && bytes[0..2] == [0xFF, 0xFE] {
            let tail = &bytes[2..];
            let mut v = <Vec<u16>>::new();
            v.resize(tail.len() / 2, 0u16);
            LE::read_u16_into(&bytes[2..], &mut v[..]);
            String::from_utf16(&v)?.as_bytes().into()
        } else if bytes.len() >= 3 && bytes[0..3] == [0xEF, 0xBB, 0xBF] {
            let tail = &bytes[2..];
            tail.into()
        } else {
            bytes.into()
        },
    )
}

pub fn parse_hideout(bytes: &[u8]) -> Result<Hideout, failure::Error> {
    let bytes = decode_contents(&bytes)?;
    let item_kv_regex = regex::Regex::new(r#"(\w+)=(\d+)"#)?;
    let reader = BufReader::new(&bytes[..]);
    let mut language = None;
    let mut name = None;
    let mut hash = None;
    let mut items = Vec::new();
    for line in reader.lines().map(|l| l.unwrap()) {
        let sides = line.splitn(2, " = ").collect::<Vec<_>>();
        if sides.len() != 2 {
            continue;
        }
        match sides[0] {
            "Language" => language = Some(sides[1].to_owned()),
            "Hideout Name" => name = Some(sides[1].to_owned()),
            "Hideout Hash" => hash = str::parse::<u32>(sides[1]).ok(),
            _ => {
                let mut hash = None;
                let mut x = None;
                let mut y = None;
                let mut rot = None;
                let mut flip = None;
                let mut var = None;
                for caps in item_kv_regex.captures_iter(&sides[1]) {
                    if caps.len() == 3 {
                        match &caps[1] {
                            "Hash" => hash = str::parse::<u32>(&caps[2]).ok(),
                            "X" => x = str::parse::<i32>(&caps[2]).ok(),
                            "Y" => y = str::parse::<i32>(&caps[2]).ok(),
                            "Rot" => rot = str::parse::<i16>(&caps[2]).ok(),
                            "Flip" => flip = str::parse::<u8>(&caps[2]).ok(),
                            "Var" => var = str::parse::<u8>(&caps[2]).ok(),
                            _ => {}
                        }
                        if let Some(item) = (|| {
                            Some(HideoutItem {
                                name: sides[0].to_owned(),
                                hash: hash?,
                                x: x?,
                                y: y?,
                                rot: rot?,
                                flip: flip?,
                                var: var?,
                            })
                        })() {
                            items.push(item);
                        }
                    }
                }
            }
        }
    }
    Ok(Hideout {
        language: language.ok_or_else(|| format_err!("Language key missing"))?,
        name: name.ok_or_else(|| format_err!("Hideout Name key missing"))?,
        hash: hash.ok_or_else(|| format_err!("Hideout Hash key missing"))?,
        items,
    })
}

#[cfg(test)]
mod tests {
    #[test]
    fn ref_hideouts() -> failure::Fallible<()> {
        for entry in std::fs::read_dir(
            std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("ref/hideout"),
        )? {
            let path = entry?.path();
            eprintln!("parsing {:?}", &path);
            let contents = std::fs::read(&path)?;
            let _ho = super::parse_hideout(&contents)?;
            // eprintln!("{:#?}", &_ho);
        }
        Ok(())
    }

    #[test]
    fn balor_hideouts() -> Result<(), failure::Error> {
        for entry in std::fs::read_dir(
            std::path::PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("ref/hideout/balormage"),
        )? {
            let path = entry?.path();
            eprintln!("parsing {:?}", &path);
            let contents = std::fs::read(&path)?;
            let _ho = super::parse_hideout(&contents)?;
            eprintln!("{:#?}", &_ho);
        }
        Ok(())
    }
}
