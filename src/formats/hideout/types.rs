#[derive(Debug)]
pub struct Hideout {
    pub language: String,
    pub name: String,
    pub hash: u32,
    pub items: Vec<HideoutItem>,
}

#[derive(Debug)]
pub struct HideoutItem {
    pub name: String,
    pub hash: u32,
    pub x: i32,
    pub y: i32,
    pub rot: i16,
    pub flip: u8,
    pub var: u8,
}