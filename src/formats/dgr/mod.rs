mod types;
pub use types::*;

pub fn parse_dgr(contents: &str) -> failure::Fallible<Dgr> {
    use crate::parse::ParseOps;

    let mut lines = contents.lines();
    || -> Option<Dgr> {
        // version 18
        let version = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.usize()?
        };

        // Size: 1 1
        let (size_x, size_y) = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            (po.usize()?, po.usize()?)
        };

        // Resolution: 1000 1000
        let (res_x, res_y) = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            (po.usize()?, po.usize()?)
        };

        // MasterFile: "Metadata/Terrain/Dungeon/master.tsi"
        let master_file = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.quoted_string()?.to_owned()
        };

        // Nodes: 1
        let node_count = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.usize()?
        };

        // Edges: 0
        let edge_count = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.usize()?
        };
        // "Metadata/Terrain/Act4/Area2/GroundTypes/mountain_ground_top.gt"
        let gt_top = {
            let mut po = ParseOps::new(lines.next()?);
            po.quoted_string()?.to_owned()
        };

        // "Metadata/Terrain/Act4/Area2/GroundTypes/mountain_ground.gt"
        let gt_mid = {
            let mut po = ParseOps::new(lines.next()?);
            po.quoted_string()?.to_owned()
        };

        // "Metadata/Terrain/Act4/Area2/GroundTypes/mountain_ground_bottom.gt"
        let gt_bot = {
            let mut po = ParseOps::new(lines.next()?);
            po.quoted_string()?.to_owned()
        };

        // Default%: 0 0 0
        let (def_x, def_y, def_z) = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            (po.usize()?, po.usize()?, po.usize()?)
        };

        // 497 535 0 "" I 5 "telepad1" "telepad2" "teleport1" "teleport2" "accessible" 100 0 N
        let mut nodes = Vec::new();
        for _ in 0..node_count {
            let mut po = ParseOps::new(lines.next()?);
            let x = po.usize()?;
            let y = po.usize()?;
            let val_count = po.usize()?;
            let mut vals = Vec::new();
            for _ in 0..val_count {
                vals.push(po.usize()?);
            }
            let key = po.quoted_string()?.to_owned();
            let rotate_flip = po.word()?.to_owned();
            let str_count = po.usize()?;
            let mut strs = Vec::new();
            for _ in 0..str_count {
                strs.push(po.quoted_string()?.to_owned());
            }
            let unk_num1 = po.usize()?;
            let unk_num2 = po.usize()?;
            let unk_last = po.word()?.to_owned();

            nodes.push(Node {
                x,
                y,
                vals,
                key,
                rotate_flip,
                strs,
                unk_num1,
                unk_num2,
                unk_last,
            })
        }

        let mut edges = Vec::new();
        for _ in 0..edge_count {
            let mut po = ParseOps::new(lines.next()?);
            let from_node = po.usize()?;
            let to_node = po.usize()?;
            let coord_count = po.usize()?;
            let mut coords = Vec::new();
            for _ in 0..coord_count {
                coords.push((po.usize()?, po.usize()?));
            }
            let unk2_num = po.usize()?;
            let unk3_num = po.usize()?;
            let et = po.quoted_string()?.to_owned();
            let unk4_num = po.usize()?;
            let unk5_num = po.usize()?;
            let unk5a_str = if version >= 18 {
                Some(po.quoted_string()?.to_owned())
            } else {
                None
            };
            let unk6_num = po.usize()?;
            let unk7_num = po.usize()?;
            let unk8_num = po.usize()?;
            let unk9_letter = po.word()?.to_owned();
            let unk10_letter = po.word()?.to_owned();
            let unk11_num = po.usize()?;
            edges.push(Edge {
                from_node,
                to_node,
                coords,
                unk2_num,
                unk3_num,
                et,
                unk4_num,
                unk5_num,
                unk5a_str,
                unk6_num,
                unk7_num,
                unk8_num,
                unk9_letter,
                unk10_letter,
                unk11_num,
            });
        }

        Some(Dgr {
            version,
            size: (size_x, size_y),
            resolution: (res_x, res_y),
            master_file,
            gt_top,
            gt_mid,
            gt_bot,
            default_percent: (def_x, def_y, def_z),
            nodes,
            edges,
        })
    }()
    .ok_or_else(|| format_err!("Failed to parse DGR file"))
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_dgr_files() -> failure::Fallible<()> {
        time_test!();
        use crate::{formats::ggpk, util};
        let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
        let (tx, rx) = crossbeam::channel::bounded(128);
        let t = {
            let pack = pack.clone();
            std::thread::spawn(move || {
                util::visit_ggpk_files(&pack, tx, "/").unwrap();
            })
        };

        while let Ok((_, file)) = rx.recv() {
            use ggpk::NodeInfo;
            if file.name().to_lowercase().ends_with(".dgr") {
                let full_path = crate::util::find_full_path(&pack, file.offset());
                let text = crate::util::vec_to_utf16(file.contents())
                    .ok_or_else(|| format_err!("DGR file not UTF-16LE"));
                let res = super::parse_dgr(&text.unwrap());
                if res.is_err() {
                    eprintln!("{}: {:?}", full_path, res);
                    panic!();
                }
            }
        }

        t.join().unwrap();
        Ok(())
    }
}
