#[derive(Debug)]
pub struct Dgr {
    pub version: usize,
    pub size: (usize, usize),
    pub resolution: (usize, usize),
    pub master_file: String,
    pub gt_top: String,
    pub gt_mid: String,
    pub gt_bot: String,
    pub default_percent: (usize, usize, usize),
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
}

#[derive(Debug)]
pub struct Node {
    pub x: usize,
    pub y: usize,
    pub vals: Vec<usize>,
    pub key: String,
    pub rotate_flip: String,
    pub strs: Vec<String>,
    pub unk_num1: usize,
    pub unk_num2: usize,
    pub unk_last: String,
}

#[derive(Debug)]
pub struct Edge {
    pub from_node: usize,
    pub to_node: usize,
    pub coords: Vec<(usize, usize)>,
    pub unk2_num: usize,
    pub unk3_num: usize,
    pub et: String,
    pub unk4_num: usize,
    pub unk5_num: usize,
    pub unk5a_str: Option<String>,
    pub unk6_num: usize,
    pub unk7_num: usize,
    pub unk8_num: usize,
    pub unk9_letter: String,
    pub unk10_letter: String,
    pub unk11_num: usize,
}
