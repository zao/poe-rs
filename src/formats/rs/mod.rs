mod types;
pub use types::*;

pub fn parse_rs(contents: &str) -> failure::Fallible<Rs> {
    use crate::parse::ParseOps;

    let mut lines = contents.lines().filter(|line| !line.trim().is_empty());
    || -> Option<Rs> {
        // version 2
        let version = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.usize()?
        };

        let mut arms = Vec::new();
        for line in lines {
            let line = line.trim_start_matches('\\');
            let mut po = ParseOps::new(line);
            let weight = po.usize(); // might fail due to missing weight, is ok
            let name = po.quoted_string()?.to_owned();
            let xform1 = po.word().map(ToOwned::to_owned);
            let xform2 = po.word().map(ToOwned::to_owned);

            arms.push(RsArm{ weight, name, xform1, xform2 });
        }

        Some(Rs { version, arms })
    }()
    .ok_or_else(|| format_err!("Failed to parse RS file"))
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_rs_files() -> failure::Fallible<()> {
        time_test!();
        use crate::{formats::ggpk, util};
        let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
        let (tx, rx) = crossbeam::channel::bounded(128);
        let t = {
            let pack = pack.clone();
            std::thread::spawn(move || {
                util::visit_ggpk_files(&pack, tx, "/").unwrap();
            })
        };

        while let Ok((_, file)) = rx.recv() {
            use ggpk::NodeInfo;
            if file.name().to_lowercase().ends_with(".rs") {
                let full_path = crate::util::find_full_path(&pack, file.offset());
                let text = crate::util::vec_to_utf16(file.contents())
                    .ok_or_else(|| format_err!("RS file not UTF-16LE"));
                let res = super::parse_rs(&text.unwrap());
                if res.is_err() {
                    eprintln!("{}: {:?}", full_path, res);
                    panic!();
                }
            }
        }

        t.join().unwrap();
        Ok(())
    }
}
