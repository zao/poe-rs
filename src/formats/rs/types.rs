#[derive(Debug)]
pub struct Rs {
    pub version: usize,
    pub arms: Vec<RsArm>,
}

#[derive(Debug)]
pub struct RsArm {
    pub weight: Option<usize>,
    pub name: String,
    pub xform1: Option<String>,
    pub xform2: Option<String>,
}