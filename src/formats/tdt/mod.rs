mod types;
pub use types::*;

use byteorder::{LE, ReadBytesExt};
use std::io::{Cursor, Seek, SeekFrom};

pub fn parse_tdt(data: &[u8]) -> failure::Fallible<Tdt> {
    let mut r = &data[..];
    let _version = r.read_u32::<LE>()?;
    let codepoint_count = r.read_u32::<LE>()? as usize;
    let _codepoints = {
        let mut v = vec![0u16; codepoint_count];
        r.read_u16_into::<LE>(&mut v)?;
        v
    };
    Ok(Tdt {})
}

pub trait Probe {
    fn probe<S: AsRef<str>, R: Seek>(&mut self, tag: S, reader: &mut R);
}

impl Probe for () {
    fn probe<S: AsRef<str>, R: Seek>(&mut self, _tag: S, _reader: &mut R) {}
}

impl Probe for Vec<(String, u64)> {
    fn probe<S: AsRef<str>, R: Seek>(&mut self, tag: S, reader: &mut R) {
        let tag = tag.as_ref().to_owned();
        let pos = reader.seek(SeekFrom::Current(0)).unwrap();
        self.push((tag, pos));
    }
}

pub fn probe_tdt<P: Probe>(data: &[u8], probe: &mut P) -> failure::Fallible<Tdt> {
    let mut r = Cursor::new(&data[..]);
    probe.probe("version", &mut r);
    let _version = r.read_u32::<LE>()?;
    probe.probe("codepoint_count", &mut r);
    let codepoint_count = r.read_u32::<LE>()? as usize;
    probe.probe("codepoints", &mut r);
    let _codepoints = {
        let mut v = vec![0u16; codepoint_count];
        r.read_u16_into::<LE>(&mut v)?;
        v
    };
    probe.probe("tail", &mut r);
    Ok(Tdt {})
}

#[cfg(test)]
mod tests {
    use serde::Serialize;
    use std::collections::BTreeMap;

    #[derive(Debug, Serialize)]
    struct ProbeOutput {
        files: BTreeMap<String, ProbeOutputFile>,
    }

    #[derive(Debug, Serialize)]
    struct ProbeOutputFile {
        tags: BTreeMap<String, u64>,
    }

    #[test]
    fn parse_all_tdt() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;
        use std::collections::BTreeMap;
        use std::sync::Mutex;
        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        let probes: Mutex<BTreeMap<String, Vec<(String, u64)>>> = Mutex::new(BTreeMap::new());
        let results: Vec<(ggpk::File, Result<super::Tdt, _>)> = crossbeam::thread::scope(|s| {
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                let pack = pack.clone();
                s.spawn(move |_| {
                    crate::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
                });
            }
            file_rx.iter().par_bridge().filter_map(|(_dir, file)| {
                if file.name().ends_with(".tdt") {
                    // let res = super::parse_tdt(file.contents());
                    let mut probe = vec![];
                    let res = super::probe_tdt(file.contents(), &mut probe);
                    let path = crate::util::find_full_path(&pack, file.offset);
                    probes.lock().unwrap().insert(path, probe);
                    Some((file, res))
                }
                else {
                    None
                }
            }).collect()
        }).unwrap();
        let probe_output = ProbeOutput {
            files: probes.lock().unwrap().iter().map(|(path, probe)| {
                let tags = probe.iter().cloned().collect::<BTreeMap<String, u64>>();
                (path.clone(), ProbeOutputFile { tags })
            }).collect(),
        };
        serde_json::to_writer_pretty(std::fs::File::create("tdt-probes.json")?, &probe_output)?;
        for res in results {
            match res {
                (_file, Ok(_tdt)) => {}
                (file, Err(e)) => {
                    eprintln!("{} - {}", crate::util::find_full_path(&pack, file.offset), e);
                }
            }
        }
        Ok(())
    }
}