mod types;
pub use types::*;

use crate::murmur;
use byteorder::{ReadBytesExt, LE};
use crossbeam::sync::ShardedLock;
use memmap::Mmap;
use sha2::{Digest as Sha256Digest, Sha256};
use std::collections::HashMap;
use std::io::Read;
use std::path::Path;
use std::sync::Arc;

impl std::fmt::Display for Digest {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        for byte in &self.0 {
            f.write_fmt(format_args!("{:02X}", byte))?;
        }
        Ok(())
    }
}

impl NodeInfo for Directory {
    fn compute_digest(&self) -> Digest {
        use std::convert::TryInto;
        let mut ret = Digest::default();
        let mut hasher = Sha256::new();
        for entry in self.entries() {
            let e = resolve_entry(entry.mem, entry.offset).unwrap();
            hasher.input(e.digest().0);
        }
        ret.0 = hasher.result().as_slice().try_into().unwrap();
        ret
    }

    fn digest(&self) -> Digest {
        let digest_base = (self.offset + 16) as usize;
        let mut reader = &self.mem[digest_base..];
        reader.read_digest().unwrap()
    }

    fn name(&self) -> String {
        let mut reader = &self.mem[self.name_base as usize..];
        String::from_utf16(
            &reader
                .read_sized_terminated_string_u16(self.name_len)
                .unwrap(),
        )
        .unwrap()
    }

    fn name16(&self) -> widestring::U16CString {
        let mut reader = &self.mem[self.name_base as usize..];
        widestring::U16CString::from_ustr(widestring::U16Str::from_slice(
            &reader
                .read_sized_terminated_string_u16(self.name_len)
                .unwrap(),
        ))
        .unwrap()
    }

    fn offset(&self) -> u64 {
        self.offset
    }
}

impl NodeInfo for File {
    fn compute_digest(&self) -> Digest {
        use std::convert::TryInto;

        let mut ret = Digest::default();
        ret.0 = Sha256::digest(self.contents())
            .as_slice()
            .try_into()
            .unwrap();
        ret
    }

    fn digest(&self) -> Digest {
        let digest_base = (self.offset + 12) as usize;
        let mut reader = &self.mem[digest_base..];
        reader.read_digest().unwrap()
    }

    fn name(&self) -> String {
        let mut reader = &self.mem[self.name_base as usize..];
        String::from_utf16(
            &reader
                .read_sized_terminated_string_u16(self.name_len)
                .unwrap(),
        )
        .unwrap()
    }

    fn name16(&self) -> widestring::U16CString {
        let mut reader = &self.mem[self.name_base as usize..];
        widestring::U16CString::from_ustr(widestring::U16Str::from_slice(
            &reader
                .read_sized_terminated_string_u16(self.name_len)
                .unwrap(),
        ))
        .unwrap()
    }

    fn offset(&self) -> u64 {
        self.offset
    }
}

impl NodeInfo for ResolvedEntry {
    fn compute_digest(&self) -> Digest {
        match self {
            ResolvedEntry::Directory(dir) => dir.compute_digest(),
            ResolvedEntry::File(file) => file.compute_digest(),
        }
    }

    fn digest(&self) -> Digest {
        match self {
            ResolvedEntry::Directory(dir) => dir.digest(),
            ResolvedEntry::File(file) => file.digest(),
        }
    }

    fn name(&self) -> String {
        match self {
            ResolvedEntry::Directory(dir) => dir.name(),
            ResolvedEntry::File(file) => file.name(),
        }
    }

    fn name16(&self) -> widestring::U16CString {
        match self {
            ResolvedEntry::Directory(dir) => dir.name16(),
            ResolvedEntry::File(file) => file.name16(),
        }
    }

    fn offset(&self) -> u64 {
        match self {
            ResolvedEntry::Directory(dir) => dir.offset(),
            ResolvedEntry::File(file) => file.offset(),
        }
    }
}

pub fn resolve_entry(
    mem: Arc<Mmap>,
    offset: StorageOffset,
) -> Result<ResolvedEntry, failure::Error> {
    let mut reader = buffered_reader::Generic::new(
        mem.get(offset as usize..)
            .ok_or_else(|| format_err!("Entry offset {} out of bounds", offset))?,
        None,
    );
    let (rec_len, tag) = reader.read_record_header()?;
    match tag {
        FILE_TAG => {
            let name_len = reader.read_u32::<LE>()? as usize;
            let _digest = reader.read_digest()?;
            let name_base = offset + 12 + 32;
            let data_base = name_base + name_len as u64 * 2;
            let data_size = rec_len as usize - (data_base - offset) as usize;
            let end = offset as usize + rec_len as usize;
            mem.get(0..end)
                .ok_or_else(|| format_err!("File at {} truncated", offset))?;
            Ok(ResolvedEntry::File(File {
                mem,
                offset,
                name_len,
                name_base,
                data_size,
                data_base,
            }))
        }
        PDIR_TAG => {
            let name_len = reader.read_u32::<LE>()? as usize;
            let child_count = reader.read_u32::<LE>()? as usize;
            let _digest = reader.read_digest()?;
            let name_base = offset + 16 + 32;
            let child_base = name_base + name_len as u64 * 2;
            let end = child_base as usize + child_count as usize * 6;
            mem.get(0..end)
                .ok_or_else(|| format_err!("Directory at {} truncated", offset))?;
            Ok(ResolvedEntry::Directory(Directory {
                mem,
                offset,
                name_len,
                name_base,
                child_count,
                child_base,
            }))
        }
        _ => bail!(
            "Entry offset {} from directory is not a directory or file",
            offset
        ),
    }
}

impl Entry {
    pub fn resolve(&self) -> Result<ResolvedEntry, failure::Error> {
        resolve_entry(self.mem.clone(), self.offset)
    }
}

impl Iterator for Entries {
    type Item = Entry;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.count {
            return None;
        }

        let entry_offset = self.base as usize + self.index * 12;
        self.index += 1;

        let mut reader = self.mem.get(entry_offset..)?;
        let hash = reader.read_u32::<LE>().ok()?;
        let offset = reader.read_u64::<LE>().ok()?;
        Some(Entry {
            mem: self.mem.clone(),
            hash,
            offset,
        })
    }
}

impl Directory {
    pub fn entries(&self) -> Entries {
        Entries {
            mem: self.mem.clone(),
            base: self.child_base,
            index: 0,
            count: self.child_count,
        }
    }
}

impl File {
    pub fn size(&self) -> usize {
        self.data_size
    }
    pub fn contents(&self) -> &[u8] {
        let beg = self.data_base as usize;
        let end = beg + self.data_size as usize;
        self.mem
            .get(beg..end)
            .ok_or_else(|| format_err!("File contents outside of pack bounds"))
            .unwrap()
    }
}

const FILE_TAG: Tag = [b'F', b'I', b'L', b'E'];
const FREE_TAG: Tag = [b'F', b'R', b'E', b'E'];
const GGPK_TAG: Tag = [b'G', b'G', b'P', b'K'];
const PDIR_TAG: Tag = [b'P', b'D', b'I', b'R'];

impl<R: std::io::Read + ?Sized> ReadGgpkExt for R {
    fn read_record_header(&mut self) -> std::io::Result<types::RecordHeader> {
        Ok((self.read_u32::<LE>()?, self.read_tag()?))
    }

    fn read_tag(&mut self) -> std::io::Result<Tag> {
        let mut tag = Tag::default();
        self.read_exact(&mut tag)?;
        Ok(tag)
    }

    fn read_digest(&mut self) -> std::io::Result<Digest> {
        let mut digest = Digest::default();
        self.read_exact(&mut digest.0)?;
        Ok(digest)
    }

    fn read_sized_terminated_string_u16(&mut self, code_units: usize) -> std::io::Result<Vec<u16>> {
        if code_units < 1 {
            return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
        }
        let n = code_units - 1;
        let mut ret = vec![0; n];
        self.read_u16_into::<LE>(&mut ret[..])?;
        self.read_u16::<LE>()?; // Discard terminator
        Ok(ret)
    }
}

fn utf16_le_bytes<S: AsRef<str>>(s: S) -> Vec<u8> {
    let mut ret = Vec::new();
    for part in s.as_ref().encode_utf16().map(u16::to_le_bytes) {
        ret.extend(&part);
    }
    ret
}

impl Ggpk {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Arc<Ggpk>, failure::Error> {
        let fh = std::fs::File::open(path.as_ref())?;
        let signature = {
            let meta = fh.metadata()?;
            format!(
                "{}_{:?}",
                meta.len(),
                meta.modified()?
                    .duration_since(std::time::SystemTime::UNIX_EPOCH)?
                    .as_nanos()
            )
        };
        eprintln!("{}", signature);
        let mem = Arc::new(unsafe { Mmap::map(&fh)? });
        let mut header_reader = mem
            .get(0..)
            .ok_or_else(|| format_err!("GGPK file seems empty"))?;
        let rec_len = header_reader.read_u32::<LE>()?;
        let mut header_reader = header_reader.take(u64::from(rec_len).saturating_sub(4));
        let tag = header_reader.read_tag()?;
        if tag != GGPK_TAG {
            bail!("File at {:?} is not a GGPK file", path.as_ref());
        }
        let child_count = header_reader.read_u32::<LE>()?;
        let mut root: Option<u64> = None;
        let mut free: Option<u64> = None;
        for _ in 0..child_count {
            let offset = header_reader.read_u64::<LE>()? as usize;
            let mut child_reader = mem
                .get(offset..)
                .ok_or_else(|| format_err!("Child record at {} is missing", offset))?;
            let rec_len = child_reader.read_u32::<LE>()? as usize;
            let mut child_reader = child_reader.take((rec_len as u64).saturating_sub(4));
            let tag = child_reader.read_tag()?;
            match tag {
                PDIR_TAG => root = Some(offset as u64),
                FREE_TAG => free = Some(offset as u64),
                _ => {} // Ignore unknown block types
            }
        }
        let root = root.ok_or_else(|| format_err!("GGPK file does not have a root directory"))?;
        let parents = Arc::new(ShardedLock::new(HashMap::new()));
        let ret = Arc::new(Ggpk {
            mem,
            root,
            free,
            parents,
        });
        {
            let pack = ret.clone();
            let (ch_s, ch_r) = crossbeam::channel::bounded::<()>(1);
            std::thread::spawn(move || {
                let mut parents = pack.parents.write().unwrap();
                ch_s.send(()).unwrap();
                let f = |parent: Directory, child: ResolvedEntry| -> failure::Fallible<()> {
                    parents.insert(child.offset(), parent.offset);
                    Ok(())
                };
                crate::util::visit_ggpk_entries_sync(&pack, f, "/").unwrap();
            });
            ch_r.recv()?;
        }
        Ok(ret)
    }

    pub fn root(&self) -> StorageOffset {
        self.root
    }

    pub fn root_digest(&self) -> Option<Digest> {
        let root_dir = self.resolve(self.root()).ok();
        match root_dir {
            Some(ResolvedEntry::Directory(d)) => Some(d.digest()),
            _ => None,
        }
    }

    pub fn directory(&self, offset: StorageOffset) -> Result<Directory, failure::Error> {
        Ok(match resolve_entry(self.mem.clone(), offset)? {
            ResolvedEntry::Directory(dir) => dir,
            _ => bail!("Entry at {} is not a directory", offset),
        })
    }

    pub fn file(&self, offset: StorageOffset) -> Result<File, failure::Error> {
        Ok(match resolve_entry(self.mem.clone(), offset)? {
            ResolvedEntry::File(file) => file,
            _ => bail!("Entry at {} is not a file", offset),
        })
    }

    pub fn lookup<P: AsRef<str>>(&self, path: P) -> Result<ResolvedEntry, failure::Error> {
        let components = path
            .as_ref()
            .split('/')
            .filter(|c| !c.is_empty())
            .collect::<Vec<_>>();

        let mem = self.mem.clone();
        let mut current_entry = resolve_entry(mem, self.root)?;
        for comp in components {
            let comp_bytes = utf16_le_bytes(comp.to_lowercase());
            let comp_hash = murmur::murmur2_32(&comp_bytes);
            match &current_entry {
                ResolvedEntry::Directory(dir) => {
                    match dir.entries().find(|e| e.hash == comp_hash) {
                        Some(next) => current_entry = next.resolve()?,
                        None => bail!("Component not found in directory."),
                    }
                }
                ResolvedEntry::File(_) => bail!("Path traversal treated a file as a directory."),
            }
        }
        Ok(current_entry)
    }

    pub fn lookup_file<P: AsRef<str>>(&self, path: P) -> Result<File, failure::Error> {
        match self.lookup(&path)? {
            ResolvedEntry::File(file) => Ok(file),
            _ => bail!("Path {} is a directory, not a file", path.as_ref()),
        }
    }

    pub fn lookup_directory<P: AsRef<str>>(&self, path: P) -> Result<Directory, failure::Error> {
        match self.lookup(&path)? {
            ResolvedEntry::Directory(dir) => Ok(dir),
            _ => bail!("Path {} is a file, not a directory", path.as_ref()),
        }
    }

    pub fn resolve(&self, offset: u64) -> failure::Fallible<ResolvedEntry> {
        resolve_entry(self.mem.clone(), offset)
    }
}
