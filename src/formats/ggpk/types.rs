use crossbeam::sync::ShardedLock;
use memmap::Mmap;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(Debug, Default)]
pub struct Digest(pub [u8; 32]);

pub type NameHash = u32;
pub type StorageOffset = u64;
pub(crate) type Tag = [u8; 4];

pub(crate) type RecordHeader = (u32, Tag);

#[derive(Debug, Clone)]
pub struct Ggpk {
    pub(crate) mem: Arc<Mmap>,
    pub(crate) root: StorageOffset,
    pub(crate) free: Option<StorageOffset>,
    pub(crate) parents: Arc<ShardedLock<HashMap<StorageOffset, StorageOffset>>>,
}

#[derive(Debug, Clone)]
pub struct Directory {
    pub(crate) mem: Arc<Mmap>,
    pub offset: StorageOffset,
    pub(crate) name_len: usize,
    pub(crate) name_base: StorageOffset,
    pub(crate) child_count: usize,
    pub(crate) child_base: StorageOffset,
}

#[derive(Debug, Clone)]
pub struct File {
    pub(crate) mem: Arc<Mmap>,
    pub offset: StorageOffset,
    pub(crate) name_len: usize,
    pub(crate) name_base: StorageOffset,
    pub(crate) data_size: usize,
    pub(crate) data_base: StorageOffset,
}

#[derive(Debug, Clone)]
pub struct Entry {
    pub(crate) mem: Arc<Mmap>,
    pub hash: u32,
    pub offset: u64,
}

#[derive(Debug, Clone)]
pub enum ResolvedEntry {
    Directory(Directory),
    File(File),
}

pub trait NodeInfo {
    fn compute_digest(&self) -> Digest;
    fn digest(&self) -> Digest;
    fn name(&self) -> String;
    fn name16(&self) -> widestring::U16CString;
    fn offset(&self) -> u64;
}

#[derive(Debug, Clone)]
pub struct Entries {
    pub(crate) mem: Arc<Mmap>,
    pub(crate) base: StorageOffset,
    pub(crate) index: usize,
    pub(crate) count: usize,
}

pub trait ReadGgpkExt {
    fn read_digest(&mut self) -> std::io::Result<Digest>;
    fn read_record_header(&mut self) -> std::io::Result<RecordHeader>;
    fn read_tag(&mut self) -> std::io::Result<Tag>;
    fn read_sized_terminated_string_u16(&mut self, code_units: usize) -> std::io::Result<Vec<u16>>;
}
