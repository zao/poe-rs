mod types;
pub use types::*;

use nalgebra_glm as glm;

/*
version 5
SkinnedMeshData "Art/Microtransactions/spell/raise_spectre_celestial/rig_7d539149.smd"
Materials 2
	"Art/Microtransactions/spell/raise_spectre_celestial/textures/space-01c.mat" 1
	"Art/Microtransactions/spell/raise_spectre_celestial/textures/addc.mat" 1
BoundingBox -125.568 -125.568 -10.6485 125.568 125.568 -5.89479
*/

pub fn parse_sm(data: &[u8]) -> failure::Fallible<Sm> {
    match crate::util::vec_to_utf16(&data) {
        None => bail!("SM file not valid UTF-16"),
        Some(contents) => {
            use regex::Regex;
            lazy_static! {
                static ref VERSION_RE: Regex = Regex::new(r#"^version (\d+)$"#).unwrap();
                static ref SKINNED_MESH_DATA_RE: Regex = Regex::new(r#"^SkinnedMeshData "([^"]*)"$"#).unwrap();
                static ref MATERIALS_RE: Regex = Regex::new(r#"^Materials (\d+)$"#).unwrap();
                static ref MATERIAL_RE: Regex = Regex::new(r#"^\s+"([^"]*)" (\d+)$"#).unwrap();
                static ref BOUNDING_BOX_RE: Regex = Regex::new(&format!(
                    "^BoundingBox {0} {0} {0} {0} {0} {0}$", "([-+e.0-9]+)"
                )).unwrap();
            }
            let mut lines = contents.lines();

            let version = {
                let caps = VERSION_RE.captures(lines.next().ok_or_else(|| format_err!("Version field missing"))?).ok_or_else(|| format_err!("Version field malformed"))?;
                let version: usize = caps.get(1).unwrap().as_str().parse().unwrap();
                if version < 4 || version > 5 {
                    bail!("Unknown SM version {}", version);
                }
                version
            };

            let skinned_mesh_data = {
                let caps = SKINNED_MESH_DATA_RE.captures(lines.next().ok_or_else(|| format_err!("SkinnedMeshData field missing"))?).ok_or_else(|| format_err!("SkinnedMeshData field malformed"))?;
                caps.get(1).unwrap().as_str().to_owned()
            };
            
            let materials = {
                let caps = MATERIALS_RE.captures(lines.next().ok_or_else(|| format_err!("Materials field missing"))?).ok_or_else(|| format_err!("Materials field malformed"))?;
                let count = caps.get(1).unwrap().as_str().parse().unwrap();
                let mut v = Vec::with_capacity(count);
                for _ in 0..count {
                    let caps = MATERIAL_RE.captures(lines.next().ok_or_else(|| format_err!("Material field missing"))?).ok_or_else(|| format_err!("Material field malformed"))?;
                    let path = caps.get(1).unwrap().as_str().to_owned();
                    let count: usize = caps.get(2).unwrap().as_str().parse().unwrap();
                    v.push(SmMaterial{ path, count });
                }
                v
            };
            let bounding_box = if version == 5 {
                let caps = BOUNDING_BOX_RE.captures(lines.next().ok_or_else(|| format_err!("BoundingBox field missing"))?).ok_or_else(|| format_err!("BoundingBox field malformed"))?;
                let values = caps.iter().skip(1).map(|cap| cap.unwrap().as_str().parse()).collect::<Result<Vec<_>, _>>();
                let values = values?;
                Some(SmBoundingBox { min: glm::make_vec3(&values[0..3]), max: glm::make_vec3(&values[3..6]) })
            }
            else {
                None
            };

            Ok(Sm {
                version,
                skinned_mesh_data,
                materials,
                bounding_box,
            })
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn survey_sm() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;
        use std::collections::{BTreeMap, BTreeSet};

        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|scope| {
            let (all_keys_tx, all_keys_rx) = crossbeam::channel::unbounded();
            let (mat_count_tx, mat_count_rx) = crossbeam::channel::unbounded();
            let (versions_tx, versions_rx) = crossbeam::channel::unbounded();
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                scope.spawn(move |_| {
                    file_rx.iter().par_bridge().for_each(|(_dir, file): (ggpk::Directory, ggpk::File)| {
                        if file.name().to_lowercase().ends_with(".sm") {
                            if let Some(text) = crate::util::vec_to_utf16(file.contents()) {
                                let lines = text.lines();
                                let mut all_keys = BTreeSet::new();
                                let mut version = None;
                                lines.for_each(|line| {
                                    if !line.is_empty() && !line.chars().next().unwrap().is_whitespace() {
                                        let mut parts = line.splitn(2, ' ').filter(|s| !s.is_empty());
                                        let key = parts.next().unwrap();
                                        if key == "version" {
                                            let value = parts.next().unwrap().to_owned();
                                            version = Some(value.clone());
                                            versions_tx.send(value).unwrap();
                                        }
                                        all_keys.insert(key.to_owned());
                                    }
                                    else if !line.is_empty() {
                                        let mut parts = line.rsplitn(2, ' ');
                                        let count = parts.next().unwrap();
                                        mat_count_tx.send(count.to_owned()).unwrap();
                                    }
                                });
                                all_keys_tx.send((version.unwrap(), all_keys)).unwrap();
                            }
                        }
                    });
                });
            }
            crate::util::visit_ggpk_files(&pack, file_tx, "/Art").unwrap();
            let ak = all_keys_rx.iter().collect::<Vec<(String, BTreeSet<String>)>>();
            let ak_iter = ak.iter();
            let common = ak_iter.fold(<BTreeMap<String, BTreeSet<String>>>::new(), |mut sets, (version, keys)| {
                sets.entry(version.clone()).and_modify(|set| *set = set.intersection(&keys).cloned().collect()).or_insert_with(|| keys.clone());
                sets
            });
            eprintln!("common: {:#?}", common);

            let ak_iter = ak.iter();
            let all = ak_iter.fold(<BTreeMap<String, BTreeSet<String>>>::new(), |mut sets, (version, keys)| {
                sets.entry(version.clone()).and_modify(|set| *set = set.union(&keys).cloned().collect()).or_insert_with(|| keys.clone());
                sets
            });
            eprintln!("all: {:#?}", all);

            let counts = mat_count_rx.iter().collect::<BTreeSet<_>>();
            eprintln!("{:#?}", counts);

            let versions = versions_rx.iter().collect::<BTreeSet<_>>();
            eprintln!("{:#?}", versions);
        }).unwrap();
        Ok(())
    }

    #[test]
    fn parse_all_sm() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;

        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|scope| {
            let (sm_tx, sm_rx) = crossbeam::channel::unbounded();
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                let pack = pack.clone();
                scope.spawn(move |_| {
                    file_rx.iter().par_bridge().for_each(|(_dir, file): (ggpk::Directory, ggpk::File)| {
                        if file.name().to_lowercase().ends_with(".sm") {
                            if let Some(_text) = crate::util::vec_to_utf16(file.contents()) {
                                let res = super::parse_sm(file.contents());
                                sm_tx.send((file, res)).unwrap();
                            }
                            else {
                                eprintln!("{} not UTF-16", crate::util::find_full_path(&pack, file.offset));
                            }
                        }
                    });
                });
            }
            crate::util::visit_ggpk_files(&pack, file_tx, "/Art").unwrap();

            let sms = sm_rx.iter().collect::<Vec<_>>();
            for sm in &sms {
                match &sm.1 {
                    Ok(_sm) => {},
                    Err(e) => eprintln!("{} - {}", crate::util::find_full_path(&pack, sm.0.offset), e),
                }
            }
            for res in sms.iter() {
                if let Err(e) = &res.1 {
                  bail!("{}", e);  
                }
            }
            Ok(())
        }).unwrap()?;
        Ok(())
    }
}