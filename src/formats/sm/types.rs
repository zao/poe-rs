use nalgebra_glm as glm;

#[derive(Debug)]
pub struct Sm {
    pub version: usize,
    pub skinned_mesh_data: String,
    pub materials: Vec<SmMaterial>,
    pub bounding_box: Option<SmBoundingBox>,
}

#[derive(Debug)]
pub struct SmMaterial {
    pub path: String,
    pub count: usize,
}

#[derive(Debug)]
pub struct SmBoundingBox {
    pub min: glm::Vec3,
    pub max: glm::Vec3,
}