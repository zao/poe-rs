#[derive(Debug, Default)]
pub struct Smd {
    pub version: u8,
    pub unk_count: usize,
    pub bbox: [f32; 6],
    pub shape_names: Vec<String>,
    pub shape_triangle_offsets: Vec<u32>,
    pub triangle_indices: Indices,
    pub vertex_data: Vec<Vertex>,
    pub trailer: Vec<u8>,
    pub tail: Tail,
}

#[derive(Debug)]
pub enum Indices {
    Short(Vec<u16>),
    Long(Vec<u32>),
}

impl Default for Indices {
    fn default() -> Self {
        Indices::Short(vec![])
    }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub unknown: [u8; 8],
    pub uv: [u16; 2],
    pub bone_indices: [u8; 4],
    pub bone_weights: [u8; 4],
}

#[derive(Debug, Default, Clone)]
pub struct Tail {
    pub version: u32,
    pub sec0: Vec<Tail0>,
    pub sec1: Vec<Tail1>,
    pub sec2: Vec<Tail2>,
    pub sec3: Vec<()>,
    pub sec4: Vec<Tail4>,
    pub sec5: Vec<Tail5>,
    pub sec6: Vec<Tail6>,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Tail0 {
    pub f_0: [f32; 15],
    pub u_60: u32,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Tail1 {
    pub f_0: [f32; 4],
    pub u_16: u32,
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Tail2 {
    pub u_0: u32,
    pub u_4: u32,
}

// tail_counts[3] is always zero in all files
#[derive(Debug, Default, Clone, Copy)]
pub struct Tail3 {}

#[derive(Debug, Default, Clone, Copy)]
pub struct Tail4 {
    pub vec_0: [f32; 3],
    pub index_12: [u32; 4],
    pub weight_28: [f32; 4],
}

// Sections 5 and 6 are both u32:s, might be swapped
// #[derive(Debug, Default, Clone, Copy)]
pub type Tail5 = u32;

// #[derive(Debug, Default, Clone, Copy)]
pub type Tail6 = u32;

#[derive(Debug, Fail)]
pub enum ParseError {
    #[fail(display = "Unknown version {}.", _0)]
    UnknownVersion(u8),

    #[fail(display = "Truncated file.")]
    Truncated,

    #[fail(display = "An error occurred.")]
    Generic,
}
