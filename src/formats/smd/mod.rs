mod types;

pub use types::*;

use byteorder::{ReadBytesExt, LE};
use failure::Fallible;

struct ShapeInfo {
    name_byte_count: u32,
    triangle_offset: u32,
}

struct TailReader<R> where R: std::io::Read {
    r: R,
}

impl<R> TailReader<R> where R: std::io::Read {
    fn new(r: R) -> Self {
        TailReader { r }
    }

    fn read(&mut self) -> failure::Fallible<Tail> {
        let version = self.r.read_u32::<LE>()?;
        match version {
            2 => {
                let counts = {
                    let mut v = [0u32; 4];
                    self.r.read_u32_into::<LE>(&mut v)?;
                    v
                };
                let sec4 = self.read_4s(counts[1])?;
                let sec5 = self.read_5s(counts[2])?;
                let sec0 = self.read_0s(counts[0])?;
                let sec6 = self.read_6s(counts[3])?;
                if self.r.read_u8().is_ok() {
                    bail!("Oversized v2 trailer");
                }
                Ok(Tail {
                    version,
                    sec0,
                    sec1: vec![],
                    sec2: vec![],
                    sec3: vec![],
                    sec4,
                    sec5,
                    sec6,
                })
            }
            3 => {
                let counts = {
                    let mut v = [0u32; 7];
                    self.r.read_u32_into::<LE>(&mut v)?;
                    v
                };
                let sec0 = self.read_0s(counts[0])?;
                let sec1 = self.read_1s(counts[1])?;
                let sec2 = self.read_2s(counts[2])?;
                let sec4 = self.read_4s(counts[4])?;
                let sec5 = self.read_5s(counts[5])?;
                let sec6 = self.read_6s(counts[6])?;
                if self.r.read_u8().is_ok() {
                    bail!("Oversized v3 trailer");
                }
                Ok(Tail {
                    version,
                    sec0,
                    sec1,
                    sec2,
                    sec3: vec![],
                    sec4,
                    sec5,
                    sec6,
                })
            }
            _ => bail!("Tail version {} not 2 or 3", version),
        }
    }

    fn read_0s(&mut self, n: u32) -> std::io::Result<Vec<Tail0>> {
        let mut read_one = || -> std::io::Result<Tail0> {
            let mut f_0 = [0.0f32; 15];
            self.r.read_f32_into::<LE>(&mut f_0)?;
            let u_60 = self.r.read_u32::<LE>()?;
            Ok(Tail0 { f_0, u_60 })
        };
        let mut v = Vec::with_capacity(n as usize);
        for _ in 0..n {
            v.push(read_one()?);
        }
        Ok(v)
    }

    fn read_1s(&mut self, n: u32) -> std::io::Result<Vec<Tail1>> {
        let mut read_one = || -> std::io::Result<Tail1> {
            let mut f_0 = [0.0f32; 4];
            self.r.read_f32_into::<LE>(&mut f_0)?;
            let u_16 = self.r.read_u32::<LE>()?;
            Ok(Tail1 { f_0, u_16 })
        };
        let mut v = Vec::with_capacity(n as usize);
        for _ in 0..n {
            v.push(read_one()?);
        }
        Ok(v)
    }

    fn read_2s(&mut self, n: u32) -> std::io::Result<Vec<Tail2>> {
        let mut read_one = || -> std::io::Result<Tail2> {
            let u_0 = self.r.read_u32::<LE>()?;
            let u_4 = self.r.read_u32::<LE>()?;
            Ok(Tail2 { u_0, u_4 })
        };
        let mut v = Vec::with_capacity(n as usize);
        for _ in 0..n {
            v.push(read_one()?);
        }
        Ok(v)
    }

    // read_3s left unimplemented

    fn read_4s(&mut self, n: u32) -> std::io::Result<Vec<Tail4>> {
        let mut read_one = || -> std::io::Result<Tail4> {
            let mut vec_0 = [0.0f32; 3];
            self.r.read_f32_into::<LE>(&mut vec_0)?;
            let mut index_12 = [0u32; 4];
            self.r.read_u32_into::<LE>(&mut index_12)?;
            let mut weight_28 = [0.0f32; 4];
            self.r.read_f32_into::<LE>(&mut weight_28)?;
            Ok(Tail4 { vec_0, index_12, weight_28 })
        };
        let mut v = Vec::with_capacity(n as usize);
        for _ in 0..n {
            v.push(read_one()?);
        }
        Ok(v)
    }

    fn read_5s(&mut self, n: u32) -> std::io::Result<Vec<Tail5>> {
        let mut v = vec![0u32; n as usize];
        self.r.read_u32_into::<LE>(&mut v)?;
        Ok(v)
    }

    fn read_6s(&mut self, n: u32) -> std::io::Result<Vec<Tail6>> {
        let mut v = vec![0u32; n as usize];
        self.r.read_u32_into::<LE>(&mut v)?;
        Ok(v)
    }
}

pub fn parse_smd(data: &[u8]) -> Fallible<Smd> {
    use std::io::Read;

    let data = crate::util::decompress(data)?;
    let mut r = &data[..];
    let version = r.read_u8()?;
    if version != 1 {
        bail!(ParseError::UnknownVersion(version));
    }
    let triangle_count = r.read_u32::<LE>()? as usize;
    let vertex_count = r.read_u32::<LE>()? as usize;
    let _const_04 = r.read_u8()?;
    let shape_count = r.read_u16::<LE>()? as usize;
    let unk_count = r.read_u32::<LE>()? as usize;

    let bbox = {
        let mut v = [0.0f32; 6];
        r.read_f32_into::<LE>(&mut v)?;
        v
    };

    // Shape list
    let shape_info_list = {
        let mut v = Vec::with_capacity(shape_count);
        for _ in 0..shape_count {
            v.push(ShapeInfo {
                name_byte_count: r.read_u32::<LE>()?,
                triangle_offset: r.read_u32::<LE>()?,
            });
        }
        v
    };

    let shape_triangle_offsets = shape_info_list
        .iter()
        .map(|si| si.triangle_offset)
        .collect();

    // Shape name list
    let shape_names = {
        let mut v = Vec::with_capacity(shape_count);
        for shape_info in &shape_info_list {
            let byte_count = shape_info.name_byte_count as usize;
            let unit_count = byte_count / 2;
            let mut codeunits = vec![0; unit_count];
            r.read_u16_into::<LE>(&mut codeunits)
                .map_err(|_| ParseError::Truncated)?;
            v.push(String::from_utf16_lossy(&codeunits));
        }
        v
    };

    // Indexed triangle list
    let triangle_indices = if vertex_count > 0xFFFF {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        r.read_u32_into::<LE>(&mut v)?;
        Indices::Long(v)
    } else {
        let mut v = vec![];
        v.resize(triangle_count * 3, 0);
        r.read_u16_into::<LE>(&mut v)?;
        Indices::Short(v)
    };

    // Vertex data list
    let vertex_data = {
        let mut v = Vec::with_capacity(vertex_count);
        for _ in 0..vertex_count {
            let position = [
                r.read_f32::<LE>()?,
                r.read_f32::<LE>()?,
                r.read_f32::<LE>()?,
            ];
            let mut unknown = [0u8; 8];
            r.read_exact(&mut unknown)?;
            let uv = [r.read_u16::<LE>()?, r.read_u16::<LE>()?];
            let mut bone_indices = [0u8; 4];
            r.read_exact(&mut bone_indices)?;
            let mut bone_weights = [0u8; 4];
            r.read_exact(&mut bone_weights)?;
            let vertex = Vertex {
                position,
                unknown,
                uv,
                bone_indices,
                bone_weights,
            };
            v.push(vertex);
        }
        v
    };

    // Extract trailer for troubleshooting
    let mut trailer = Vec::new();
    r.read_to_end(&mut trailer)?;

    // Also parse the trailer
    let mut r = &trailer[..];
    let tail = TailReader::new(&mut r).read()?;

    Ok(Smd {
        version,
        unk_count,
        bbox,
        shape_names,
        shape_triangle_offsets,
        triangle_indices,
        vertex_data,
        trailer,
        tail,
    })
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_smd() -> failure::Fallible<()> {
        let pack_path = r#"C:\Temp\GGPKs\Content-3.6.6c.ggpk"#;
        let pack = crate::formats::ggpk::Ggpk::open(pack_path)?;
        let smd_file = pack.resolve(2_903_357_359u64)?;
        if let crate::formats::ggpk::ResolvedEntry::File(file) = smd_file {
            let smd = super::parse_smd(file.contents())?;
            println!("SMD: {:#?}", &smd);
        }
        Ok(())
    }

    #[derive(Debug)]
    enum F32OrU32 {
        Neither,
        F32(f32),
        U32(u32),
        Both(f32, u32),
    }

    #[test]
    fn test_all_smd() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        use rayon::prelude::*;
        use std::collections::BTreeMap;
        let pack_path = r#"Content.ggpk"#;
        let pack = ggpk::Ggpk::open(pack_path)?;

        let (tx, rx) = crossbeam::channel::bounded::<(ggpk::Directory, ggpk::File)>(4096);
        let t = {
            let pack = pack.clone();
            std::thread::spawn(move || {
                crate::util::visit_ggpk_files(&pack, tx, "/").unwrap();
            })
        };

        let (group_tx, group_rx) = crossbeam::channel::unbounded::<(ggpk::File, super::Smd)>();
        rx.iter().par_bridge().for_each(|(_parent, file)| {
            if file.name().ends_with(".smd") {
                let res = super::parse_smd(file.contents());
                match res {
                    Ok(smd) => {
                        // eprintln!("{} - {}", crate::util::find_full_path(&pack, file.offset), smd.trailer.len());
                        // eprintln!("{} - {:?}", crate::util::find_full_path(&pack, file.offset), smd.tail);
                        group_tx.send((file, smd)).unwrap();
                    }
                    Err(e) => {
                        eprintln!("{} - {}", crate::util::find_full_path(&pack, file.offset), e);
                    }
                }
                // println!("SMD: {:#?}", &smd);
            }
        });
        drop(group_tx);
        let by_len = group_rx.iter().fold(<BTreeMap<usize, Vec<(ggpk::File, super::Smd)>>>::new(), |mut g, t| {
            g.entry(t.1.trailer.len()).or_default().push(t);
            g
        });

        use std::io::Write;

        let mut report = std::fs::File::create("smd.html").unwrap();
        writeln!(report, "<html><head>").unwrap();
        writeln!(report, r#"<style>
table {{ display: inline; border: 1px solid; }}
td {{ border: 1px dashed; }}
/* Remove default bullets */
ul, #tree_list {{
  list-style-type: none;
}}

/* Remove margins and padding from the parent ul */
#tree_ul {{
  margin: 0;
  padding: 0;
}}

/* Style the caret/arrow */
.caret {{
  cursor: pointer;
  user-select: none; /* Prevent text selection */
}}

/* Create the caret/arrow with a unicode, and style it */
.caret::before {{
  content: "\25BC";
  color: black;
  display: inline-block;
  margin-right: 6px;
}}

/* Rotate the caret/arrow icon when clicked on (using JavaScript) */
.caret-down::before {{
  transform: rotate(-90deg);
}}

/* Hide the nested list */
.nested {{
  display: none;
}}

/* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
.active {{
  display: block;
}}
</style>"#).unwrap();
        writeln!(report, "</head><body>").unwrap();
        writeln!(report, r#"<ul id="tree_ul">"#).unwrap();
        for (len, files) in &by_len {
            writeln!(report, r#"<li><h1 class="caret">{}</h1><div class="nested active">"#, len).unwrap();
            use byteorder::{LE, ReadBytesExt};
            let files = files.iter().filter(|(_f, smd)| smd.trailer[0] == 0x3 && smd.trailer[4 * 4] > 0).collect::<Vec<_>>();
            let names = files.iter().map(|(f, _smd)| crate::util::find_full_path(&pack, f.offset)).collect::<Vec<_>>();
            let by_fours = files.iter().map(|(_f, smd)| {
                use std::io::{Seek, SeekFrom};
                let mut r = std::io::Cursor::new(&smd.trailer[..]);
                let mut v1 = vec![0.0f32; smd.trailer.len() / 4];
                r.read_f32_into::<LE>(&mut v1[..]).unwrap();
                r.seek(SeekFrom::Start(0)).unwrap();
                let mut v2 = vec![0u32; smd.trailer.len() / 4];
                r.read_u32_into::<LE>(&mut v2[..]).unwrap();
                v1.into_iter().map(|f| if f == 0.0 || f.abs() > 0.000_000_1 {
                    Some(f)
                } else {
                    None
                }).zip(v2.into_iter().map(
                    |u| if u <= 0x00FF_FFFF {
                        Some(u)
                    } else {
                        None
                    })).collect::<Vec<_>>()
            }).collect::<Vec<_>>();
            let blarghs = by_fours.iter().map(|v| {
                v.iter().map(|x| match x {
                    (Some(f), Some(u)) => F32OrU32::Both(*f, *u),
                    (Some(f), None) => F32OrU32::F32(*f),
                    (None, Some(u)) => F32OrU32::U32(*u),
                    _ => F32OrU32::Neither,
                }).collect::<Vec<_>>()
            }).collect::<Vec<_>>();
            // if *len >= 16000 && *len < 19000 {
            {
                for (name, blargh) in names.iter().zip(blarghs.iter()) {
                    // writeln!(report, "<h2>{}</h2>", name).unwrap();
                    writeln!(report, r#"<table title="{name}"><tr><th><a href="{name}">Word</a></th><th>f32</th><th>u32</th></tr>"#, name = name).unwrap();
                    for (i, b) in blargh.iter().enumerate() {
                        match b {
                            F32OrU32::Neither => writeln!(report, "<tr><td>{}</td><td/><td/></tr>", i),
                            F32OrU32::F32(f) => writeln!(report, "<tr><td>{}</td><td>{}</td><td/></tr>", i, f),
                            F32OrU32::U32(u) => writeln!(report, "<tr><td>{}</td><td/><td>{}</td>", i, u),
                            F32OrU32::Both(f, u) => writeln!(report, "<tr><td>{}</td><td>{}</td><td>{}</td></tr>", i, f, u),
                        }.unwrap();
                    }
                    writeln!(report, "</table>").unwrap();
                }
            }
            writeln!(report, "</div></li>").unwrap();
        }
        writeln!(report, "</ul>").unwrap();
        writeln!(report, r#"<script>
var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {{
  toggler[i].addEventListener("click", function() {{
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  }});
}}</script>"#).unwrap();
        writeln!(report, "</body></html>").unwrap();

        t.join().unwrap();
        Ok(())
    }

    #[test]
    fn list_data_contents() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use ggpk::NodeInfo;
        let pack_path = r"C:\Temp\GGPKs\Content-3.7.2.ggpk";
        let pack = crate::formats::ggpk::Ggpk::open(pack_path)?;

        let (tx, rx) = crossbeam::channel::bounded::<(ggpk::Directory, ggpk::File)>(128);
        let t = std::thread::spawn(move || {
            crate::util::visit_ggpk_files(&pack, tx, "/").unwrap();
        });

        while let Ok((_parent, file)) = rx.recv() {
            eprintln!("{}", file.name());
        }

        t.join().unwrap();
        Ok(())
    }
}
