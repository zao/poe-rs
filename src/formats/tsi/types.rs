use std::collections::HashMap;

#[derive(Debug)]
pub struct Tsi {
    pub version: usize,
    pub fields: HashMap<String, String>,
}
