mod types;
pub use types::*;

pub fn parse_tsi(contents: &str) -> failure::Fallible<Tsi> {
    use crate::parse::ParseOps;
    use std::collections::HashMap;

    let mut lines = contents.lines().filter(|line| !line.is_empty());
    || -> Option<Tsi> {
        // version 2
        let version = {
            let mut po = ParseOps::new(lines.next()?);
            po.word()?;
            po.usize()?
        };

        let mut fields = HashMap::new();
        for line in lines {
            let mut po = ParseOps::new(line);
            let key = po.word()?.to_owned();
            let value = po.tail().trim_start().to_owned();
            fields.insert(key, value);
        }

        Some(Tsi { version, fields })
    }()
    .ok_or_else(|| format_err!("Failed to parse TSI file"))
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_tsi_files() -> failure::Fallible<()> {
        time_test!();
        use crate::{formats::ggpk, util};
        let pack = ggpk::Ggpk::open(r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#)?;
        let (tx, rx) = crossbeam::channel::bounded(128);
        let t = {
            let pack = pack.clone();
            std::thread::spawn(move || {
                util::visit_ggpk_files(&pack, tx, "/").unwrap();
            })
        };

        while let Ok((_, file)) = rx.recv() {
            use ggpk::NodeInfo;
            if file.name().to_lowercase().ends_with(".tsi") {
                let full_path = crate::util::find_full_path(&pack, file.offset());
                let text = crate::util::vec_to_utf16(file.contents())
                    .ok_or_else(|| format_err!("TSI file not UTF-16LE"));
                let res = super::parse_tsi(&text.unwrap());
                if res.is_err() {
                    eprintln!("{}: {:?}", full_path, res);
                    panic!();
                }
            }
        }

        t.join().unwrap();
        Ok(())
    }
}
