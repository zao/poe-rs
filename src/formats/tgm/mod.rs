mod types;
pub use types::*;

use byteorder::ReadBytesExt;
use std::collections::BTreeMap;
use std::io::{Seek, SeekFrom};

pub fn parse_tgm(data: &[u8]) -> failure::Fallible<Tgm> {
    let mut r = data;
    let version = r.read_u8()? as usize;
    let next = r.read_u8()?;
    Ok(Tgm { version, next })
}

#[derive(Debug, Clone)]
#[derive(Default)]
pub struct Markup {
    files: BTreeMap<String, MarkupInstance>,
}

impl Markup {
    pub fn new() -> Self {
        Markup {
            files: BTreeMap::new(),
        }
    }
}

#[derive(Debug, Clone)]
#[derive(Default)]
pub struct MarkupInstance {
    scalars: BTreeMap<u64, u64>,
    // tags: Vec<(u64, u64, String)>,
    tags: Vec<(u64, String)>,
    // pending_tags: Vec<PendingTag>,
}

// #[derive(Debug, Clone)]
// struct PendingTag {
//     tag: String,
//     at: u64,
// }

impl MarkupInstance {
    pub fn new() -> Self {
        MarkupInstance {
            scalars: BTreeMap::new(),
            tags: Vec::new(),
            // pending_tags: vec![],
        }
    }

    // fn start_tag<S: AsRef<str>, R: Seek>(&mut self, tag: S, r: &mut R) {
    //     self.pending_tags.push(PendingTag {
    //         tag: tag.as_ref().to_owned(),
    //         at: r.seek(std::io::SeekFrom::Current(0)).unwrap(),
    //     });
    // }

    // fn end_tag<R: Seek>(&mut self, r: &mut R) {
    //     let pending = self.pending_tags.pop().unwrap();
    //     let end = r.seek(std::io::SeekFrom::Current(0)).unwrap();
    //     self.tags.push((pending.at, end, pending.tag));
    // }

    fn tag<S: AsRef<str>, R: Seek>(&mut self, tag: S, r: &mut R) {
        self.tags.push((
            r.seek(SeekFrom::Current(0)).unwrap(),
            tag.as_ref().to_owned(),
        ));
    }
}

pub fn markup_tgm(data: &[u8], markup: &mut MarkupInstance) -> failure::Fallible<()> {
    let mut r = std::io::Cursor::new(data);
    markup.tag("version", &mut r);
    let _version = r.read_u8()?;
    markup.tag("unhandled", &mut r);
    failure::bail!("oh no");
}

#[cfg(test)]
mod tests {
    #[test]
    fn parse_all_tgm() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use rayon::prelude::*;
        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|s| {
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                let pack = pack.clone();
                s.spawn(move |_| {
                    crate::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
                });
            }

            let results = file_rx
                .iter()
                .par_bridge()
                .filter_map(|(_dir, file)| {
                    use ggpk::NodeInfo;
                    if file.name().to_lowercase().ends_with(".tgm") {
                        let res =
                            super::parse_tgm(&crate::util::decompress(file.contents()).unwrap());
                        Some((file, res))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();
            let mut tally = vec![0usize; 256];
            results.iter().for_each(|(_file, r)| match r {
                Ok(tgm) => {
                    tally[tgm.next as usize] += 1;
                    // if tgm.next != 0 { eprintln!("version: {:08X}", tgm.version); }
                }
                Err(_e) => {
                    // eprintln!("error: {} - {}", crate::util::find_full_path(&pack, file.offset), e),
                }
            });
            for (i, xs) in tally.chunks(16).enumerate() {
                eprintln!("{:3}: {:6?}", i, xs);
            }
        })
        .unwrap();
        Ok(())
    }

    #[test]
    fn markup_all_tgm() -> failure::Fallible<()> {
        use crate::formats::ggpk;
        use rayon::prelude::*;
        let pack = ggpk::Ggpk::open("Content.ggpk")?;
        crossbeam::thread::scope(|s| {
            let (file_tx, file_rx) = crossbeam::channel::unbounded();
            {
                let pack = pack.clone();
                s.spawn(move |_| {
                    crate::util::visit_ggpk_files(&pack, file_tx, "/").unwrap();
                });
            }

            let results = file_rx
                .iter()
                .par_bridge()
                .filter_map(|(_dir, file)| {
                    use ggpk::NodeInfo;
                    if file.name().to_lowercase().ends_with(".tgm") {
                        let mut markup = super::MarkupInstance::new();
                        let res = super::markup_tgm(
                            &crate::util::decompress(file.contents()).unwrap(),
                            &mut markup,
                        );
                        Some((file, res, markup))
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();
            let mut markups = super::Markup::new();
            results.iter().for_each(|(file, r, markup)| {
                let filename = crate::util::find_full_path(&pack, file.offset);
                markups.files.insert(filename, markup.clone());
                match r {
                    Ok(_tgm) => {}
                    Err(_e) => {
                        // eprintln!("error: {} - {}", &filename, e);
                    }
                }
            });
            for x in markups.files.iter() {
                eprintln!("{} - {:?}", x.0, x.1);
            }
        })
        .unwrap();
        Ok(())
    }
}
