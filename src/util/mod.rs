pub mod survey;

use crate::formats::ggpk;

pub fn read_f16_le(r: &mut dyn std::io::Read) -> Result<half::f16, std::io::Error> {
    use byteorder::{ReadBytesExt, LE};
    let x = r.read_u16::<LE>()?;
    Ok(half::f16::from_bits(x))
}

pub fn read_f16_into_le(
    r: &mut dyn std::io::Read,
    buf: &mut [half::f16],
) -> Result<(), std::io::Error> {
    for x in buf.iter_mut() {
        *x = read_f16_le(r)?;
    }
    Ok(())
}

pub fn vec_to_utf16(bytes: &[u8]) -> Option<String> {
    use byteorder::{ReadBytesExt, LE};
    let mut code_units: Vec<u16> = Vec::new();
    code_units.resize(bytes.len() / 2, 0u16);
    let mut r = &bytes[..];
    if let Ok(bom) = r.read_u16::<LE>() {
        let offset = match bom {
            0xFEFF => {
                code_units.pop();
                0
            }
            _ => {
                code_units[0] = bom;
                1
            }
        };
        r.read_u16_into::<LE>(&mut code_units[offset..]).ok();
    }
    String::from_utf16(&code_units).ok()
}

pub fn resolve_symlinks(pack: &ggpk::Ggpk, file: ggpk::File) -> ggpk::File {
    let mut current_file = file;
    loop {
        let contents = current_file.contents();
        if contents.len() > 4 && contents.len() < 1024 && contents[0] == b'*' {
            if let Ok(path) = std::str::from_utf8(&contents[1..]) {
                if let Ok(ggpk::ResolvedEntry::File(file)) = pack.lookup(path) {
                    current_file = file;
                    continue;
                }
            }
        }
        break;
    }
    current_file
}

pub fn find_full_path(pack: &ggpk::Ggpk, offset: u64) -> String {
    use ggpk::NodeInfo;
    let mut offset = offset;
    let mut parts = Vec::new();
    let relations = pack.parents.read().unwrap();
    while let Ok(x) = pack.resolve(offset) {
        let name = x.name();
        if !name.is_empty() {
            parts.push(x.name());
        }
        if let Some(next) = relations.get(&offset) {
            offset = *next;
        } else {
            break;
        }
    }
    parts.reverse();
    parts.join("/")
}

pub fn decompress_without_magic_word(data: &[u8]) -> failure::Fallible<Vec<u8>> {
    use byteorder::{ReadBytesExt, LE};
    let mut r = &data[..];
    let uncompressed_size = r.read_u32::<LE>()? as usize;
    let mut buf = vec![0u8; uncompressed_size];
    let res = brotli_decompressor::brotli_decode(&data[4..], &mut buf[..]);
    if res.decoded_size != uncompressed_size {
        bail!(
            "Compressed data not of stated size: {} decoded, {} expected - {}",
            res.decoded_size,
            uncompressed_size,
            std::str::from_utf8(&res.error_string).unwrap(),
        );
    }
    if let brotli_decompressor::BrotliResult::ResultSuccess = res.result {
        Ok(buf)
    } else {
        bail!("{}", std::str::from_utf8(&res.error_string).unwrap());
    }
}

pub fn decompress(data: &[u8]) -> failure::Fallible<Vec<u8>> {
    use std::io::Read;
    let mut r = &data[..];
    {
        let mut tag = [0u8; 3];
        r.read_exact(&mut tag)?;
        if tag != [b'C', b'M', b'P'] {
            bail!("Not a compressed file");
        }
    }
    decompress_without_magic_word(&data[3..])
}

use std::borrow::Cow;
pub fn try_decompress(data: &[u8]) -> Cow<[u8]> {
    let res = decompress(data);
    if let Ok(data) = res {
        Cow::from(data)
    }
    else {
        Cow::from(data)
    }
}

pub fn try_decompress_without_magic_word(data: &[u8]) -> Cow<[u8]> {
    let res = decompress_without_magic_word(data);
    if let Ok(data) = res {
        Cow::from(data)
    }
    else {
        Cow::from(data)
    }
}

pub fn visit_ggpk_files(
    pack: &ggpk::Ggpk,
    file_tx: crossbeam::channel::Sender<(ggpk::Directory, ggpk::File)>,
    path: &str,
) -> failure::Fallible<()> {
    use std::collections::VecDeque;
    let root_dir = match pack.lookup(path)? {
        ggpk::ResolvedEntry::Directory(dir) => dir,
        _ => bail!("root directory not a directory"),
    };
    let mut dirs = <VecDeque<_>>::new();
    dirs.push_back(root_dir);
    while let Some(current_dir) = dirs.pop_front() {
        for entry in current_dir.entries() {
            let resolved = entry.resolve()?;
            match resolved {
                ggpk::ResolvedEntry::Directory(dir) => dirs.push_back(dir),
                ggpk::ResolvedEntry::File(file) => {
                    file_tx.send((current_dir.clone(), file))?;
                }
            };
        }
    }
    Ok(())
}

pub fn visit_ggpk_entries(
    pack: &ggpk::Ggpk,
    entry_tx: crossbeam::channel::Sender<(ggpk::Directory, ggpk::ResolvedEntry)>,
    path: &str,
) -> failure::Fallible<()> {
    use std::collections::VecDeque;
    let root_dir = match pack.lookup(path)? {
        ggpk::ResolvedEntry::Directory(dir) => dir,
        _ => bail!("root directory not a directory"),
    };
    let mut dirs = <VecDeque<_>>::new();
    dirs.push_back(root_dir);
    while let Some(current_dir) = dirs.pop_front() {
        for entry in current_dir.entries() {
            let resolved = entry.resolve()?;
            if let ggpk::ResolvedEntry::Directory(dir) = &resolved {
                dirs.push_back(dir.clone());
            }
            entry_tx.send((current_dir.clone(), resolved))?;
        }
    }
    Ok(())
}

pub fn visit_ggpk_entries_sync<F>(
    pack: &ggpk::Ggpk,
    mut entry_fn: F,
    path: &str,
) -> failure::Fallible<()>
where
    F: FnMut(ggpk::Directory, ggpk::ResolvedEntry) -> failure::Fallible<()>,
{
    use std::collections::VecDeque;
    let root_dir = match pack.lookup(path)? {
        ggpk::ResolvedEntry::Directory(dir) => dir,
        _ => bail!("root directory not a directory"),
    };
    let mut dirs = <VecDeque<_>>::new();
    dirs.push_back(root_dir);
    while let Some(current_dir) = dirs.pop_front() {
        for entry in current_dir.entries() {
            let resolved = entry.resolve()?;
            if let ggpk::ResolvedEntry::Directory(dir) = &resolved {
                dirs.push_back(dir.clone());
            }
            entry_fn(current_dir.clone(), resolved)?;
        }
    }
    Ok(())
}

pub fn visit_ggpk_files_sync<F>(
    pack: &ggpk::Ggpk,
    mut file_fn: F,
    path: &str,
) -> failure::Fallible<()>
where
    F: FnMut(ggpk::Directory, ggpk::File) -> failure::Fallible<()>,
{
    visit_ggpk_entries_sync(pack, |parent, entry| match entry {
        ggpk::ResolvedEntry::File(file) => file_fn(parent, file),
        _ => Ok(()),
    }, path)
}

pub fn strip_quotes(s: &str) -> Option<String> {
    let chars = s.chars().collect::<Vec<_>>();
    if *chars.first()? != '"' || *chars.last()? != '"' {
        return None;
    }
    
    Some(chars[1..chars.len() - 1].iter().collect())
}
