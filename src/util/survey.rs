use byteorder::{ReadBytesExt, LE};
use half::f16;
use std::collections::BTreeMap;
use std::io::{Read, Seek, SeekFrom};
use std::sync::{Arc, Mutex};

#[derive(Debug)]
pub enum Value {
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
    F16(f16),
    F16_2([f16; 2]),
    F16_3([f16; 3]),
    F16_4([f16; 4]),
    F32(f32),
    F32_2([f32; 2]),
    F32_3([f32; 3]),
    F32_4([f32; 4]),
    F64(f64),
    F64_2([f64; 2]),
    F64_3([f64; 3]),
    F64_4([f64; 4]),
}

#[derive(Debug, Default, Clone)]
pub struct Reference {
    pub scalars: BTreeMap<String, u64>,
}

pub trait Lens {
    fn evaluate(&self, value: &Value, reference: &Reference) -> Vec<&str>;
}

#[derive(Default)]
pub struct PrimitiveCountLens {}

impl PrimitiveCountLens {
    pub fn new() -> Self {
        PrimitiveCountLens {}
    }
}

impl Lens for PrimitiveCountLens {
    fn evaluate(&self, value: &Value, _reference: &Reference) -> Vec<&str> {
        use Value::*;
        match value {
            U8(_) => vec!["u8"],
            U16(_) => vec!["u16"],
            U32(_) => vec!["u32"],
            U64(_) => vec!["u64"],
            F16(_) => vec!["f16"],
            F16_2(_) => vec!["f16[2]"],
            F16_3(_) => vec!["f16[3]"],
            F16_4(_) => vec!["f16[4]"],
            F32(_) => vec!["f32"],
            F32_2(_) => vec!["f32[2]"],
            F32_3(_) => vec!["f32[3]"],
            F32_4(_) => vec!["f32[4]"],
            F64(_) => vec!["f64"],
            F64_2(_) => vec!["f64[2]"],
            F64_3(_) => vec!["f64[3]"],
            F64_4(_) => vec!["f64[4]"],
        }
    }
}

#[derive(Default)]
pub struct QuaternionLens {}

impl QuaternionLens {
    pub fn new() -> Self {
        QuaternionLens {}
    }
}

impl Lens for QuaternionLens {
    fn evaluate(&self, value: &Value, _reference: &Reference) -> Vec<&str> {
        use Value::*;
        if let Some((label, eps, x)) = match value {
            F16_4(x) => Some((
                "quat16",
                0.001,
                [x[0].to_f64(), x[1].to_f64(), x[2].to_f64(), x[3].to_f64()],
            )),
            F32_4(x) => Some((
                "quat32",
                0.00001,
                [x[0] as f64, x[1] as f64, x[2] as f64, x[3] as f64],
            )),
            F64_4(x) => Some(("quat64", 0.000_000_1, *x)),
            _ => None,
        } {
            let mag = x[0] * x[0] + x[1] * x[1] + x[2] * x[2] + x[3] * x[3];
            if (mag - 1.0).abs() < eps {
                return vec![label];
            }
        }
        vec![]
    }
}

#[derive(Default)]
pub struct ReasonableFloatLens {}

impl ReasonableFloatLens {
    pub fn new() -> Self {
        ReasonableFloatLens {}
    }
}

impl Lens for ReasonableFloatLens {
    fn evaluate(&self, value: &Value, _reference: &Reference) -> Vec<&str> {
        use Value::*;
        let ok_range = |x: f64| x == 0.0 || x > 0.000_000_1 && x < 1_000_000.0;
        if let Some((label, v)) = match &value {
            F32(x) => Some(("f32_ok", vec![*x as f64])),
            F64(x) => Some(("f64_ok", vec![*x])),
            F32_2(x) => Some(("f32_2", x.iter().map(|x| *x as f64).collect())),
            F32_3(x) => Some(("f32_3", x.iter().map(|x| *x as f64).collect())),
            F32_4(x) => Some(("f32_4", x.iter().map(|x| *x as f64).collect())),
            F64_2(x) => Some(("f64_2", x.to_vec())),
            F64_3(x) => Some(("f64_3", x.to_vec())),
            F64_4(x) => Some(("f64_4", x.to_vec())),
            _ => None,
        } {
            if v.iter().all(|&x| ok_range(x)) {
                return vec![label];
            }
        }
        vec![]
    }
}

#[derive(Debug, Default)]
pub struct Survey {
    pub results: Mutex<BTreeMap<u64, (Reference, Vec<Value>)>>,
}

type Region = (usize, String, u64, u64);

#[derive(Debug, Default)]
pub struct Instance {
    pub survey: Arc<Mutex<Survey>>,
    pub depth: usize,
    pub pending_regions: Vec<(usize, String, u64)>,
    pub regions: Vec<Region>,
}

impl Instance {
    pub fn new(survey: Arc<Mutex<Survey>>) -> Self {
        Instance {
            survey,
            depth: 0,
            pending_regions: vec![],
            regions: vec![],
        }
    }

    pub fn open_region(&mut self, label: &str, r: &mut dyn Seek) {
        let start = r.seek(SeekFrom::Current(0)).unwrap();
        self.pending_regions
            .push((self.depth, label.to_owned(), start));
        self.depth += 1;
    }

    pub fn close_region(&mut self, r: &mut dyn Seek) {
        let end = r.seek(SeekFrom::Current(0)).unwrap();
        let (depth, label, start) = self.pending_regions.pop().unwrap();
        self.regions.push((depth, label, start, end));
        self.depth -= 1;
    }

    pub fn sample(&self, id: u64, r: impl Read + Clone, reference: &Reference) {
        self.survey.lock().unwrap().sample(id, r, reference)
    }
}

impl Survey {
    pub fn sample(&self, id: u64, r: impl Read + Clone, reference: &Reference) {
        let mut samples: Vec<Value> = vec![];
        let _ = r.clone().read_u8().map(|x| samples.push(Value::U8(x)));
        let _ = r
            .clone()
            .read_u16::<LE>()
            .map(|x| samples.push(Value::U16(x)));
        let _ = r
            .clone()
            .read_u32::<LE>()
            .map(|x| samples.push(Value::U32(x)));
        let _ = r
            .clone()
            .read_u64::<LE>()
            .map(|x| samples.push(Value::U64(x)));
        {
            let mut r = r.clone();
            let _ = crate::util::read_f16_le(&mut r).map(|f0| {
                if f0.is_finite() {
                    samples.push(Value::F16(f0));
                    let _ = crate::util::read_f16_le(&mut r).map(|f1| {
                        if f1.is_finite() {
                            samples.push(Value::F16_2([f0, f1]));
                            let _ = crate::util::read_f16_le(&mut r).map(|f2| {
                                if f2.is_finite() {
                                    samples.push(Value::F16_3([f0, f1, f2]));
                                    let _ = crate::util::read_f16_le(&mut r).map(|f3| {
                                        if f3.is_finite() {
                                            samples.push(Value::F16_4([f0, f1, f2, f3]));
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        {
            let mut r = r.clone();
            let _ = r.read_f32::<LE>().map(|f0| {
                if f0.is_finite() {
                    samples.push(Value::F32(f0));
                    let _ = r.read_f32::<LE>().map(|f1| {
                        if f1.is_finite() {
                            samples.push(Value::F32_2([f0, f1]));
                            let _ = r.read_f32::<LE>().map(|f2| {
                                if f2.is_finite() {
                                    samples.push(Value::F32_3([f0, f1, f2]));
                                    let _ = r.read_f32::<LE>().map(|f3| {
                                        if f3.is_finite() {
                                            samples.push(Value::F32_4([f0, f1, f2, f3]));
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }

        {
            let mut r = r;
            let _ = r.read_f64::<LE>().map(|f0| {
                if f0.is_finite() {
                    samples.push(Value::F64(f0));
                    let _ = r.read_f64::<LE>().map(|f1| {
                        if f1.is_finite() {
                            samples.push(Value::F64_2([f0, f1]));
                            let _ = r.read_f64::<LE>().map(|f2| {
                                if f2.is_finite() {
                                    samples.push(Value::F64_3([f0, f1, f2]));
                                    let _ = r.read_f64::<LE>().map(|f3| {
                                        if f3.is_finite() {
                                            samples.push(Value::F64_4([f0, f1, f2, f3]));
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
        let reference = reference.clone();
        self.results
            .lock()
            .unwrap()
            .insert(id, (reference, samples));
    }
}
