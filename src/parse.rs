use regex::Regex;

pub struct ParseOps<'i> {
    s: &'i str,
}

impl<'i> ParseOps<'i> {
    pub fn new(s: &'i str) -> Self {
        ParseOps { s: s.trim() }
    }

    pub fn word(&mut self) -> Option<&'i str> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r#"^\s*(\S+)(.*)$"#).unwrap();
        }
        let caps = RE.captures(self.s)?;
        if caps.len() != 3 {
            None
        } else {
            self.s = caps.get(2).unwrap().as_str();
            Some(caps.get(1).unwrap().as_str())
        }
    }

    pub fn word_or<E>(&mut self, err: E) -> Result<&'i str, E> {
        self.word().ok_or(err)
    }

    pub fn usize(&mut self) -> Option<usize> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r#"^\s*(\d+)(.*)$"#).unwrap();
        }
        let caps = RE.captures(self.s)?;
        if caps.len() != 3 {
            None
        } else {
            self.s = caps.get(2).unwrap().as_str();
            caps.get(1).and_then(|c| c.as_str().parse().ok())
        }
    }

    pub fn usize_or<E>(&mut self, err: E) -> Result<usize, E> {
        self.usize().ok_or(err)
    }

    pub fn quoted_string(&mut self) -> Option<&'i str> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r#"^\s*"([^"]*)"(.*)$"#).unwrap();
        }
        let caps = RE.captures(self.s)?;
        if caps.len() != 3 {
            None
        } else {
            self.s = caps.get(2).unwrap().as_str();
            caps.get(1).map(|c| c.as_str())
        }
    }

    pub fn quoted_string_or<E>(&mut self, err: E) -> Result<&'i str, E> {
        self.quoted_string().ok_or(err)
    }

    pub fn tail(self) -> &'i str {
        self.s
    }
}
