pub fn murmur2_32(bytes: &[u8]) -> u32 {
    let len = bytes.len() as u32;
    let m = 0x5bd1_e995u32;
    let r = 24u32;

    let mut h = len;

    let rem = len % 4;
    let lim = (len - rem) as usize;
    for i in (0..lim).step_by(4) {
        let d0 = bytes[i] as u32;
        let d1 = (bytes[i + 1] as u32).wrapping_shl(8);
        let d2 = (bytes[i + 2] as u32).wrapping_shl(16);
        let d3 = (bytes[i + 3] as u32).wrapping_shl(24);
        let mut k: u32 = d0 | d1 | d2 | d3;
        k = k.wrapping_mul(m);
        k ^= k.wrapping_shr(r);
        k = k.wrapping_mul(m);

        h = h.wrapping_mul(m);
        h ^= k;
    }

    if rem == 3 {
        h ^= (bytes[lim + 2] as u32).wrapping_shl(16);
    }
    if rem >= 2 {
        h ^= (bytes[lim + 1] as u32).wrapping_shl(8);
    }
    if rem >= 1 {
        h ^= bytes[lim] as u32;
        h = h.wrapping_mul(m);
    }

    h ^= h.wrapping_shr(13);
    h = h.wrapping_mul(m);
    h ^= h.wrapping_shr(15);

    h
}
