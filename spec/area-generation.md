```
[4:56 PM] zao: I actually don't know anything, started from arm files and am working outwards through gt/et/ao/atlas and started looking at tsi and tst.
[4:58 PM] chuanhsing: WorldAreas.dat contains Topologies, Topologies contain dgrfile (Digital Graph Document), and it point to MasterFile (.tsi) contains generate.rs, that mapping to .arm file
[5:01 PM] chuanhsing: I'm rewriting Mod tools these days, wish I have time to make .arm on the web later
[5:04 PM] zao: Ooh, thanks.
[5:04 PM] chuanhsing:
WorldAreas
    ID: 2, Name: The Twilight Strand, Topologies: 0, 1, 2
Topologies
    ID: 0, DGR: Metadata/Terrain/Act1/Area1/Graphs/macro_beach4.tgr
Metadata/Terrain/Act1/Area1/Graphs/macro_beach4.tgr
    MasterFile: "Metadata/Terrain/Act1/Area1/master.tsi"
Metadata/Terrain/Act1/Area1/master.tsi
    RoomSet                 "room_tiles.rs"
Metadata/Terrain/Act1/Area1/room_tiles.rs
    version 2
    "Metadata/Terrain/Act1/Area1/Rooms/town_entrance_exclude.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/washed_up3.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/largecliff_wall_1a.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/largecliff_wall_2a.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/largecliff_wall_3a.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/largecliff_wall_4a.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/stream_shore_1.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/stream_shore_2.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/cliff_nests.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/cliff_nests2.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/cliff_nests3.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/cliff_beach.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/streams.arm"
    "Metadata/Terrain/Act1/Area1/Rooms/tutorial_01.arm"
```