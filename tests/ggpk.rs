use poe::formats::ggpk;

#[macro_use]
extern crate failure;

#[cfg(not(windows))]
const REFERENCE_PATH: &str = r#"/mnt/c/Temp/GGPKs/Content-2019-03-31.ggpk"#;

#[cfg(windows)]
const REFERENCE_PATH: &str = r#"C:\Temp\GGPKs\Content-2019-03-31.ggpk"#;

#[test]
fn open() -> Result<(), failure::Error> {
    ggpk::Ggpk::open(REFERENCE_PATH)?;
    Ok(())
}

#[test]
fn show_root() -> Result<(), failure::Error> {
    use ggpk::NodeInfo;
    let pack = ggpk::Ggpk::open(REFERENCE_PATH)?;
    let root_dir = pack.directory(pack.root())?;
    for entry in root_dir.entries() {
        let resolved = entry.resolve()?;
        let is_dir = match resolved {
            ggpk::ResolvedEntry::Directory(..) => true,
            _ => false,
        };
        eprintln!(r#"{}{}"#, resolved.name(), if is_dir { "/" } else { "" });
        // match entry {
        //     Directory(d) => "directory",
        //     File(f) => "file",
        // }
    }
    Ok(())
}

#[test]
fn cat_uiimages() -> Result<(), failure::Error> {
    let pack = ggpk::Ggpk::open(REFERENCE_PATH)?;
    let file = pack.lookup("Art/UIImages1.txt")?;
    if let ggpk::ResolvedEntry::File(file) = file {
        use byteorder::{ReadBytesExt, LE};
        let mut bytes = file.contents();
        let mut code_units: Vec<u16> = Vec::new();
        code_units.resize(bytes.len() / 2, 0u16);
        bytes.read_u16_into::<LE>(&mut code_units)?;
        let text = String::from_utf16(&code_units)?;
        eprintln!("===\n{}\n===", text);
    } else {
        bail!("File lookup found a directory.");
    }
    Ok(())
}

#[test]
fn read_glacier() -> Result<(), failure::Error> {
    let pack = ggpk::Ggpk::open(REFERENCE_PATH)?;
    let file = pack.lookup("Metadata/Terrain/Missions/Hideouts/Glacier/Rooms/hideout.arm")?;
    if let ggpk::ResolvedEntry::File(file) = file {
        use byteorder::{ReadBytesExt, LE};
        let mut bytes = file.contents();
        let mut code_units: Vec<u16> = Vec::new();
        code_units.resize(bytes.len() / 2, 0u16);
        bytes.read_u16_into::<LE>(&mut code_units)?;
        let text = String::from_utf16(&code_units)?;
        eprintln!("===\n{}\n===", text);
    }
    Ok(())
}
