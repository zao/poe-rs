extern crate proc_macro;

use quote::quote;
use syn::parse_macro_input;

use proc_macro::TokenStream;
use syn::DeriveInput;

#[proc_macro_derive(SpecInfo, attributes(spec_info))]
pub fn derive_spec_info(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let name = &input.ident;

    let row_widths = match input.data {
        syn::Data::Struct(ref data) => {
            data.fields.iter().map(|field| {
                let ty = &field.ty;
                quote! {
                    <#ty>::field_width()
                }
            }).collect()
        }
        _ => vec![]
    };
    let populates = match input.data {
        syn::Data::Struct(ref data) => {
            data.fields.iter().map(|field| {
                field.attrs.iter().for_each(|attr| {
                    eprintln!("{:?}", &attr.tts);
                });
                let ident = field.ident.as_ref().unwrap();
                quote! {
                    #ident: DatRead::dat_read(&mut r)?
                }
            }).collect()
        }
        _ => vec![],
    };

    let expanded = quote! {
        impl crate::formats::dat::SpecInfo for #name {
            fn row_width() -> usize {
                use super::DatRead;
                #(#row_widths)+*
            }
            
            fn populate(base: &[u8], data: &[u8]) -> Option<Self> {
                use super::DatRead;
                let mut r = DatReader::new(base, data);

                Some(Self {
                    #(#populates,)*
                })
            }
        }
    };

    TokenStream::from(expanded)
}